
class H5PPlatform implements H5PFrameworkInterface {
  public function getPlatformInfo() {
    return h5phost_get_platform_info();
  }

  public function fetchExternalData($url, $data = NULL, $blocking = TRUE, $stream = NULL) {
    return h5phost_fetch_external_data($url, $data, $blocking, $stream);
  }

  public function setLibraryTutorialUrl($machineName, $tutorialUrl) {
    h5phost_set_library_tutorial_url($machineName, $tutorialUrl);
  }

  public function setErrorMessage($message, $code = NULL) {
    h5phost_set_error_message($message, $code);
  }

  public function setInfoMessage($message) {
    h5phost_set_info_message($message);
  }

  public function getMessages($type) {
    return h5phost_get_message($type);
  }

  public function t($message, $replacements = array()) {
    return h5phost_t($message, $replacements);
  }

  public function getLibraryFileUrl($libraryFolderName, $fileName) {
    return h5phost_get_library_file_url($libraryFolderName, $fileName);
  }

  public function getUploadedH5pFolderPath() {
    return h5phost_get_uploaded_h5p_folder_path();
  }

  public function getUploadedH5pPath() {
    return h5phost_get_uploaded_h5p_path();
  }

  public function loadAddons() {
    return h5phost_load_addons();
  }

  public function getLibraryConfig($libraries = NULL) {
    return h5phost_get_library_config($libraries);
  }

  public function loadLibraries() {
    return h5phost_load_libraries();
  }

  public function getAdminUrl() {
    return h5phost_get_admin_url();
  }

  public function getLibraryId($machineName, $majorVersion = NULL, $minorVersion = NULL) {
    return h5phost_get_library_id($machineName, $majorVersion, $minorVersion);
  }

  public function getWhitelist($isLibrary, $defaultContentWhitelist, $defaultLibraryWhitelist) {
    return h5phost_get_whitelist($isLibrary, $defaultContentWhitelist, $defaultLibraryWhitelist);
  }

  public function isPatchedLibrary($library) {
    return h5phost_is_patched_library($library);
  }

  public function isInDevMode() {
    return h5phost_is_in_dev_mode();
  }

  public function mayUpdateLibraries() {
    return h5phost_may_update_libraries();
  }

  public function saveLibraryData(&$libraryData, $new = TRUE) {
    h5phost_save_library_data($libraryData, $new);
  }

  public function insertContent($content, $contentMainId = NULL) {
    return h5phost_insert_content($content, $contentMainId);
  }

  public function updateContent($content, $contentMainId = NULL) {
    h5phost_update_content($content, $contentMainId);
  }

  public function resetContentUserData($contentId) {
    h5phost_reset_content_user_data($contentId);
  }

  public function saveLibraryDependencies($libraryId, $dependencies, $dependency_type) {
    h5phost_save_library_dependencies($libraryId, $dependencies, $dependency_type);
  }

  public function copyLibraryUsage($contentId, $copyFromId, $contentMainId = NULL) {
    h5phost_copy_library_usage($contentId, $copyFromId, $contentMainId);
  }

  public function deleteContentData($contentId) {
    h5phost_delete_content_data($contentId);
  }

  public function deleteLibraryUsage($contentId) {
    h5phost_delete_library_usage($contentId);
  }

  public function saveLibraryUsage($contentId, $librariesInUse) {
    h5phost_save_library_usage($contentId, $librariesInUse);
  }

  public function getLibraryUsage($libraryId, $skipContent = FALSE) {
    return h5phost_get_library_usage($libraryId, $skipContent);
  }

  public function loadLibrary($machineName, $majorVersion, $minorVersion) {
    return h5phost_load_library($machineName, $majorVersion, $minorVersion);
  }

  public function loadLibrarySemantics($machineName, $majorVersion, $minorVersion) {
    return h5phost_load_library_semantics($machineName, $majorVersion, $minorVersion);
  }

  public function alterLibrarySemantics(&$semantics, $machineName, $majorVersion, $minorVersion) {
    h5phost_alter_library_semantics($semantics, $machineName, $majorVersion, $minorVersion);
  }

  public function deleteLibraryDependencies($libraryId) {
    h5phost_delete_library_dependencies($libraryId);
  }

  public function lockDependencyStorage() {
    h5phost_lock_dependency_storage();
  }

  public function unlockDependencyStorage() {
    h5phost_unlock_dependency_storage();
  }


  public function deleteLibrary($library) {
    h5phost_delete_library($library);
  }

  public function loadContent($id) {
    return h5phost_load_content($id);
  }

  public function loadContentDependencies($id, $type = NULL) {
    return h5phost_load_content_dependencies($id, $type);
  }

  public function getOption($name, $default = NULL) {
    $opt = h5phost_get_option($name);
    return $opt !== null ? $opt : $default;
  }

  public function setOption($name, $value) {
    h5phost_set_option($name, $value);
  }

  public function updateContentFields($id, $fields) {
    h5phost_update_content_fields($id, $fields);
  }

  public function clearFilteredParameters($library_ids) {
    h5phost_clear_filtered_parameters($library_ids);
  }

  public function getNumNotFiltered() {
    return h5phost_get_num_not_filtered();
  }

  public function getNumContent($libraryId, $skip = NULL) {
    return h5phost_get_num_content($libraryId, $skip);
  }

  public function isContentSlugAvailable($slug) {
    return h5phost_is_content_slug_available($slug);
  }

  public function getLibraryStats($type) {
    return h5phost_get_library_stats($type);
  }

  public function getNumAuthors() {
    return h5phost_get_num_authors();
  }

  public function saveCachedAssets($key, $libraries) {
    h5phost_save_cached_assets($key, $libraries);
  }

  public function deleteCachedAssets($library_id) {
    h5phost_delete_cached_assets($library_id);
  }

  public function getLibraryContentCount() {
    return h5phost_get_library_content_count();
  }

  public function afterExportCreated($content, $filename) {
    h5phost_after_export_created($content, $filename);
  }

  public function hasPermission($permission, $id = NULL) {
    return h5phost_has_permission($permission, $id);
  }

  public function replaceContentTypeCache($contentTypeCache) {
    h5phost_replace_content_type_cache($contentTypeCache);
  }

  public function libraryHasUpgrade($library) {
    return h5phost_library_has_upgrade($library);
  }
}

