//
// Created by janeover on 17.11.2020.
//

#include "filesystem/MappedFile.h"
#include "filesystem/FileException.h"
#include <iostream>
#include <sys/mman.h>
#include <fcntl.h>
#include <unistd.h>
#include <boost/algorithm/string.hpp>

#define ESCAPE_SIMP(esc) generated[pos++] = '\\'; \
    generated[pos++] = esc;
#define CASE_SIMP(ch, esc) case ch: \
    ESCAPE_SIMP(esc);               \
    break;
#define CASE_SIMP_S(chesc) CASE_SIMP(chesc, chesc)

int gen_file(const std::string &inputFile, const std::string &outputFile, const std::string &defineName) {
    std::string codegen("#ifndef ");
    {
        std::string duplicateDefName = inputFile;
        while (true) {
            auto iterator = std::find(duplicateDefName.begin(), duplicateDefName.end(), '/');
            if (iterator == duplicateDefName.end()) {
                break;
            }
            iterator++;
            std::string build;
            std::for_each(iterator, duplicateDefName.end(), [&build] (const char ch) { build.append({ch}); });
            duplicateDefName = build;
        }
        boost::to_upper(duplicateDefName);
        std::replace(duplicateDefName.begin(), duplicateDefName.end(), '.', '_');
        codegen.append(duplicateDefName);
        codegen.append("\n#define ");
        codegen.append(duplicateDefName);
    }
    codegen.append("\n\n#define ");
    codegen.append(defineName);
    codegen.append(" \"");
    try {
        MappedFile mappedFile(inputFile);
        const size_t generated_size = mappedFile.Size() * 5;
        unsigned char *generated = static_cast<unsigned char *>(mmap(NULL, generated_size, PROT_READ | PROT_WRITE,
                                                                     MAP_PRIVATE | MAP_ANONYMOUS, -1, 0));
        if (generated == MAP_FAILED) {
            perror("mmap failed");
            return 2;
        }
        size_t pos = 0;
        for (const unsigned char ch : mappedFile.iterate<unsigned char>()) {
            switch (ch) {
                CASE_SIMP('\0', '0')
                CASE_SIMP('\a', 'a')
                CASE_SIMP('\b', 'b')
                CASE_SIMP('\f', 'f')
                CASE_SIMP('\v', 'v')
                CASE_SIMP('\t', 't')
                CASE_SIMP('\r', 'r')
                CASE_SIMP('\n', 'n')
                CASE_SIMP_S('\?')
                CASE_SIMP_S('\'')
                CASE_SIMP_S('\"')
                CASE_SIMP_S('\\')
                default:
                    if (ch < ' ' || ch > '~') {
                        generated[pos++] = '\\';
                        generated[pos++] = '0' + (ch >> 6);
                        generated[pos++] = '0' + ((ch >> 3) & 7);
                        generated[pos++] = '0' + (ch & 7);
                    } else {
                        generated[pos++] = ch;
                    }
            }
        }
        {
            const void *generated_ptr = static_cast<void *>(generated);
            codegen.append(static_cast<const char *>(generated_ptr), pos);
        }
        if (munmap(generated, generated_size) != 0) {
            perror("munmap failed");
            return 2;
        }
    } catch (FileException &e) {
        std::cerr << e.what() << "\n";
        return 2;
    }
    codegen.append("\"\n\n#endif\n");

    const char *cptr = codegen.c_str();
    size_t remaining = codegen.length();

    int fd = open(outputFile.c_str(), O_WRONLY | O_TRUNC | O_CREAT, 0644);
    if (fd < 0) {
        perror("open output");
        return 2;
    }
    while (remaining > 0) {
        auto written = write(fd, cptr, remaining);
        if (written < 0) {
            perror("write output");
            close(fd);
            return 2;
        }
        remaining -= written;
        cptr += written;
    }
    if (close(fd) != 0) {
        perror("close output");
        return 2;
    }
    return 0;
}

int input_file(const std::string &cmd, const std::string &inputFile, int argc, const char **argv) {
    if (argc == 2) {
        return gen_file(inputFile, std::string(argv[0]), std::string(argv[1]));
    } else {
        std::cerr << "Usage:\n" << cmd << " <input-file> <output-file> <define-name>\n";
        return 1;
    }
}

int main(int argc, const char **argv) {
    if (argc > 1) {
        return input_file(std::string(argv[0]), std::string(argv[1]), argc - 2, &(argv[2]));
    } else {
        std::cerr << "Usage:\n" << argv[0] << " <input-file> <output-file>\n";
        return 1;
    }
}