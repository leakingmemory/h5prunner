//
// Created by janeover on 17.11.2020.
//

// Generated:
#include "H5pPlatform.php.h"
#include "filesystem/MappedFile.h"
#include <h5prunner/H5pMemoryPlatform.h>
#include <h5prunner/H5pPhpBindings.h>
#include <h5prunner/PhpEngine.h>
#include <h5prunner/H5pCore.h>

#include <iostream>

int run_h5p_lib(const std::string &cmd, const std::string &h5pLibPath, int argc, const char **argv) {
    if (argc > 0) {
        std::string command = argv[0];
        if (command == "validate") {
            if (argc == 2) {
                H5pCore h5pCore("https://testh5p.radiotube.org/files/", h5pLibPath);
                MappedFile mappedFile(argv[1]);
                if (h5pCore.ValidateContent(mappedFile.Content(), mappedFile.Size())) {
                    std::cout << "H5p file appears to be valid\n";
                } else {
                    std::cerr << "Error: H5p file did not work\n";
                    for (auto msg : h5pCore.getErrorMessages()) {
                        std::cerr << msg << "\n";
                    }
                }
                return 0;
            } else {
                std::cerr << "Usage:\n" << argv[0] << " <h5p-lib-path> validate <h5p-file>\n";
                return 1;
            }
        } else if (command == "testimport" || command == "testembed") {
            if (argc == 2) {
                H5pCore h5pCore("https://testh5p.radiotube.org/files/", h5pLibPath);
                MappedFile mappedFile(argv[1]);
                std::optional<unsigned int> content_id = h5pCore.ImportContent(mappedFile.Content(), mappedFile.Size(), true, true);
                if (content_id) {
                    std::cout << "H5p content appears to have been imported with ID: " << content_id.value() << "\n";
                    for (auto msg : h5pCore.getInfoMessages()) {
                        std::cout << msg << "\n";
                    }
                    for (auto msg : h5pCore.getErrorMessages()) {
                        std::cerr << msg << "\n";
                    }

                    if (command == "testembed") {
                        H5pViewData viewData = h5pCore.getViewData(content_id.value());
                        web::json::value json = web::json::value::object();
                        web::json::object &object = json.as_object();
                        object["title"] = web::json::value::string(viewData.title);
                        object["embedType"] = web::json::value::string(viewData.embedType);
                        object["raw_files"] = web::json::value::object();
                        object["h5p_files"] = web::json::value::object();
                        object["raw_files"]["styles"] = web::json::value::array();
                        object["raw_files"]["scripts"] = web::json::value::array();
                        object["h5p_files"]["styles"] = web::json::value::array();
                        object["h5p_files"]["scripts"] = web::json::value::array();
                        for (const H5pFile &file : viewData.rawFiles.styles) {
                            web::json::array &array = object["raw_files"]["styles"].as_array();
                            std::string uri(file.filename);
                            uri.append(file.cacheBusting);
                            array[array.size()] = web::json::value::string(uri);
                        }
                        for (const H5pFile &file : viewData.rawFiles.scripts) {
                            web::json::array &array = object["raw_files"]["scripts"].as_array();
                            std::string uri(file.filename);
                            uri.append(file.cacheBusting);
                            array[array.size()] = web::json::value::string(uri);
                        }
                        for (const std::string &file : viewData.h5pFiles.styles) {
                            web::json::array &array = object["h5p_files"]["styles"].as_array();
                            array[array.size()] = web::json::value::string(file);
                        }
                        for (const std::string &file : viewData.h5pFiles.scripts) {
                            web::json::array &array = object["h5p_files"]["scripts"].as_array();
                            array[array.size()] = web::json::value::string(file);
                        }
                        std::cout << json.serialize() << "\n";
                    } else {
                        Dependencies deps = h5pCore.getDependencies(content_id.value());
                        std::function<void (int, const Dependencies &)> printDeps = [&printDeps] (int level, const Dependencies &deps) {
                            for (auto dep : deps.libraries) {
                                for (auto i = level; i > 0; i--) {
                                    std::cout << " ";
                                }
                                std::cout << "- " << dep.library_id << "\n";
                                printDeps(level + 1, dep.dependencies);
                            }
                        };
                        std::cout << "Content " << content_id.value() << " dependencies:\n";
                        printDeps(1, deps);
                    }
                } else {
                    std::cerr << "No content ID returned\n";
                    bool err = false;
                    for (auto msg : h5pCore.getInfoMessages()) {
                        std::cout << msg << "\n";
                    }
                    for (auto msg : h5pCore.getErrorMessages()) {
                        std::cerr << msg << "\n";
                        err = true;
                    }
                    if (err) {
                        return 2;
                    }
                }
                return 0;
            } else {
                std::cerr << "Usage:\n" << argv[0] << " <h5p-lib-path> " << command << " <h5p-file>\n";
                return 1;
            }
        }
        std::cerr << "Command not found: " << argv[0] << "\n";
        return 1;
    } else {
        H5pCore h5pCore("https://testh5p.radiotube.org/files/", h5pLibPath);
        return 0;
    }
}

int main(int argc, const char **argv) {
    if (argc > 1) {
        run_h5p_lib(std::string(argv[0]), std::string(argv[1]), argc - 2, &(argv[2]));
    } else {
        std::cerr << "Usage:\n" << argv[0] << " <h5p-lib-path> ...\n";
    }
}