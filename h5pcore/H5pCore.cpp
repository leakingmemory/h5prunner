//
// Created by janeover on 18.11.2020.
//

#include <h5prunner/H5pCore.h>
#include <h5prunner/TmpFile.h>

#include <cpprest/json.h>

#include "../H5pPlatform.php.h"

H5pCore::H5pCore(const std::string &url_files, const std::string &h5p_lib_path) : h5pMemoryPlatform(url_files), h5pPhpBindings(h5pMemoryPlatform), phpEngine(h5pPhpBindings.zend_function_entries()) {
    std::string h5pRun(h5p_lib_path);
    h5pRun.append("/vendor/autoload.php");

    phpEngine.executeFile(h5pRun);

    phpEngine.execute(std::string(h5p_platform_php, sizeof(h5p_platform_php)));

    h5pPhpBindings.pushCommand("false"); // export enabled
    h5pPhpBindings.pushCommand("en"); // language
    h5pPhpBindings.pushCommand(url_files); // url to files
    h5pPhpBindings.pushCommand(tmpDir.GetPath());

    phpEngine.execute(R"(
        $h5pCore = (function () {
            $filePath = host_pop_command();
            $fileUrl = host_pop_command();
            $language = host_pop_command();
            $exportEnabled = host_pop_command() !== "true";

            return new H5pCore(new H5PPlatform(), $filePath, $fileUrl, $language, $exportEnabled);
        })();

        $_SESSION = array();

        host_push_command("success");
    )");

    if (h5pPhpBindings.popCommand() != "success") {
        throw std::exception();
    }
}

bool H5pCore::ValidateContent(const void *ptr, size_t size) {
    TmpDir unpackDir;
    TmpFile tmpFile(ptr, size);
    tmpFile.RenameWithSuffix(".h5p");

    {
        auto uploadContext = h5pPhpBindings.framework_interface()->setH5pUpload(tmpFile.GetPath(), unpackDir.GetPath());

        phpEngine.execute(R"(
            (function ($h5pCore) {
                $h5pValidator = new H5PValidator(new H5PPlatform(), $h5pCore);
                if ($h5pValidator->isValidPackage()) {
                    host_push_command("true");
                } else {
                    host_push_command("false");
                }
            })($h5pCore);
        )");
    }

    if (h5pPhpBindings.popCommand() == "true") {
        return true;
    } else {
        return false;
    }
}

std::vector<std::string> H5pCore::getErrorMessages() {
    return h5pMemoryPlatform.getMessages(h5p::error);
}

std::optional<unsigned int> H5pCore::ImportContent(const void *ptr, size_t size, bool content, bool libs) {
    TmpDir unpackDir;
    TmpFile tmpFile(ptr, size);
    tmpFile.RenameWithSuffix(".h5p");

    {
        auto uploadContext = h5pPhpBindings.framework_interface()->setH5pUpload(tmpFile.GetPath(), unpackDir.GetPath());

        h5pPhpBindings.pushCommand(content ? "true" : "false");
        h5pMemoryPlatform.SetMayUpdateLibraries(libs);

        phpEngine.execute(R"(
            (function ($h5pCore) {
                $importContent = host_pop_command() === "true";

                $platform = new H5PPlatform();
                $h5pValidator = new H5PValidator($platform, $h5pCore);
                $h5pStorage = new H5PStorage($platform, $h5pCore);
                if ($h5pValidator->isValidPackage()) {
                    $h5pStorage->savePackage(NULL, NULL, !$importContent);
                    if ($h5pStorage->contentId) {
                        host_push_command("".$h5pStorage->contentId);
                    } else {
                        host_push_command("false");
                    }
                } else {
                    host_push_command("false");
                }
            })($h5pCore);
        )");
    }

    std::string result = h5pPhpBindings.popCommand();
    if (result != "false") {
        unsigned int content_id = std::stol(result);
        return {content_id};
    } else {
        return {};
    }
}

std::vector<std::string> H5pCore::getInfoMessages() {
    return h5pMemoryPlatform.getMessages(h5p::info);
}

template<typename T> inline std::vector<T> getH5pFileList(
        web::json::object &object,
        const std::string &key,
        std::function<T (web::json::value &)> factory
        ) {
    web::json::value value = object[key];
    if (!value.is_array()) {
        throw std::exception();
    }
    web::json::array array = value.as_array();
    std::vector<T> files = {};
    for (web::json::value item : array) {
        files.push_back(factory(item));
    }
    return files;
}
template<typename T> inline H5pFiles<T> getH5pFiles(
        web::json::object &object,
        const std::string &key,
        std::function<T (web::json::value &)> factory
        ) {
    H5pFiles<T> files;
    web::json::value value = object[key];
    if (!value.is_object()) {
        throw std::exception();
    }
    web::json::object &obj = value.as_object();
    files.scripts = getH5pFileList<T>(obj, "scripts", factory);
    files.styles = getH5pFileList<T>(obj, "styles", factory);
    return files;
}

H5pViewData H5pCore::getViewData(unsigned int id) {
    h5pPhpBindings.pushCommand(std::to_string(id));
    phpEngine.execute(R"(
        (function ($h5pCore) {
            $id = (int) host_pop_command();
            $content = $h5pCore->loadContent($id);
            $h5pCore->filterParameters($content);
            $result = [
                'content' => $content,
                'embedType' => \H5pCore::determineEmbedType($result['content']['embedType'], $result['content']['library']['embedTypes'])
            ];
            $preload = $h5pCore->loadContentDependencies($id, 'preloaded');
            $result['files'] = $h5pCore->getDependenciesFiles($preload);
            $result['files_h5pcore']['scripts'] = $h5pCore->getAssetsUrls($result['files']['scripts']);
            $result['files_h5pcore']['styles'] = $h5pCore->getAssetsUrls($result['files']['styles']);
            host_push_command(json_encode($result));
        })($h5pCore);
    )");
    std::string result = h5pPhpBindings.popCommand();
    if (result.empty()) {
        throw std::exception();
    }
    web::json::object object = ([&result] () {
        {
            web::json::value value = web::json::value::parse(result);
            if (!value.is_object()) {
                throw std::exception();
            }
            return value.as_object();
        }
    })();
    web::json::object content = ([&object] () {
        web::json::value value = object["content"];
        if (!value.is_object()) {
            throw std::exception();
        }
        return value.as_object();
    })();
    auto asString = [] (web::json::object &object, const std::string &key) {
        web::json::value value = object[key];
        if (!value.is_string()) {
            throw std::exception();
        }
        return value.as_string();
    };
    H5pViewData viewData = {};
    viewData.title = asString(content, "title");
    viewData.embedType = asString(object, "embedType");
    viewData.rawFiles = getH5pFiles<H5pFile>(object, "files", [] (web::json::value &value) {
        if (!value.is_object()) {
            throw std::exception();
        }
        web::json::object object = value.as_object();
        web::json::value path = object["path"];
        web::json::value version = object["version"];
        if (!path.is_string() || !version.is_string()) {
            throw std::exception();
        }
        H5pFile file{
            .filename = path.as_string(),
            .cacheBusting = version.as_string()
        };
        return file;
    });
    viewData.h5pFiles = getH5pFiles<std::string>(object, "files_h5pcore", [] (web::json::value &value) {
        if (value.is_string()) {
            return value.as_string();
        } else {
            throw std::exception();
        }
    });
    return viewData;
}

std::map<unsigned int, InMemoryH5pLibrary *> H5pCore::getDependenciesFromLib(const std::map<unsigned int, InMemoryH5pLibrary *> &map, InMemoryH5pLibrary &lib) {
    auto data = lib.GetDependencyData();
    std::map<unsigned int, InMemoryH5pLibrary *> deps{};
    std::vector<std::vector<h5p::library_without_patch_version>> deplists = {
            data.dynamicDependencies.value_or(std::vector<h5p::library_without_patch_version>{}),
            data.editorDependencies.value_or(std::vector<h5p::library_without_patch_version>{}),
            data.preloadedDependencies.value_or(std::vector<h5p::library_without_patch_version>{})
    };
    for (auto &list : deplists) {
        for (auto &dep : list) {
            bool found = false;
            for (auto [id, l] : map) {
                if (dep.machineName == l->GetMachineName() && dep.majorVersion == l->GetMajorVersion() &&
                    dep.minorVersion == l->GetMinorVersion()) {
                    if (deps.find(id) == deps.end()) {
                        deps.insert_or_assign(id, l);
                    }
                    found = true;
                }
            }
            if (!found) {
                throw std::exception();
            }
        }
    }
    return deps;
}

Dependencies H5pCore::getDependenciesFromFlat(const std::map<unsigned int, InMemoryH5pLibrary *> &deps) {
    std::function<std::vector<unsigned int> (const Dependencies &)> getFlat = [&getFlat] (const Dependencies &deps) {
        std::vector<unsigned int> collected = {};
        for (auto &dep : deps.libraries) {
            {
                unsigned int id = dep.library_id;
                if (std::find(collected.begin(), collected.end(), id) == collected.end()) {
                    collected.push_back(id);
                }
            }
            {
                std::vector<unsigned int> subdeps = getFlat(dep.dependencies);
                for (unsigned int id : subdeps) {
                    if (std::find(collected.begin(), collected.end(), id) == collected.end()) {
                        collected.push_back(id);
                    }
                }
            }
        }
        return collected;
    };
    std::function<Dependencies (std::vector<unsigned int> &, const std::map<unsigned int, InMemoryH5pLibrary *> &)> getDependencies = [this, &deps, &getDependencies, &getFlat] (std::vector<unsigned int> &trail, const std::map<unsigned int, InMemoryH5pLibrary *> &flat) {
        Dependencies dependencies = {};
        for (auto &[id, lib] : flat) {
            if (std::find(trail.begin(), trail.end(), id) != trail.end()) {
                std::cerr << "warning: circular library dependency ";
                for (auto circ : trail) {
                    std::cerr << circ << " > ";
                }
                std::cerr << id << "\n";
                continue;
            }
            const auto subdeps = getDependenciesFromLib(deps, *lib);
            trail.push_back(id);
            Dependency dependency = {.library_id = id, .dependencies = getDependencies(trail, subdeps)};
            trail.pop_back();
            dependencies.libraries.push_back(dependency);
        }
        Dependencies filteredDeps = {};
        std::vector<unsigned int> subdeps = {};
        for (auto &dependency : dependencies.libraries) {
            if (std::find(subdeps.begin(), subdeps.end(), dependency.library_id) != subdeps.end()) {
                continue;
            }
            std::vector<unsigned int> this_subdeps = {};
            for (auto id : getFlat(dependency.dependencies)) {
                if (id == dependency.library_id) {
                    throw std::exception(); // Circular dependency
                }
                if (std::find(subdeps.begin(), subdeps.end(), id) == subdeps.end()) {
                    subdeps.push_back(id);
                }
                if (std::find(this_subdeps.begin(), this_subdeps.end(), id) == this_subdeps.end()) {
                    this_subdeps.push_back(id);
                }
            }
            {
                Dependencies refiltered = {};
                int filtered = 0;
                for (auto &fdep : filteredDeps.libraries) {
                    if (std::find(this_subdeps.begin(), this_subdeps.end(), fdep.library_id) == this_subdeps.end()) {
                        refiltered.libraries.push_back(fdep);
                    } else {
                        filtered++;
                    }
                }
                if (filtered > 0) {
                    filteredDeps = refiltered;
                }
            }
            filteredDeps.libraries.push_back(dependency);
        }

        return filteredDeps;
    };
    std::vector<unsigned int> emptyTrail = {};
    return getDependencies(emptyTrail, deps);
}

Dependencies H5pCore::getDependencies(unsigned int id) {
    InMemoryContent *content = h5pMemoryPlatform.getContent(id);
    if (content == nullptr) {
        throw std::exception();
    }
    h5pPhpBindings.pushCommand(std::to_string(id));
    phpEngine.execute(R"(
        (function ($h5pCore) {
            $id = (int) host_pop_command();
            $content = $h5pCore->loadContent($id);
            $h5pCore->filterParameters($content);
        })($h5pCore);
    )");
    std::map<unsigned int, InMemoryH5pLibrary *> deps{};
    {
        std::vector<h5p::library_dependency> libdeps = content->GetLibdeps();
        for (auto dep : libdeps) {
            if (deps.find(dep.library_id) == deps.end()) {
                InMemoryH5pLibrary *lib = h5pMemoryPlatform.getLibrary(dep.library_id);
                if (lib == nullptr) {
                    throw std::exception();
                }
                deps.insert_or_assign(dep.library_id, lib);
            }
        }
    }
    return getDependenciesFromFlat(deps);
}
