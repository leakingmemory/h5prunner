//
// Created by janeover on 31.10.2020.
//

#ifndef H5PRUNNER_PHPENGINE_H
#define H5PRUNNER_PHPENGINE_H

#include <php.h>
#include <SAPI.h>
#include <h5prunner/PhpStreamWrapper.h>
#include <cpprest/json.h>

class PhpEngine {
private:
    PhpStreamWrapper *stdin_wr;
    PhpStreamWrapper *stdout_wr;
    PhpStreamWrapper *stderr_wr;
    zval *stdin_zval;
    zval *stdout_zval;
    zval *stderr_zval;
    sapi_module_struct sapi_module;
    void register_constant(const std::string &name, const zval &val);
public:
    PhpEngine(const PhpEngine &engine) = delete;
    PhpEngine(const zend_function_entry *pib_functions);
    ~PhpEngine();
    void execute(const std::string &script);
    web::json::value executeFile(const std::string &filename);
};


#endif //H5PRUNNER_PHPENGINE_H
