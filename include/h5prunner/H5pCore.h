//
// Created by janeover on 18.11.2020.
//

#ifndef H5PRUNNER_H5PCORE_H
#define H5PRUNNER_H5PCORE_H

#include <h5prunner/TmpDir.h>
#include <h5prunner/H5pMemoryPlatform.h>
#include <h5prunner/H5pPhpBindings.h>
#include <h5prunner/PhpEngine.h>

typedef struct {
    std::string filename;
    std::string cacheBusting;
} H5pFile;

template<typename T> struct s_h5p_files {
    std::vector<T> styles;
    std::vector<T> scripts;
};
template<typename T> using H5pFiles = struct s_h5p_files<T>;

typedef struct {
    std::string title;
    std::string embedType;
    H5pFiles<H5pFile> rawFiles;
    H5pFiles<std::string> h5pFiles;
} H5pViewData;

struct s_dependency;

typedef struct s_dependency Dependency;

typedef struct {
    std::vector<Dependency> libraries;
} Dependencies;

struct s_dependency {
    unsigned int library_id = 0;
    Dependencies dependencies;
};

class H5pCore {
private:
    TmpDir tmpDir;
    H5pMemoryPlatform h5pMemoryPlatform;
    H5pPhpBindings h5pPhpBindings;
    PhpEngine phpEngine;

    std::map<unsigned int, InMemoryH5pLibrary *> getDependenciesFromLib(const std::map<unsigned int, InMemoryH5pLibrary *> &map, InMemoryH5pLibrary &lib);
    Dependencies getDependenciesFromFlat(const std::map<unsigned int, InMemoryH5pLibrary *> &deps);
public:
    H5pCore(const std::string &url_files, const std::string &h5p_lib_path);
    bool ValidateContent(const void *ptr, size_t size);
    std::optional<unsigned int> ImportContent(const void *ptr, size_t size, bool content = true, bool libs = true);

    std::vector<std::string> getErrorMessages();
    std::vector<std::string> getInfoMessages();

    H5pViewData getViewData(unsigned int id);

    Dependencies getDependencies(unsigned int id);
};


#endif //H5PRUNNER_H5PCORE_H
