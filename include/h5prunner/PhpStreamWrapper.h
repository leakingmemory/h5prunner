//
// Created by janeover on 31.10.2020.
//

#ifndef H5PRUNNER_PHPSTREAMWRAPPER_H
#define H5PRUNNER_PHPSTREAMWRAPPER_H

extern "C" {
#include <php.h>
};

#include <string>

class PhpStreamWrapper {
private:
    php_stream *stream;
    zval *pzval;
public:
    PhpStreamWrapper();
    PhpStreamWrapper(const PhpStreamWrapper &cp) = delete;
    PhpStreamWrapper(PhpStreamWrapper &&mv) noexcept ;
    PhpStreamWrapper(int fd, const std::string &phpOpenMode = "rb");
    PhpStreamWrapper(const std::string &phpUri, const std::string &phpOpenMode = "rb");
    ~PhpStreamWrapper();

    php_stream *get_stream();
    php_stream *detach_stream();
    zval *to_zval();
};


#endif //H5PRUNNER_PHPSTREAMWRAPPER_H
