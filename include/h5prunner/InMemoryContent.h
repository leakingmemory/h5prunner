//
// Created by janeover on 06.11.2020.
//

#ifndef H5PRUNNER_INMEMORYCONTENT_H
#define H5PRUNNER_INMEMORYCONTENT_H

#include <h5prunner/H5pPlatformInterface.h>

class InMemoryContent {
private:
    h5p::content_data content;
    std::optional<unsigned int> content_main_id;
    std::vector<h5p::library_dependency> libdeps;
    std::optional<std::string> filtered;
public:
    InMemoryContent(const h5p::content_data &content, std::optional<unsigned int> content_main_id);
    InMemoryContent(const InMemoryContent &cp);
    unsigned int GetId();
    unsigned int GetLibraryId();
    std::optional<std::string> GetSlug();
    std::optional<std::string> GetFiltered();
    void Update(const h5p::content_data &content, std::optional<unsigned int> content_main_id);
    void UpdateLibdeps(const std::vector<h5p::library_dependency> &libdeps);
    void UpdateFields(const h5p::update_content_fields &fields);
    void ClearFiltered();
    const std::vector<h5p::library_dependency> &GetLibdeps();
    std::vector<h5p::library_dependency_load> LoadLibdeps(std::optional<h5p::dependency_type> type);
    h5p::content_load CreateContentLoad();
};


#endif //H5PRUNNER_INMEMORYCONTENT_H
