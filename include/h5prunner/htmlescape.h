//
// Created by janeover on 03.11.2020.
//

#ifndef H5PRUNNER_HTMLESCAPE_H
#define H5PRUNNER_HTMLESCAPE_H

#include <string>

inline void escape_html_append(std::string &str, const char ch) {
    switch (ch) {
        case '<':
            str.append("&lt;");
            break;
        case '>':
            str.append("&gt;");
            break;
        case '"':
            str.append("&quot;");
            break;
        case '\'':
            str.append("&#39;");
            break;
        case '&':
            str.append("&amp;");
            break;
        default:
            // Inefficient, avoid
            str.append(std::string(&ch, 1));
    }
}

inline void html_escape(std::string &string) {
    size_t pos = string.find_first_of("<>\"'&");
    if (pos == std::string::npos) {
        return;
    }
    char ch = string[pos];
    std::string remaining = string.substr(pos + 1, string.length());
    string.resize(pos);
    escape_html_append(string, ch);
    while (!remaining.empty()) {
        pos = remaining.find_first_of("<>\"'&");
        if (pos == std::string::npos) {
            string.append(remaining);
            return;
        }
        ch = remaining[pos];
        string.append(remaining.substr(0, pos));
        escape_html_append(string, ch);
        remaining = remaining.substr(pos + 1, remaining.length());
    }
}

#endif //H5PRUNNER_HTMLESCAPE_H
