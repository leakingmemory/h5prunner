//
// Created by janeover on 30.10.2020.
//

#ifndef H5PRUNNER_H5PPLATFORMINTERFACE_H
#define H5PRUNNER_H5PPLATFORMINTERFACE_H

#include <tuple>
#include <optional>
#include <vector>
#include <map>
#include <thread>

#include <cpprest/json.h>

#include <h5prunner/H5pExternalData.h>

namespace h5p {
    enum message_type {info, error};
    enum dependency_type {editor, preloaded, dynamic};
    enum permission {download_h5p, embed_h5p, create_restricted, install_recommended, update_libraries, copy_h5p};

    typedef struct {
        unsigned int libraryId;
        std::string addTo;
        std::string machineName;
        unsigned int majorVersion;
        unsigned int minorVersion;
        unsigned int patchVersion;
        std::vector<std::string> preloadedJs;
        std::vector<std::string> preloadedCss;
    } library_addon;

    typedef struct {
        unsigned int id;
        std::string name;
        std::string title;
        unsigned int major_version;
        unsigned int minor_version;
        unsigned int patch_version;
        bool runnable;
        bool restricted;
    } library_load;

    typedef struct {
        std::string machineName;
        unsigned int majorVersion;
        unsigned int minorVersion;
        unsigned int patchVersion;
    } library_with_version;
    typedef struct {
        std::string machineName;
        unsigned int majorVersion;
        unsigned int minorVersion;
    } library_without_patch_version;

    typedef struct {
        bool disable;
        bool disableExtraTitleField;
    } lib_metadata_settings;

    typedef struct {
        unsigned int libraryId;//: The id of the library if it is an existing library.
        std::string title;//: The library's name
        std::string machineName;//: The library machineName
        unsigned int majorVersion;//: The library's majorVersion
        unsigned int minorVersion;//: The library's minorVersion
        unsigned int patchVersion;//: The library's patchVersion
        bool runnable;//: 1 if the library is a content type, 0 otherwise
        h5p::lib_metadata_settings metadataSettings;//: Associative array containing:
        //      - disable: 1 if the library should not support setting metadata (copyright etc)
        //      - disableExtraTitleField: 1 if the library don't need the extra title field
        bool fullscreen;//(optional): 1 if the library supports fullscreen, 0 otherwise
        std::vector<std::string> embedTypes;//(optional): list of supported embed types
        std::vector<std::string> preloadedJs;//(optional): list of associative arrays containing:
        //     - path: path to a js file relative to the library root folder
        std::vector<std::string> preloadedCss;//(optional): list of associative arrays containing:
        //     - path: path to css file relative to the library root folder
        std::vector<std::string> dropLibraryCss;//(optional): list of associative arrays containing:
        //     - machineName: machine name for the librarys that are to drop their css
        std::optional<std::string> semantics;//(optional): Json describing the content structure for the library
        std::optional<std::map<std::string,std::string>> language;//(optional): associative array containing:
        //     - languageCode: Translation in json format

        // Undocumented:
        std::optional<std::string> addTo;
    } save_library_data;

    typedef struct {
        std::optional<std::vector<library_without_patch_version>> preloadedDependencies;
        std::optional<std::vector<library_without_patch_version>> dynamicDependencies;
        std::optional<std::vector<library_without_patch_version>> editorDependencies;
    } dependency_data;

    typedef struct {
        save_library_data library;
        dependency_data dependencies;
    } load_library_data;

    typedef struct {
        unsigned int id;
        std::string json;//*   - params: The content in json format
        unsigned int library;//: An associative array containing:
        //        *     - libraryId: The id of the main library for this content
        std::string title;

        // Undocumented features:
        std::map<std::string,std::string> metadata;
        std::vector<std::string> embed_type;
        bool disable;
        int max_score;
        std::optional<std::string> slug;
        std::string user_id;
        bool is_published;
        bool is_private;
        std::optional<std::string> language_iso_639_3;
    } content_data;

    typedef struct {
        std::optional<std::vector<std::string>> dropLibraryCss;
        std::string machineName;
        unsigned int libraryId;
    } library_dependency_library;
    typedef struct {
        unsigned int library_id;
        dependency_type type;
        bool drop_css;
    } library_dependency;

    typedef struct {
        unsigned int content;
        unsigned int libraries;
    } library_usage;

    typedef struct {
        unsigned int contentId;//: Identifier for the content
        std::string json;//*   - params: json content as string
        std::vector<std::string> embedType;//: csv of embed types
        std::string title;//: The contents title
        std::string user_id;
        bool disable;
        int max_score;
        std::optional<std::string> slug;
        std::optional<std::string> language;//: Language code for the content
        unsigned int libraryId;//: Id for the main library
        std::string libraryName;//: The library machine name
        unsigned int libraryMajorVersion;//: The library's majorVersion
        unsigned int libraryMinorVersion;//: The library's minorVersion
        std::vector<std::string> libraryEmbedTypes;//: CSV of the main library's embed types
        bool libraryFullscreen;//: 1 if fullscreen is supported. 0 otherwise.
        std::map<std::string,std::string> metadata;
    } content_load;

    typedef struct {
        std::optional<unsigned int> libraryId;//: The id of the library if it is an existing library.
        std::string machineName;//: The library machineName
        unsigned int majorVersion;//: The library's majorVersion
        unsigned int minorVersion;//: The library's minorVersion
        unsigned int patchVersion;//: The library's patchVersion
        std::optional<std::vector<std::string>> preloadedJs;//(optional): comma separated string with js file paths
        std::optional<std::vector<std::string>> preloadedCss;//(optional): comma separated sting with css file paths
        std::optional<std::vector<std::string>> dropCss;//(optional): csv of machine names
    } library_dependency_load;

    typedef struct {
        std::optional<std::string> title;
        std::optional<std::string> slug;
        std::optional<std::string> filtered;
    } update_content_fields;

    typedef struct {
        std::vector<std::vector<std::string>> delete_paths;
        web::json::value merge;
    } json_modifications;

    typedef struct {
        std::string name;
        unsigned int major_version;
        unsigned int minor_version;
        unsigned int patch_version;
        unsigned int h5P_major_version;
        unsigned int h5P_minor_version;
        std::string title;
        std::string summary;
        std::string description;
        std::string icon;
        bool is_recommended;
        std::string popularity;
        std::string screenshots;
        std::string license;
        std::string example;
        std::string tutorial;
        std::string keywords;
        std::string categories;
        std::string owner;
    } hub_content_type;
}

class TempContext {
private:
    std::function<void ()> destroy;
public:
    TempContext(TempContext &&mv) {
        destroy = mv.destroy;
        mv.destroy = [] () {};
    }
    TempContext(const TempContext &cp) = delete;
    explicit TempContext(std::function<void ()> &&destroy) : destroy(destroy) {
    }
    ~TempContext() {
        destroy();
    }
};

class H5PFrameworkInterface {
private:
    std::optional<std::string> uploadedH5p;
    std::optional<std::string> uploadedH5pTmpDir;
public:
        virtual ~H5PFrameworkInterface() = default;

        /**
         * Caution: H5p core may delete the file before returning (Doh! - php developers!)
         * Keep the context object until you're done. Destruction of it clears the filenames.
         */
        virtual TempContext setH5pUpload(const std::string &h5pTmpFilename, const std::string &tmpDir) {
            uploadedH5p = std::string(h5pTmpFilename);
            uploadedH5pTmpDir = std::string(tmpDir);
            return std::move(TempContext([&] () {
                uploadedH5p = {};
                uploadedH5pTmpDir = {};
            }));
        }

        /**
         * Returns info for the current platform
         *
         * @return array
         *   An associative array containing:
         *   - name: The name of the platform, for instance "Wordpress"
         *   - version: The version of the platform, for instance "4.0"
         *   - h5pVersion: The version of the H5P plugin/module
         */
        virtual std::tuple<std::string,std::string,std::string> getPlatformInfo() = 0;


        /**
         * Fetches a file from a remote server using HTTP GET
         *
         * @param string $url Where you want to get or send data.
         * @param array $data Data to post to the URL.
         * @param bool $blocking Set to 'FALSE' to instantly time out (fire and forget).
         * @param string $stream Path to where the file should be saved.
         * @return string The content (response body). NULL if something went wrong
         */
        virtual std::optional<std::string> fetchExternalData(
                const std::string &url,
                const std::optional<std::map<std::string,std::vector<std::string>>> &data = {},
                bool blocking = true,
                const std::optional<std::string> &stream = {}) {
            std::function<std::optional<std::string> (H5pExternalData &&)> doRequest = [stream] (auto &&request) {
                if (stream) {
                    // TODO - Filesystem required
                    throw std::exception();
                }
                return request.download();
            };
            H5pExternalData request(url, data);
            if (blocking) {
                return doRequest(std::move(request));
            } else {
                std::thread thread(doRequest, std::move(request));
                thread.detach();
                return {};
            }
        }

        /**
         * Set the tutorial URL for a library. All versions of the library is set
         *
         * @param string $machineName
         * @param string $tutorialUrl
         */
        virtual void setLibraryTutorialUrl(const std::string &machineName, const std::string &tutorialUrl) = 0;

        /**
         * Show the user an error message
         *
         * @param string $message The error message
         * @param string $code An optional code
         */
        virtual void setErrorMessage(const std::string &message, const std::optional<std::string> &code = {}) = 0;

        /**
         * Show the user an information message
         *
         * @param string $message
         *  The error message
         */
        virtual void setInfoMessage(const std::string &message) = 0;

        /**
         * Return messages
         *
         * @param string $type 'info' or 'error'
         * @return string[]
         */
        virtual std::vector<std::string> getMessages(h5p::message_type type) = 0;

        /**
         * Translation function
         *
         * @param string $message
         *  The english string to be translated.
         * @param array $replacements
         *   An associative array of replacements to make after translation. Incidences
         *   of any key in this array are replaced with the corresponding value. Based
         *   on the first character of the key, the value is escaped and/or themed:
         *    - !variable: inserted as is
         *    - @variable: escape plain text to HTML
         *    - %variable: escape text and theme as a placeholder for user-submitted
         *      content
         * @return string Translated string
         * Translated string
         */
        virtual std::string t(const std::string &message, const std::map<std::string,std::string> &replacements = {}) = 0;

        /**
         * Get URL to file in the specific library
         * @param string $libraryFolderName
         * @param string $fileName
         * @return string URL to file
         */
        virtual std::string getLibraryFileUrl(const std::string &libraryFolderName, const std::string &fileName) = 0;

        /**
         * Get the Path to the last uploaded h5p
         *
         * @return string
         *   Path to the folder where the last uploaded h5p for this session is located.
         */
        virtual std::string getUploadedH5pFolderPath() {
            if (uploadedH5pTmpDir) {
                return uploadedH5pTmpDir.value();
            } else {
                throw std::exception(); // Unexpected call
            }
        };

        /**
         * Get the path to the last uploaded h5p file
         *
         * @return string
         *   Path to the last uploaded h5p
         */
        virtual std::string getUploadedH5pPath() {
            if (uploadedH5p) {
                return uploadedH5p.value();
            } else {
                throw std::exception(); // Unexpected call
            }
        };

        /**
         * Load addon libraries
         *
         * @return array
         */
        virtual std::vector<h5p::library_addon> loadAddons() = 0;

        /**
         * Load config for libraries
         *
         * @param array $libraries
         * @return array
         */
        virtual void getLibraryConfig(const std::vector<std::string> &libraries = {}) {
            /* Not used, not implemented */
        }

        /**
         * Get a list of the current installed libraries
         *
         * @return array
         *   Associative array containing one entry per machine name.
         *   For each machineName there is a list of libraries(with different versions)
         */
        virtual std::map<std::string,std::vector<h5p::library_load>> loadLibraries() = 0;

        /**
         * Returns the URL to the library admin page
         *
         * @return string
         *   URL to admin page
         */
        virtual std::optional<std::string> getAdminUrl() {
            return {};
        };

        /**
         * Get id to an existing library.
         * If version number is not specified, the newest version will be returned.
         *
         * @param string $machineName
         *   The librarys machine name
         * @param int $majorVersion
         *   Optional major version number for library
         * @param int $minorVersion
         *   Optional minor version number for library
         * @return int
         *   The id of the specified library or FALSE
         */
        virtual std::optional<unsigned int> getLibraryId(const std::string &machineName, std::optional<unsigned int> majorVersion = {}, std::optional<unsigned int> minorVersion = {}) = 0;

        /**
         * Get file extension whitelist
         *
         * The default extension list is part of h5p, but admins should be allowed to modify it
         *
         * @param boolean $isLibrary
         *   TRUE if this is the whitelist for a library. FALSE if it is the whitelist
         *   for the content folder we are getting
         * @param string $defaultContentWhitelist
         *   A string of file extensions separated by whitespace
         * @param string $defaultLibraryWhitelist
         *   A string of file extensions separated by whitespace
         */
        virtual std::string getWhitelist(bool isLibrary, const std::string &defaultContentWhitelist, const std::string &defaultLibraryWhitelist) {
            std::string allowlist(defaultContentWhitelist);
            if (isLibrary) {
                allowlist.append(" ");
                allowlist.append(defaultLibraryWhitelist);
            }
            return allowlist;
        }

        /**
         * Is the library a patched version of an existing library?
         *
         * @param object $library
         *   An associative array containing:
         *   - machineName: The library machineName
         *   - majorVersion: The librarys majorVersion
         *   - minorVersion: The librarys minorVersion
         *   - patchVersion: The librarys patchVersion
         * @return boolean
         *   TRUE if the library is a patched version of an existing library
         *   FALSE otherwise
         */
        virtual bool isPatchedLibrary(const h5p::library_with_version &libraryWithVersion) = 0;

        /**
         * Is H5P in development mode?
         *
         * @return boolean
         *  TRUE if H5P development mode is active
         *  FALSE otherwise
         */
        virtual bool isInDevMode() = 0;

        /**
         * Is the current user allowed to update libraries?
         *
         * @return boolean
         *  TRUE if the user is allowed to update libraries
         *  FALSE if the user is not allowed to update libraries
         */
        virtual bool mayUpdateLibraries() = 0;

        /**
         * Store data about a library
         *
         * Also fills in the libraryId in the libraryData object if the object is new
         *
         * @param object $libraryData
         *   Associative array containing:
         *   - libraryId: The id of the library if it is an existing library.
         *   - title: The library's name
         *   - machineName: The library machineName
         *   - majorVersion: The library's majorVersion
         *   - minorVersion: The library's minorVersion
         *   - patchVersion: The library's patchVersion
         *   - runnable: 1 if the library is a content type, 0 otherwise
         *   - metadataSettings: Associative array containing:
         *      - disable: 1 if the library should not support setting metadata (copyright etc)
         *      - disableExtraTitleField: 1 if the library don't need the extra title field
         *   - fullscreen(optional): 1 if the library supports fullscreen, 0 otherwise
         *   - embedTypes(optional): list of supported embed types
         *   - preloadedJs(optional): list of associative arrays containing:
         *     - path: path to a js file relative to the library root folder
         *   - preloadedCss(optional): list of associative arrays containing:
         *     - path: path to css file relative to the library root folder
         *   - dropLibraryCss(optional): list of associative arrays containing:
         *     - machineName: machine name for the librarys that are to drop their css
         *   - semantics(optional): Json describing the content structure for the library
         *   - language(optional): associative array containing:
         *     - languageCode: Translation in json format
         * @param bool $new
         * @return
         */
        virtual void saveLibraryData(h5p::save_library_data &libraryData, bool newLib = true) = 0;

        /**
         * Insert new content.
         *
         * @param array $content
         *   An associative array containing:
         *   - id: The content id (wrong - it is not included)
         *   - params: The content in json format
         *   - library: An associative array containing:
         *     - libraryId: The id of the main library for this content
         * @param int $contentMainId
         *   Main id for the content if this is a system that supports versions
         */
        virtual unsigned int insertContent(const h5p::content_data &content, std::optional<unsigned int> contentMainId = {}) = 0;

        /**
         * Update old content.
         *
         * @param array $content
         *   An associative array containing:
         *   - id: The content id
         *   - params: The content in json format
         *   - library: An associative array containing:
         *     - libraryId: The id of the main library for this content
         * @param int $contentMainId
         *   Main id for the content if this is a system that supports versions
         */
        virtual void updateContent(const h5p::content_data &content, std::optional<unsigned int> contentMainId = {}) = 0;

        /**
         * Resets marked user data for the given content.
         *
         * @param int $contentId
         */
        virtual void resetContentUserData(unsigned int contentId) = 0;

        /**
         * Save what libraries a library is depending on
         *
         * @param int $libraryId
         *   Library Id for the library we're saving dependencies for
         * @param array $dependencies
         *   List of dependencies as associative arrays containing:
         *   - machineName: The library machineName
         *   - majorVersion: The library's majorVersion
         *   - minorVersion: The library's minorVersion
         * @param string $dependency_type
         *   What type of dependency this is, the following values are allowed:
         *   - editor
         *   - preloaded
         *   - dynamic
         */
        virtual void saveLibraryDependencies(unsigned int libraryId, const std::vector<h5p::library_without_patch_version> &dependencies, h5p::dependency_type dependency_type) = 0;

        /**
         * Give an H5P the same library dependencies as a given H5P
         *
         * @param int $contentId
         *   Id identifying the content
         * @param int $copyFromId
         *   Id identifying the content to be copied
         * @param int $contentMainId
         *   Main id for the content, typically used in frameworks
         *   That supports versions. (In this case the content id will typically be
         *   the version id, and the contentMainId will be the frameworks content id
         */
        virtual void copyLibraryUsage(unsigned int contentId, unsigned int copyFromId, std::optional<unsigned int> contentMainId = {}) = 0;

        /**
         * Deletes content data
         *
         * @param int $contentId
         *   Id identifying the content
         */
        virtual void deleteContentData(unsigned int contentId) = 0;

        /**
         * Delete what libraries a content item is using
         *
         * @param int $contentId
         *   Content Id of the content we'll be deleting library usage for
         */
        virtual void deleteLibraryUsage(unsigned int contentId) = 0;

        /**
         * Saves what libraries the content uses
         *
         * @param int $contentId
         *   Id identifying the content
         * @param array $librariesInUse
         *   List of libraries the content uses. Libraries consist of associative arrays with:
         *   - library: Associative array containing:
         *     - dropLibraryCss(optional): comma separated list of machineNames
         *     - machineName: Machine name for the library
         *     - libraryId: Id of the library
         *   - type: The dependency type. Allowed values:
         *     - editor
         *     - dynamic
         *     - preloaded
         */
        virtual void saveLibraryUsage(unsigned int contentId, const std::vector<h5p::library_dependency> &librariesInUse) = 0;

        /**
         * Get number of content/nodes using a library, and the number of
         * dependencies to other libraries
         *
         * @param int $libraryId
         *   Library identifier
         * @param boolean $skipContent
         *   Flag to indicate if content usage should be skipped
         * @return array
         *   Associative array containing:
         *   - content: Number of content using the library
         *   - libraries: Number of libraries depending on the library
         */
        virtual h5p::library_usage getLibraryUsage(unsigned int libraryId, bool skipContent = false) = 0;

        /**
         * Loads a library
         *
         * @param string $machineName
         *   The library's machine name
         * @param int $majorVersion
         *   The library's major version
         * @param int $minorVersion
         *   The library's minor version
         * @return array|FALSE
         *   FALSE if the library does not exist.
         *   Otherwise an associative array containing:
         *   - libraryId: The id of the library if it is an existing library.
         *   - title: The library's name
         *   - machineName: The library machineName
         *   - majorVersion: The library's majorVersion
         *   - minorVersion: The library's minorVersion
         *   - patchVersion: The library's patchVersion
         *   - runnable: 1 if the library is a content type, 0 otherwise
         *   - fullscreen(optional): 1 if the library supports fullscreen, 0 otherwise
         *   - embedTypes(optional): list of supported embed types
         *   - preloadedJs(optional): comma separated string with js file paths
         *   - preloadedCss(optional): comma separated sting with css file paths
         *   - dropLibraryCss(optional): list of associative arrays containing:
         *     - machineName: machine name for the librarys that are to drop their css
         *   - semantics(optional): Json describing the content structure for the library
         *   - preloadedDependencies(optional): list of associative arrays containing:
         *     - machineName: Machine name for a library this library is depending on
         *     - majorVersion: Major version for a library this library is depending on
         *     - minorVersion: Minor for a library this library is depending on
         *   - dynamicDependencies(optional): list of associative arrays containing:
         *     - machineName: Machine name for a library this library is depending on
         *     - majorVersion: Major version for a library this library is depending on
         *     - minorVersion: Minor for a library this library is depending on
         *   - editorDependencies(optional): list of associative arrays containing:
         *     - machineName: Machine name for a library this library is depending on
         *     - majorVersion: Major version for a library this library is depending on
         *     - minorVersion: Minor for a library this library is depending on
         */
        virtual std::optional<h5p::load_library_data> loadLibrary(const std::string &machineName, unsigned int majorVersion, unsigned int minorVersion) = 0;

        /**
         * Loads library semantics.
         *
         * @param string $machineName
         *   Machine name for the library
         * @param int $majorVersion
         *   The library's major version
         * @param int $minorVersion
         *   The library's minor version
         * @return string
         *   The library's semantics as json
         */
        virtual std::optional<std::string> loadLibrarySemantics(const std::string &machineName, unsigned int majorVersion, unsigned int minorVersion) = 0;

        /**
         * Makes it possible to alter the semantics, adding custom fields, etc.
         *
         * @param array $semantics
         *   Associative array representing the semantics
         * @param string $machineName
         *   The library's machine name
         * @param int $majorVersion
         *   The library's major version
         * @param int $minorVersion
         *   The library's minor version
         */
        // Returns optional modified json semantics
        virtual std::optional<h5p::json_modifications> alterLibrarySemantics(const web::json::value &semantics, const std::string &machineName, unsigned int majorVersion, unsigned int minorVersion) {
            return {};
        }

        /**
         * Delete all dependencies belonging to given library
         *
         * @param int $libraryId
         *   Library identifier
         */
        virtual void deleteLibraryDependencies(unsigned int libraryId) = 0;

        /**
         * Start an atomic operation against the dependency storage
         */
        virtual void lockDependencyStorage() = 0;

        /**
         * Stops an atomic operation against the dependency storage
         */
        virtual void unlockDependencyStorage() = 0;


        /**
         * Delete a library from database and file system
         *
         * @param stdClass $library
         *   Library object with id, name, major version and minor version.
         */
        virtual void deleteLibrary(unsigned int libraryId) = 0;

        /**
         * Load content.
         *
         * @param int $id
         *   Content identifier
         * @return array
         *   Associative array containing:
         *   - contentId: Identifier for the content
         *   - params: json content as string
         *   - embedType: csv of embed types
         *   - title: The contents title
         *   - language: Language code for the content
         *   - libraryId: Id for the main library
         *   - libraryName: The library machine name
         *   - libraryMajorVersion: The library's majorVersion
         *   - libraryMinorVersion: The library's minorVersion
         *   - libraryEmbedTypes: CSV of the main library's embed types
         *   - libraryFullscreen: 1 if fullscreen is supported. 0 otherwise.
         */
        virtual std::optional<h5p::content_load> loadContent(unsigned int id) = 0;

        /**
         * Load dependencies for the given content of the given type.
         *
         * @param int $id
         *   Content identifier
         * @param int $type
         *   Dependency types. Allowed values:
         *   - editor
         *   - preloaded
         *   - dynamic
         * @return array
         *   List of associative arrays containing:
         *   - libraryId: The id of the library if it is an existing library.
         *   - machineName: The library machineName
         *   - majorVersion: The library's majorVersion
         *   - minorVersion: The library's minorVersion
         *   - patchVersion: The library's patchVersion
         *   - preloadedJs(optional): comma separated string with js file paths
         *   - preloadedCss(optional): comma separated sting with css file paths
         *   - dropCss(optional): csv of machine names
         */
        virtual std::vector<h5p::library_dependency_load> loadContentDependencies(unsigned int id, std::optional<h5p::dependency_type> type = {}) = 0;

        /**
         * Get stored setting.
         *
         * @param string $name
         *   Identifier for the setting
         * @param string $default
         *   Optional default value if settings is not set
         * @return mixed
         *   Whatever has been stored as the setting
         */
        virtual std::optional<std::string> getOption(const std::string &name) = 0;

        /**
         * Stores the given setting.
         * For example when did we last check h5p.org for updates to our libraries.
         *
         * @param string $name
         *   Identifier for the setting
         * @param mixed $value Data
         *   Whatever we want to store as the setting
         */
        virtual void setOption(const std::string &name, const std::optional<std::string> &value) = 0;

        /**
         * This will update selected fields on the given content.
         *
         * @param int $id Content identifier
         * @param array $fields Content fields, e.g. filtered or slug.
         */
        virtual void updateContentFields(unsigned int id, const h5p::update_content_fields &fields) = 0;

        /**
         * Will clear filtered params for all the content that uses the specified
         * libraries. This means that the content dependencies will have to be rebuilt,
         * and the parameters re-filtered.
         *
         * @param array $library_ids
         */
        virtual void clearFilteredParameters(std::vector<unsigned int> library_ids) = 0;

        /**
         * Get number of contents that has to get their content dependencies rebuilt
         * and parameters re-filtered.
         *
         * @return int
         */
        virtual unsigned int getNumNotFiltered() = 0;

        /**
         * Get number of contents using library as main library.
         *
         * @param int $libraryId
         * @param array $skip
         * @return int
         */
        virtual unsigned int getNumContent(unsigned int libraryId, std::vector<unsigned int> skip = {}) = 0;

        /**
         * Determines if content slug is used.
         *
         * @param string $slug
         * @return boolean
         */
        virtual bool isContentSlugAvailable(const std::string &slug) = 0;

        /**
         * Generates statistics from the event log per library
         *
         * @param string $type Type of event to generate stats for
         * @return array Number values indexed by library name and version
         */
        // Apparently not critical
        //public function getLibraryStats($type);

        /**
         * Aggregate the current number of H5P authors
         * @return int
         */
        virtual unsigned int getNumAuthors() {
            return 0;
        };

        /**
         * Stores hash keys for cached assets, aggregated JavaScripts and
         * stylesheets, and connects it to libraries so that we know which cache file
         * to delete when a library is updated.
         *
         * @param string $key
         *  Hash key for the given libraries
         * @param array $libraries
         *  List of dependencies(libraries) used to create the key
         */
        virtual void saveCachedAssets(const std::string &key, std::vector<unsigned int> libraries) = 0;

        /**
         * Locate hash keys for given library and delete them.
         * Used when cache file are deleted.
         *
         * @param int $library_id
         *  Library identifier
         * @return array
         *  List of hash keys removed
         */
        virtual std::vector<std::string> deleteCachedAssets(unsigned int library_id) = 0;

        /**
         * Get the amount of content items associated to a library
         * return int
         */
        virtual std::vector<std::tuple<h5p::library_without_patch_version,unsigned int>> getLibraryContentCount() = 0;

        /**
         * Will trigger after the export file is created.
         */
        virtual void afterExportCreated(unsigned int content_id, const std::string &filename) = 0;

        /**
         * Check if user has permissions to an action
         *
         * @method hasPermission
         * @param  [H5PPermission] $permission Permission type, ref H5PPermission
         * @param  [int]           $id         Id need by platform to determine permission
         * @return boolean
         */
        virtual bool hasPermission(h5p::permission permission, std::optional<unsigned int> id = {}) = 0;

        /**
         * Replaces existing content type cache with the one passed in
         *
         * @param object $contentTypeCache Json with an array called 'libraries'
         *  containing the new content type cache that should replace the old one.
         */
        virtual void replaceContentTypeCache(const std::vector<h5p::hub_content_type> &contentTypeCache) = 0;

        /**
         * Checks if the given library has a higher version.
         *
         * @param array $library
         * @return boolean
         */
        virtual bool libraryHasUpgrade(const h5p::library_with_version &library) {
            return false;
        };
};

#endif //H5PRUNNER_H5PPLATFORMINTERFACE_H
