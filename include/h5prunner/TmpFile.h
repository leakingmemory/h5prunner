//
// Created by janeover on 18.11.2020.
//

#ifndef H5PRUNNER_TMPFILE_H
#define H5PRUNNER_TMPFILE_H

#include <optional>
#include <string>

class TmpFile {
private:
    std::optional<std::string> path;
public:
    TmpFile(const void *ptr, size_t size);
    ~TmpFile();
    void RenameWithSuffix(const std::string &suffix);
    std::string GetPath();
};

#endif //H5PRUNNER_TMPFILE_H
