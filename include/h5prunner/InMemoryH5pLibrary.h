//
// Created by janeover on 02.11.2020.
//

#ifndef H5PRUNNER_INMEMORYH5PLIBRARY_H
#define H5PRUNNER_INMEMORYH5PLIBRARY_H

#include <h5prunner/H5pPlatformInterface.h>

class InMemoryH5pLibrary {
private:
    h5p::save_library_data library_data;
    h5p::dependency_data  dependencies;
    std::optional<std::string> tutorial_url;
public:
    InMemoryH5pLibrary(InMemoryH5pLibrary &&mv) noexcept;
    InMemoryH5pLibrary(const h5p::save_library_data &save);
    std::string GetMachineName();
    unsigned int GetMajorVersion();
    unsigned int GetMinorVersion();
    int GetId();
    void update(const h5p::save_library_data &update);
    void SetTutorialUrl(const std::string &tutorial_url);
    std::optional<std::string> GetTutorialUrl();
    std::optional<h5p::library_addon> GetAddOn();
    h5p::load_library_data GetLoadLibraryData();
    h5p::library_load  GetLibraryLoad();
    void DeleteDependencies();
    void UpdateDependencies(const std::vector<h5p::library_without_patch_version> &dependencies,
                            h5p::dependency_type dependency_type);
    const h5p::dependency_data &GetDependencyData();
    void LoadLibraryDependency(h5p::library_dependency_load &ldl);
    std::optional<std::string> GetSemantics();
    void LoadContent(h5p::content_load &cl);

    InMemoryH5pLibrary &operator=(const InMemoryH5pLibrary& assign) {
        this->library_data = assign.library_data;
        return *this;
    }
};


#endif //H5PRUNNER_INMEMORYH5PLIBRARY_H
