//
// Created by janeover on 01.11.2020.
//

#ifndef H5PRUNNER_H5PMEMORYPLATFORM_H
#define H5PRUNNER_H5PMEMORYPLATFORM_H

#include <h5prunner/H5pPlatformInterface.h>
#include <h5prunner/InMemoryH5pLibrary.h>
#include <h5prunner/InMemoryContent.h>

class H5pMemoryPlatform : public H5PFrameworkInterface {
private:
    std::string filesBaseUri;

    std::mutex mutex;
    std::map<unsigned int,InMemoryH5pLibrary> libraries;
    std::map<std::string,std::map<unsigned int,std::map<unsigned int,std::map<unsigned int,unsigned int>>>> machineNameVersionMap;
    std::map<unsigned int,InMemoryContent> content;
    std::vector<std::tuple<std::string,std::optional<std::string>>> errors;
    std::vector<std::string> infoMessages;
    std::map<std::string,std::string> config;
    std::map<unsigned int,std::vector<std::string>> contentHashes;
    std::vector<h5p::hub_content_type> contentTypeCache;

    bool may_update_libraries;
public:
    explicit H5pMemoryPlatform(const std::string &filesBaseUri);
    ~H5pMemoryPlatform() override;

    std::tuple<std::string,std::string,std::string> getPlatformInfo() override;
    void setLibraryTutorialUrl(const std::string &machineName, const std::string &tutorialUrl) override;
    void setErrorMessage(const std::string &message, const std::optional<std::string> &code = {}) override;
    void setInfoMessage(const std::string &message) override;
    std::vector<std::string> getMessages(h5p::message_type type) override;
    std::string t(const std::string &message, const std::map<std::string,std::string> &replacements = {}) override;
    std::string getLibraryFileUrl(const std::string &libraryFolderName, const std::string &fileName) override;
    std::vector<h5p::library_addon> loadAddons() override;
    std::map<std::string,std::vector<h5p::library_load>> loadLibraries() override;
    std::optional<unsigned int> getLibraryId(const std::string &machineName, std::optional<unsigned int> majorVersion = {}, std::optional<unsigned int> minorVersion = {}) override;
    bool isPatchedLibrary(const h5p::library_with_version &libraryWithVersion) override;
    bool isInDevMode() override;
    bool mayUpdateLibraries() override;
    void saveLibraryData(h5p::save_library_data &libraryData, bool newLib = true) override;
    unsigned int insertContent(const h5p::content_data &content, std::optional<unsigned int> contentMainId = {}) override;
    void updateContent(const h5p::content_data &content, std::optional<unsigned int> contentMainId = {}) override;
    void resetContentUserData(unsigned int contentId) override;
    void saveLibraryDependencies(unsigned int libraryId, const std::vector<h5p::library_without_patch_version> &dependencies, h5p::dependency_type dependency_type) override;
    void copyLibraryUsage(unsigned int contentId, unsigned int copyFromId, std::optional<unsigned int> contentMainId = {}) override;
    void deleteContentData(unsigned int contentId) override;
    void deleteLibraryUsage(unsigned intcontentId) override;
    void saveLibraryUsage(unsigned int contentId, const std::vector<h5p::library_dependency> &librariesInUse) override;
    h5p::library_usage getLibraryUsage(unsigned int libraryId, bool skipContent = false) override;
    std::optional<h5p::load_library_data> loadLibrary(const std::string &machineName, unsigned int majorVersion, unsigned int minorVersion) override;
    std::optional<std::string> loadLibrarySemantics(const std::string &machineName, unsigned int majorVersion, unsigned int minorVersion) override;
    void deleteLibraryDependencies(unsigned int libraryId) override;
    void lockDependencyStorage() override;
    void unlockDependencyStorage() override;
    void deleteLibrary(unsigned int libraryId) override;
    std::optional<h5p::content_load> loadContent(unsigned int id) override;
    std::vector<h5p::library_dependency_load> loadContentDependencies(unsigned int id, std::optional<h5p::dependency_type> type = {}) override;
    std::optional<std::string> getOption(const std::string &name) override;
    void setOption(const std::string &name, const std::optional<std::string> &value) override;
    void updateContentFields(unsigned int id, const h5p::update_content_fields &fields) override;
    void clearFilteredParameters(std::vector<unsigned int> library_ids) override;
    unsigned int getNumNotFiltered() override;
    unsigned int getNumContent(unsigned int libraryId, std::vector<unsigned int> skip = {}) override;
    bool isContentSlugAvailable(const std::string &slug) override;
    void saveCachedAssets(const std::string &key, std::vector<unsigned int> libraries) override;
    std::vector<std::string> deleteCachedAssets(unsigned int library_id) override;
    std::vector<std::tuple<h5p::library_without_patch_version,unsigned int>> getLibraryContentCount() override;
    void afterExportCreated(unsigned int content_id, const std::string &filename) override;
    bool hasPermission(h5p::permission permission, std::optional<unsigned int> id = {}) override;
    void replaceContentTypeCache(const std::vector<h5p::hub_content_type> &contentTypeCache) override;

    void SetMayUpdateLibraries(bool may_upd);
    InMemoryContent *getContent(unsigned int content_id);
    InMemoryH5pLibrary *getLibrary(unsigned int library_id);
};


#endif //H5PRUNNER_H5PMEMORYPLATFORM_H
