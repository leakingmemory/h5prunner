//
// Created by janeover on 01.11.2020.
//

#ifndef H5PRUNNER_H5PPHPBINDINGS_H
#define H5PRUNNER_H5PPHPBINDINGS_H

#include <php.h>
#include <zend_exceptions.h>

#include <h5prunner/H5pPlatformInterface.h>

class H5pPhpBindings {
private:
    H5PFrameworkInterface &h5pFrameworkInterface;
    std::vector<std::string> command;
public:
    H5pPhpBindings(H5PFrameworkInterface &h5pFrameworkInterface);
    ~H5pPhpBindings();

    H5PFrameworkInterface *framework_interface();
    const zend_function_entry *zend_function_entries();

    void pushCommand(const std::string &cmd);
    std::string popCommand();
};


#endif //H5PRUNNER_H5PPHPBINDINGS_H
