//
// Created by janeover on 17.11.2020.
//

#ifndef H5PRUNNER_PHPUTILS_H
#define H5PRUNNER_PHPUTILS_H

#include <Zend/zend_types.h>
#include <cpprest/json.h>

web::json::value zval_to_json(zval *zv);

#define ZSTR_STD(zstr) std::string(ZSTR_VAL(zstr), ZSTR_LEN(zstr))

#endif //H5PRUNNER_PHPUTILS_H
