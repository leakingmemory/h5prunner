//
// Created by janeover on 01.11.2020.
//

#ifndef H5PRUNNER_H5PEXTERNALDATA_H
#define H5PRUNNER_H5PEXTERNALDATA_H

#include <cpprest/http_client.h>
#include <string>
#include <optional>

class H5pExternalData {
private:
    web::http::client::http_client client;
    std::optional<std::map<std::string,std::vector<std::string>>> data;
public:
    H5pExternalData(H5pExternalData &&mvConstr) noexcept ;
    H5pExternalData(const std::string &url, const std::optional<std::map<std::string,std::vector<std::string>>> &data = {});
    pplx::task<std::optional<utf8string>> async();
    std::optional<std::string> download();
};


#endif //H5PRUNNER_H5PEXTERNALDATA_H
