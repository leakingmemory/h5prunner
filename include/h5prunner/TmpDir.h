//
// Created by janeover on 15.11.2020.
//

#ifndef H5PRUNNER_TMPDIR_H
#define H5PRUNNER_TMPDIR_H

#include <string>
#include <optional>

class TmpDir {
private:
    std::optional<std::string> path;
public:
    TmpDir();
    ~TmpDir();
    std::string GetPath();
};


#endif //H5PRUNNER_TMPDIR_H
