//
// Created by janeover on 06.11.2020.
//

#include <h5prunner/InMemoryContent.h>
#include "InMemorySerial.h"

InMemoryContent::InMemoryContent(const h5p::content_data &content, std::optional<unsigned int> content_main_id) : content(content), content_main_id(content_main_id), filtered() {
    this->content.id = serial();
}

InMemoryContent::InMemoryContent(const InMemoryContent &cp) : content(cp.content) {
}

unsigned int InMemoryContent::GetId() {
    return content.id;
}

void InMemoryContent::Update(const h5p::content_data &content, std::optional<unsigned int> content_main_id) {
    this->content.json = content.json;
    this->content.library = content.library;
    this->content.title = content.title;
    this->content.metadata = content.metadata;
    this->content.embed_type = content.embed_type;
    this->content.disable = content.disable;
    this->content.max_score = content.max_score;
    this->content.slug = content.slug;
    this->content.user_id = content.user_id;
    this->content.is_published = content.is_published;
    this->content.is_private = content.is_private;
    this->content.language_iso_639_3 = content.language_iso_639_3;
    this->content_main_id = content_main_id;
}

void InMemoryContent::UpdateLibdeps(const std::vector<h5p::library_dependency> &libdeps) {
    this->libdeps = libdeps;
}

std::vector<h5p::library_dependency_load> InMemoryContent::LoadLibdeps(std::optional<h5p::dependency_type> type) {
    std::vector<h5p::library_dependency_load> libdeps = {};
    for (const auto &libdep : this->libdeps) {
        if (!type || type.value() == libdep.type) {
            h5p::library_dependency_load libd = {};
            libd.libraryId = libdep.library_id;
            if (libdep.drop_css) {
                libd.dropCss = {{std::string("Y")}};
            }
            libdeps.push_back(libd);
        }
    }
    return libdeps;
}

const std::vector<h5p::library_dependency> &InMemoryContent::GetLibdeps() {
    return libdeps;
}

unsigned int InMemoryContent::GetLibraryId() {
    return content.library;
}

h5p::content_load InMemoryContent::CreateContentLoad() {
    h5p::content_load cl = {};
    cl.contentId = content.id;
    cl.libraryId = content.library;
    cl.title = content.title;
    cl.json = content.json;
    cl.embedType = content.embed_type;
    cl.language = content.language_iso_639_3;
    cl.user_id = content.user_id;
    cl.slug = content.slug;
    cl.max_score = content.max_score;
    cl.disable = content.disable;
    cl.metadata = content.metadata;
    return cl;
}

void InMemoryContent::UpdateFields(const h5p::update_content_fields &fields) {
    if (fields.title) {
        content.title = fields.title.value();
    }
    if (fields.slug) {
        content.slug = fields.slug.value();
    }
    if (fields.filtered) {
        filtered = fields.filtered.value();
    }
}

void InMemoryContent::ClearFiltered() {
    filtered = {};
}

std::optional<std::string> InMemoryContent::GetFiltered() {
    return filtered;
}

std::optional<std::string> InMemoryContent::GetSlug() {
    return content.slug;
}
