//
// Created by janeover on 02.11.2020.
//

#include <h5prunner/InMemoryH5pLibrary.h>
#include "InMemorySerial.h"

InMemoryH5pLibrary::InMemoryH5pLibrary(const h5p::save_library_data &save) : library_data(save) {
    library_data.libraryId = serial();
}

int InMemoryH5pLibrary::GetId() {
    return library_data.libraryId;
}

InMemoryH5pLibrary::InMemoryH5pLibrary(InMemoryH5pLibrary &&mv) noexcept : library_data(std::move(mv.library_data)) {
}

void InMemoryH5pLibrary::update(const h5p::save_library_data &update) {
    if (library_data.machineName.compare(update.machineName) != 0 ||
        library_data.majorVersion != update.majorVersion ||
        library_data.minorVersion != update.minorVersion ||
        library_data.patchVersion != update.patchVersion
    ) {
        /*
         * Changing machine name or version has side effects. Disallow change.
         */
        throw std::exception();
    }
    h5p::save_library_data new_data(update);
    new_data.libraryId = library_data.libraryId;
    library_data = std::move(new_data);
}

void InMemoryH5pLibrary::SetTutorialUrl(const std::string &tutorial_url) {
    this->tutorial_url = tutorial_url;
}

std::optional<std::string> InMemoryH5pLibrary::GetTutorialUrl() {
    return tutorial_url;
}

std::string InMemoryH5pLibrary::GetMachineName() {
    return library_data.machineName;
}

std::optional<h5p::library_addon> InMemoryH5pLibrary::GetAddOn() {
    if (library_data.addTo) {
        h5p::library_addon addon;
        addon.libraryId = library_data.libraryId;
        addon.machineName = library_data.machineName;
        addon.majorVersion = library_data.majorVersion;
        addon.minorVersion = library_data.minorVersion;
        addon.patchVersion = library_data.patchVersion;
        addon.addTo = library_data.addTo.value();
        addon.preloadedCss = library_data.preloadedCss;
        addon.preloadedJs = library_data.preloadedJs;
        return addon;
    } else {
        return {};
    }
}

h5p::load_library_data InMemoryH5pLibrary::GetLoadLibraryData() {
    h5p::load_library_data load;
    load.library = library_data;
    load.dependencies = dependencies;
    return load;
}

h5p::library_load InMemoryH5pLibrary::GetLibraryLoad() {
    h5p::library_load ll;
    ll.id = library_data.libraryId;
    ll.name = library_data.machineName;
    ll.major_version = library_data.majorVersion;
    ll.minor_version = library_data.minorVersion;
    ll.patch_version = library_data.patchVersion;
    ll.title = library_data.title;
    ll.runnable = library_data.runnable;
    ll.restricted = false; // TODO - Undocumented feature
    return ll;
}

void InMemoryH5pLibrary::UpdateDependencies(const std::vector<h5p::library_without_patch_version> &dependencies,
                                            h5p::dependency_type dependency_type) {
    switch (dependency_type) {
        case h5p::editor:
            this->dependencies.editorDependencies = dependencies;
            break;
        case h5p::preloaded:
            this->dependencies.preloadedDependencies = dependencies;
            break;
        case h5p::dynamic:
            this->dependencies.dynamicDependencies = dependencies;
            break;
        default:
            throw std::exception();
    }
}

void InMemoryH5pLibrary::LoadLibraryDependency(h5p::library_dependency_load &ldl) {
    ldl.machineName = library_data.machineName;
    ldl.majorVersion = library_data.majorVersion;
    ldl.minorVersion = library_data.minorVersion;
    ldl.patchVersion = library_data.patchVersion;
    ldl.preloadedJs = library_data.preloadedJs;
    ldl.preloadedCss = library_data.preloadedCss;
    if (ldl.dropCss) {
        ldl.dropCss = {ldl.machineName};
    }
}

const h5p::dependency_data &InMemoryH5pLibrary::GetDependencyData() {
    return dependencies;
}

std::optional<std::string> InMemoryH5pLibrary::GetSemantics() {
    return library_data.semantics;
}

void InMemoryH5pLibrary::DeleteDependencies() {
    dependencies = {};
}

void InMemoryH5pLibrary::LoadContent(h5p::content_load &cl) {
    cl.libraryName = library_data.machineName;
    cl.libraryMajorVersion = library_data.majorVersion;
    cl.libraryMinorVersion = library_data.minorVersion;
    cl.libraryEmbedTypes = library_data.embedTypes;
    cl.libraryFullscreen = library_data.fullscreen;
}

unsigned int InMemoryH5pLibrary::GetMajorVersion() {
    return library_data.majorVersion;
}

unsigned int InMemoryH5pLibrary::GetMinorVersion() {
    return library_data.minorVersion;
}
