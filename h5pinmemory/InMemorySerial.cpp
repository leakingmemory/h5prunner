//
// Created by janeover on 06.11.2020.
//

#include <mutex>

#include "InMemorySerial.h"

static std::mutex serialLock;
static unsigned int serialCount = 0;

unsigned int serial() {
    std::unique_lock lock(serialLock);
    return ++serialCount;
}