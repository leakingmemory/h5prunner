//
// Created by janeover on 01.11.2020.
//

#include <boost/algorithm/string.hpp>
#include <h5prunner/htmlescape.h>
#include <h5prunner/H5pMemoryPlatform.h>

H5pMemoryPlatform::H5pMemoryPlatform(const std::string &filesBaseUri) :
        filesBaseUri(filesBaseUri),
        may_update_libraries(true)
{

}

H5pMemoryPlatform::~H5pMemoryPlatform() {

}

std::tuple<std::string, std::string, std::string> H5pMemoryPlatform::getPlatformInfo() {
    return {"C++ In Memory Implementation", "0.1", "0.1"};
}

void H5pMemoryPlatform::setLibraryTutorialUrl(const std::string &machineName, const std::string &tutorialUrl) {
    std::unique_lock lock(mutex);
    for (auto &[id, lib] : libraries) {
        if (lib.GetMachineName() == machineName) {
            lib.SetTutorialUrl(tutorialUrl);
        }
    }
}

void H5pMemoryPlatform::setErrorMessage(const std::string &message, const std::optional<std::string> &code) {
    std::unique_lock lock(mutex);
    /*
     * C++ magic. Will automatically construct the complex object in place (tuple), avoiding uneccessary moves.
     */
    errors.emplace_back(message, code);
}

void H5pMemoryPlatform::setInfoMessage(const std::string &message) {
    std::unique_lock lock(mutex);
    infoMessages.push_back(message);
}

std::vector<std::string> H5pMemoryPlatform::getMessages(h5p::message_type type) {
    std::vector<std::string> messages;
    {
        std::unique_lock lock(mutex);
        switch (type) {
            case h5p::error:
                for (auto &msg : errors) {
                    messages.push_back(std::get<0>(msg));
                }
                errors = {};
                break;
            case h5p::info:
                messages = infoMessages;
                infoMessages = {};
        }
    }
    return messages;
}

std::string H5pMemoryPlatform::t(const std::string &message, const std::map<std::string,std::string> &replacements) {
    std::string text(message);
    std::string key;
    std::string insert;
    for (auto &[name, value] : replacements) {
        if (name.length() == 0)
            continue;
        if (name.starts_with("%") || name.starts_with("@")) {
            insert.assign(value);
            html_escape(insert);
            boost::replace_all(text, name, insert);
        } else if (name.starts_with("!")) {
            boost::replace_all(text, name, value);
        } else {
            key.assign("!");
            key.append(name);
            insert.assign(value);
            boost::replace_all(text, key, insert);
            key.assign("@");
            key.append(name);
            html_escape(insert);
            boost::replace_all(text, key, insert);
            key.assign("%");
            key.append(name);
            boost::replace_all(text, key, insert);
        }
    }
    return text;
}

std::string H5pMemoryPlatform::getLibraryFileUrl(const std::string &libraryFolderName, const std::string &fileName) {
    std::string path(filesBaseUri);
    bool baseEndsWithSlash = path.ends_with('/') || path.ends_with('\\');
    bool folderStartsWithSlash = libraryFolderName.starts_with('/') || libraryFolderName.starts_with('\\');
    bool folderEndsWithSlash = libraryFolderName.ends_with('/') || libraryFolderName.ends_with('\\');
    bool filenameStartsWithSlash = fileName.starts_with('/') || fileName.starts_with('\\');
    if (baseEndsWithSlash) {
        if (folderStartsWithSlash) {
            path.resize(path.length() - 1);
        }
    } else if (!folderStartsWithSlash) {
        path.append("/");
    }
    path.append(libraryFolderName);
    if (folderEndsWithSlash) {
        if (filenameStartsWithSlash) {
            path.resize(path.length() - 1);
        }
    } else if (!filenameStartsWithSlash) {
        path.append("/");
    }
    path.append(fileName);
    boost::replace_all(path, "\\", "/");
    return path;
}

std::vector<h5p::library_addon> H5pMemoryPlatform::loadAddons() {
    std::unique_lock lock(mutex);
    std::vector<h5p::library_addon> addons;
    for (auto &[id, lib] : libraries) {
        auto addon = lib.GetAddOn();
        if (addon) {
            addons.push_back(addon.value());
        }
    }
    return addons;
}

std::map<std::string, std::vector<h5p::library_load>> H5pMemoryPlatform::loadLibraries() {
    std::map<std::string, std::vector<h5p::library_load>> ll_map;
    std::unique_lock lock(mutex);
    for (auto &[machineName, v1] : machineNameVersionMap) {
        std::vector<h5p::library_load> loads_f_m;
        for (auto &[major_v, v2] : v1) {
            for (auto &[minor_v, v3] : v2) {
                for (auto &[patch_v, id] : v3) {
                    auto lf = libraries.find(id);
                    if (lf != libraries.end()) {
                        loads_f_m.push_back(lf->second.GetLibraryLoad());
                    }
                }
            }
        }
        ll_map.insert_or_assign(machineName, loads_f_m);
    }
    return ll_map;
}

std::optional<unsigned int> H5pMemoryPlatform::getLibraryId(const std::string &machineName, std::optional<unsigned int> majorVersion,
                                             std::optional<unsigned int> minorVersion) {
    std::unique_lock lock(mutex);
    auto mv_map_i = machineNameVersionMap.find(machineName);
    if (mv_map_i != machineNameVersionMap.end()) {
        auto mv_map = mv_map_i->second;
        if (majorVersion) {
            auto miv_map_i = mv_map.find(majorVersion.value());
            if (miv_map_i != mv_map.end()) {
                auto miv_map = miv_map_i->second;
                if (minorVersion) {
                    auto pv_map_i = miv_map.find(minorVersion.value());
                    if (pv_map_i != miv_map.end()) {
                        auto pv_map = pv_map_i->second;
                        return pv_map.rbegin()->second;
                    }
                } else {
                    return miv_map.rbegin()->second.rbegin()->second;
                }
            }
        } else {
            return mv_map.rbegin()->second.rbegin()->second.rbegin()->second;
        }
    }
    return {};
}

bool H5pMemoryPlatform::isPatchedLibrary(const h5p::library_with_version &libraryWithVersion) {
    std::unique_lock lock(mutex);
    auto vvv_m_i = machineNameVersionMap.find(libraryWithVersion.machineName);
    if (vvv_m_i == machineNameVersionMap.end()) {
        return false;
    }
    auto vvv_m = vvv_m_i->second;
    auto vv_m_i = vvv_m.find(libraryWithVersion.majorVersion);
    if (vv_m_i == vvv_m.end()) {
        return false;
    }
    auto vv_m = vv_m_i->second;
    auto v_m_i = vv_m.find(libraryWithVersion.minorVersion);
    if (v_m_i == vv_m.end()) {
        return false;
    }
    auto v_m = v_m_i->second;
    unsigned int firstPatchVersion = v_m.begin()->first;
    return firstPatchVersion < libraryWithVersion.patchVersion;
}

bool H5pMemoryPlatform::isInDevMode() {
    return false;
}

bool H5pMemoryPlatform::mayUpdateLibraries() {
    bool may_upd;
    {
        std::unique_lock lock(mutex);
        may_upd = this->may_update_libraries;
    }
    return may_upd;
}

void H5pMemoryPlatform::saveLibraryData(h5p::save_library_data &libraryData, bool newLib) {
    std::unique_lock lock(mutex);
    if (newLib) {
        {
            auto versionMapIterator = machineNameVersionMap.find(libraryData.machineName);
            if (versionMapIterator != machineNameVersionMap.end()) {
                auto minorPatch = versionMapIterator->second.find(libraryData.majorVersion);
                if (minorPatch != versionMapIterator->second.end()) {
                    auto patch = minorPatch->second.find(libraryData.minorVersion);
                    if (patch != minorPatch->second.end()) {
                        if (patch->second.contains(libraryData.patchVersion)) {
                            throw std::exception();
                        }
                    }
                }
            } else {
                std::map<unsigned int,std::map<unsigned int,std::map<unsigned int,unsigned int>>> versionMap;
                std::map<unsigned int,std::map<unsigned int,unsigned int>> minorVersionMap;
                std::map<unsigned int,unsigned int> patchVersionMap;
                minorVersionMap.insert_or_assign(libraryData.minorVersion,std::move(patchVersionMap));
                versionMap.insert_or_assign(libraryData.majorVersion,std::move(minorVersionMap));
                machineNameVersionMap.insert_or_assign(libraryData.machineName,std::move(versionMap));
            }
        }
        InMemoryH5pLibrary library(libraryData);
        unsigned int id = library.GetId();
        libraries.insert_or_assign(id, std::move(library));
        machineNameVersionMap
            .find(libraryData.machineName)
            ->second.find(libraryData.majorVersion)
            ->second.find(libraryData.minorVersion)
            ->second.insert_or_assign(libraryData.patchVersion,id);
        libraryData.libraryId = id;
    } else {
        auto ptr = libraries.find(libraryData.libraryId);
        if (ptr == libraries.end()) {
            throw std::exception();
        }
        ptr->second.update(libraryData);
    }
}

unsigned int H5pMemoryPlatform::insertContent(const h5p::content_data &content, std::optional<unsigned int> contentMainId) {
    std::unique_lock lock(mutex);
    InMemoryContent content_obj(content, contentMainId);
    unsigned int id = content_obj.GetId();
    this->content.insert_or_assign(id, content_obj);
    return id;
}

void H5pMemoryPlatform::updateContent(const h5p::content_data &content, std::optional<unsigned int> contentMainId) {
    std::unique_lock lock(mutex);
    auto iterator = this->content.find(content.id);
    if (iterator == this->content.end()) {
        throw std::exception();
    }
    iterator->second.Update(content, contentMainId);
}

void H5pMemoryPlatform::resetContentUserData(unsigned int contentId) {
    // TODO - currently no user data to delete
}

void H5pMemoryPlatform::saveLibraryDependencies(unsigned int libraryId,
                                                const std::vector<h5p::library_without_patch_version> &dependencies,
                                                h5p::dependency_type dependency_type) {
    std::unique_lock lock(mutex);
    auto lib_i = libraries.find(libraryId);
    if (lib_i == libraries.end()) {
        throw std::exception();
    }
    auto &lib = lib_i->second;
    lib.UpdateDependencies(dependencies, dependency_type);
}

void H5pMemoryPlatform::copyLibraryUsage(unsigned int contentId, unsigned int copyFromId,
                                         std::optional<unsigned int> contentMainId) {
    std::unique_lock lock(mutex);
    auto content_i = content.find(contentId);
    auto copy_from_i = content.find(copyFromId);
    if (content_i == content.end() || copy_from_i == content.end()) {
        throw std::exception();
    }
    content_i->second.UpdateLibdeps(copy_from_i->second.GetLibdeps());
}

void H5pMemoryPlatform::deleteContentData(unsigned int contentId) {
    std::unique_lock lock(mutex);
    content.erase(contentId);
}

void H5pMemoryPlatform::deleteLibraryUsage(unsigned int contentId) {
    std::unique_lock lock(mutex);
    auto content_i = content.find(contentId);
    if (content_i == content.end()) {
        throw std::exception();
    }
    content_i->second.UpdateLibdeps({});
}

void H5pMemoryPlatform::saveLibraryUsage(unsigned int contentId,
                                         const std::vector<h5p::library_dependency> &librariesInUse) {
    std::unique_lock lock(mutex);
    auto c_i = content.find(contentId);
    if (c_i == content.end()) {
        throw std::exception();
    }
    auto &c = c_i->second;
    c.UpdateLibdeps(librariesInUse);
}

h5p::library_usage H5pMemoryPlatform::getLibraryUsage(unsigned int libraryId, bool skipContent) {
    std::unique_lock lock(mutex);
    auto look_for_lib = libraries.find(libraryId);
    if (look_for_lib == libraries.end()) {
        throw std::exception();
    }
    auto &library = look_for_lib->second;
    h5p::library_usage stats = {};

    for (auto &[id, lib] : libraries) {
        auto depdata = lib.GetDependencyData();
        std::vector<std::optional<std::vector<h5p::library_without_patch_version>>> deps_m = {depdata.dynamicDependencies, depdata.preloadedDependencies, depdata.editorDependencies};
        for (auto &optlibs : deps_m) {
            if (optlibs) {
                auto &libs = optlibs.value();
                for (auto &lib : libs) {
                    if (lib.machineName == library.GetMachineName() && lib.majorVersion == library.GetLibraryLoad().major_version && lib.minorVersion == library.GetLibraryLoad().minor_version) {
                        stats.libraries++;
                    }
                }
            }
        }
    }

    if (!skipContent) {
        for (auto &[id, cont] : content) {
            const std::vector<h5p::library_dependency> &libdeps = cont.GetLibdeps();
            if (library.GetId() == cont.GetLibraryId()) {
                stats.content++;
            } else {
                for (auto &libdep : libdeps) {
                    if (library.GetId() == libdep.library_id) {
                        stats.content++;
                        break;
                    }
                }
            }
        }
    }
    return stats;
}

std::optional<h5p::load_library_data>
H5pMemoryPlatform::loadLibrary(const std::string &machineName, unsigned int majorVersion, unsigned int minorVersion) {
    std::unique_lock lock(mutex);

    auto mn_i = machineNameVersionMap.find(machineName);
    if (mn_i == machineNameVersionMap.end()) {
        return {};
    }
    auto &mn_m = mn_i->second;
    auto mav_i = mn_m.find(majorVersion);
    if (mav_i == mn_m.end()) {
        return {};
    }
    auto &mav_m = mav_i->second;
    auto miv_i = mav_m.find(minorVersion);
    if (miv_i == mav_m.end()) {
        return {};
    }
    const unsigned int library_id = miv_i->second.crbegin()->second;

    auto library_i = libraries.find(library_id);
    if (library_i == libraries.end()) {
        throw std::exception();
    }
    auto &library = library_i->second;

    return library.GetLoadLibraryData();
}

std::optional<std::string>
H5pMemoryPlatform::loadLibrarySemantics(const std::string &machineName, unsigned int majorVersion,
                                        unsigned int minorVersion) {
    std::unique_lock lock(mutex);

    auto mn_i = machineNameVersionMap.find(machineName);
    if (mn_i == machineNameVersionMap.end()) {
        return {};
    }
    auto &mn_m = mn_i->second;
    auto mav_i = mn_m.find(majorVersion);
    if (mav_i == mn_m.end()) {
        return {};
    }
    auto &mav_m = mav_i->second;
    auto miv_i = mav_m.find(minorVersion);
    if (miv_i == mav_m.end()) {
        return {};
    }
    const unsigned int library_id = miv_i->second.crbegin()->second;

    auto library_i = libraries.find(library_id);
    if (library_i == libraries.end()) {
        throw std::exception();
    }
    auto &library = library_i->second;

    return library.GetSemantics();
}

void H5pMemoryPlatform::deleteLibraryDependencies(unsigned int libraryId) {
    std::unique_lock lock(mutex);

    auto lib_i = libraries.find(libraryId);
    if (lib_i == libraries.end()) {
        throw std::exception();
    }
    lib_i->second.DeleteDependencies();
}

void H5pMemoryPlatform::lockDependencyStorage() {
    // TODO - Right?
}

void H5pMemoryPlatform::unlockDependencyStorage() {
    // TODO - Right?
}

void H5pMemoryPlatform::deleteLibrary(unsigned int libraryId) {
    std::unique_lock lock(mutex);
    auto id_find = libraries.find(libraryId);
    if (id_find == libraries.end()) {
        throw std::exception();
    }
    auto &lib = id_find->second;
    auto info = lib.GetLibraryLoad();
    auto m1_i = machineNameVersionMap.find(info.name);
    if (m1_i == machineNameVersionMap.end()) {
        libraries.erase(libraryId);
        throw std::exception();
    }
    auto &m1 = m1_i->second;
    auto m2_i = m1.find(info.major_version);
    if (m2_i == m1.end()) {
        libraries.erase(libraryId);
        throw std::exception();
    }
    auto &m2 = m2_i->second;
    auto m3_i = m2.find(info.minor_version);
    if (m3_i == m2.end()) {
        libraries.erase(libraryId);
        throw std::exception();
    }
    auto &m3 = m3_i->second;
    if (m3.find(info.patch_version) == m3.end()) {
        libraries.erase(libraryId);
        throw std::exception();
    }
    m3.erase(info.patch_version);
    if (m3.empty()) {
        m2.erase(info.minor_version);
        if (m2.empty()) {
            m1.erase(info.major_version);
            if (m1.empty()) {
                machineNameVersionMap.erase(info.name);
            }
        }
    }
}

std::optional<h5p::content_load> H5pMemoryPlatform::loadContent(unsigned int id) {
    std::unique_lock lock(mutex);
    const auto c_i = content.find(id);
    if (c_i == content.end()) {
        return {};
    }
    InMemoryContent &c = c_i->second;
    h5p::content_load cl = c.CreateContentLoad();

    auto lib_i = libraries.find(cl.libraryId);
    if (lib_i == libraries.end()) {
        throw std::exception();
    }
    InMemoryH5pLibrary &lib = lib_i->second;

    lib.LoadContent(cl);

    return cl;
}

std::vector<h5p::library_dependency_load>
H5pMemoryPlatform::loadContentDependencies(unsigned int id, std::optional<h5p::dependency_type> type) {
    std::unique_lock lock(mutex);
    const auto c_i = content.find(id);
    if (c_i == content.end()) {
        throw std::exception();
    }
    InMemoryContent &c = c_i->second;
    std::vector<h5p::library_dependency_load> libdeps = c.LoadLibdeps(type);
    for (auto &libdep : libdeps) {
        const auto l_i = libraries.find(libdep.libraryId.value());
        if (l_i != libraries.end()) {
            InMemoryH5pLibrary &l = l_i->second;
            l.LoadLibraryDependency(libdep);
        }
    }
    return libdeps;
}

std::optional<std::string>
H5pMemoryPlatform::getOption(const std::string &name) {
    std::unique_lock lock(mutex);
    auto iter = config.find(name);
    if (iter != config.end()) {
        return iter->second;
    } else {
        return {};
    }
}

void H5pMemoryPlatform::setOption(const std::string &name, const std::optional<std::string> &value) {
    std::unique_lock lock(mutex);
    if (value) {
        config.insert_or_assign(name, value.value());
    } else {
        auto iterator = config.find(name);
        if (iterator != config.end()) {
            config.erase(iterator);
        }
    }
}

void H5pMemoryPlatform::updateContentFields(unsigned int id, const h5p::update_content_fields &fields) {
    std::unique_lock lock(mutex);
    auto iterator = content.find(id);
    if (iterator == content.end()) {
        throw std::exception();
    }
    iterator->second.UpdateFields(fields);
}

void H5pMemoryPlatform::clearFilteredParameters(std::vector<unsigned int> library_ids) {
    std::unique_lock lock(mutex);
    for (auto &[cid, cont] : content) {
        /* Check contains(cont.GetLibraryId()): */
        if (std::find(library_ids.begin(), library_ids.end(), cont.GetLibraryId()) != library_ids.end()) {
            cont.ClearFiltered();
        }
    }
}

unsigned int H5pMemoryPlatform::getNumNotFiltered() {
    unsigned int count = 0;
    {
        std::unique_lock lock(mutex);
        for (auto &[cid, cont] : content) {
            if (cont.GetFiltered().value_or("") == "") {
                count++;
            }
        }
    }
    return count;
}

unsigned int H5pMemoryPlatform::getNumContent(unsigned int libraryId, std::vector<unsigned int> skip) {
    unsigned int count = 0;
    {
        std::unique_lock lock(mutex);
        for (auto &[cid, cont] : content) {
            if (cont.GetLibraryId() == libraryId && std::find(skip.begin(), skip.end(), cid) == skip.end()) {
                count++;
            }
        }
    }
    return count;
}

bool H5pMemoryPlatform::isContentSlugAvailable(const std::string &slug) {
    std::unique_lock lock(mutex);
    for (auto &[cid, cont] : content) {
        if (cont.GetSlug().value_or("") == slug) {
            return false;
        }
    }
    return true;
}

void H5pMemoryPlatform::saveCachedAssets(const std::string &key, std::vector<unsigned int> libraries) {
    std::unique_lock lock(mutex);
    for (unsigned int library_id : libraries) {
        auto existing = contentHashes.find(library_id);
        if (existing != contentHashes.end()) {
            existing->second.push_back(key);
        } else {
            std::vector<std::string> hashes = {key};
            contentHashes.insert_or_assign(library_id, std::move(hashes));
        }
    }
}

std::vector<std::string> H5pMemoryPlatform::deleteCachedAssets(unsigned int library_id) {
    std::unique_lock lock(mutex);
    auto find_lib = contentHashes.find(library_id);
    if (find_lib != contentHashes.end()) {
        std::vector<std::string> hashes = find_lib->second;
        contentHashes.erase(find_lib);
        return hashes;
    } else {
        return {};
    }
}

std::vector<std::tuple<h5p::library_without_patch_version, unsigned int>> H5pMemoryPlatform::getLibraryContentCount() {
    std::vector<std::tuple<h5p::library_without_patch_version, unsigned int>> output = {};
    std::map<std::string,std::map<unsigned int,std::map<unsigned int, unsigned int>>> map = {};
    {
        std::unique_lock lock(mutex);
        for (auto &[cid, cont] : content) {
            auto iterator = libraries.find(cont.GetLibraryId());
            if (iterator == libraries.end()) {
                continue;
            }
            auto &library = iterator->second;
            auto i1 = map.find(library.GetMachineName());
            if (i1 != map.end()) {
                std::map<unsigned int, std::map<unsigned int, unsigned int>> &m1 = i1->second;
                auto i2 = m1.find(library.GetMajorVersion());
                if (i2 != m1.end()) {
                    std::map<unsigned int, unsigned int> &m2 = i2->second;
                    auto i3 = m2.find(library.GetMinorVersion());
                    if (i3 != m2.end()) {
                        i3->second++;
                    } else {
                        m2.insert_or_assign(library.GetMinorVersion(), 1);
                    }
                } else {
                    std::map<unsigned int, unsigned int> m2 = {};
                    m2.insert_or_assign(library.GetMinorVersion(), 1);
                    m1.insert_or_assign(library.GetMajorVersion(), m2);
                }
            } else {
                std::map<unsigned int, unsigned int> m2 = {};
                std::map<unsigned int, std::map<unsigned int, unsigned int>> m1;
                m2.insert_or_assign(library.GetMinorVersion(), 1);
                m1.insert_or_assign(library.GetMajorVersion(), m2);
                map.insert_or_assign(library.GetMachineName(), m1);
            }
        }
    }

    for (const auto &[machine_name, m1] : map) {
        for (const auto &[major_version, m2] : m1) {
            for (const auto &[minor_version, count] : m2) {
                h5p::library_without_patch_version lib_info;
                lib_info.machineName = machine_name;
                lib_info.majorVersion = major_version;
                lib_info.minorVersion = minor_version;
                std::tuple<h5p::library_without_patch_version, unsigned int> item = {std::move(lib_info), count};
                output.push_back(std::move(item));
            }
        }
    }
    return output;
}

void H5pMemoryPlatform::afterExportCreated(unsigned int content_id, const std::string &filename) {

}

bool H5pMemoryPlatform::hasPermission(h5p::permission permission, std::optional<unsigned int> id) {
    return true;
}

void H5pMemoryPlatform::replaceContentTypeCache(const std::vector<h5p::hub_content_type> &contentTypeCache) {
    std::unique_lock lock(mutex);
    this->contentTypeCache = contentTypeCache;
}

void H5pMemoryPlatform::SetMayUpdateLibraries(bool may_upd) {
    std::unique_lock lock(mutex);
    this->may_update_libraries = may_upd;
}

InMemoryContent *H5pMemoryPlatform::getContent(unsigned int content_id) {
    auto i = content.find(content_id);
    if (i != content.end()) {
        return &(i->second);
    } else {
        return nullptr;
    }
}

InMemoryH5pLibrary *H5pMemoryPlatform::getLibrary(unsigned int library_id) {
    auto i = libraries.find(library_id);
    if (i != libraries.end()) {
        return &(i->second);
    } else {
        return nullptr;
    }
}

