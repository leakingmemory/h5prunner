#include <iostream>

#include <h5prunner/PhpEngine.h>
#include <h5prunner/H5pMemoryPlatform.h>
#include <h5prunner/H5pPhpBindings.h>

int main() {
    H5pMemoryPlatform platform("https://testh5p.radiotube.org/files/");

    H5pPhpBindings h5pPhpBindings(platform);
    PhpEngine phpEngine(h5pPhpBindings.zend_function_entries());

    {

        std::string testScript = R"(
print_r(h5phost_get_platform_info());

echo h5phost_fetch_external_data("https://worklog.radiotube.org", null, true, null)."\n";

$postResult = h5phost_fetch_external_data("https://worklog.radiotube.org", ["test1" => "val1", "test2" => ["val2", "val3"]], true, null);
echo ($postResult !== null ? $postResult : "<null>")."\n";

$postResult = h5phost_fetch_external_data("https://worklog.radiotube.org", ["test1" => "val1", "test2" => ["val2", "val3"]], false, null);
echo ($postResult !== null ? $postResult : "<null>")."\n";

$saveLibraryTest = [
    "title" => "Test",
    "machineName" => "H5P.Test",
    "majorVersion" => 0,
    "minorVersion" => 1,
    "patchVersion" => 0,
    "runnable" => 1,
    "metadataSettings" => [
        "disable" => 0,
        "disableExtraTitleField" => 0
    ],
    "fullscreen" => 0,
    "embedTypes" => "iframe, link",
    "preloadedJs" => [
        ["path" => "preload1.js"],
        ["path" => "preload2.js"]
    ],
    "preloadedCss" => [
        ["path" => "preload1.css"],
        ["path" => "preload2.css"]
    ],
    "dropLibraryCss" => [
        ["machineName" => "H5P.DropCss"]
    ],
    "semantics" => "{\"test\": \"testval\"}",
    "language" => [
        "nb" => "{}"
    ]
];

h5phost_save_library_data($saveLibraryTest, true);

print_r($saveLibraryTest);

$saveLibraryTest = [
    "libraryId" => 1,
    "title" => "Test changed",
    "machineName" => "H5P.Test",
    "majorVersion" => 0,
    "minorVersion" => 1,
    "patchVersion" => 0,
    "runnable" => true,
    "metadataSettings" => [
        "disable" => true,
        "disableExtraTitleField" => false
    ],
    "fullscreen" => true,
    "embedTypes" => "iframe, link",
    "preloadedJs" => [
        ["path" => "preload1.js"],
        ["path" => "preload2.js"]
    ],
    "preloadedCss" => [
        ["path" => "preload1.css"],
        ["path" => "preload2.css"]
    ],
    "dropLibraryCss" => [
        ["machineName" => "H5P.DropCss"]
    ],
    "semantics" => "{\"test\": \"testval\"}",
    "language" => [
        "nb" => "{}"
    ]
];

h5phost_save_library_data($saveLibraryTest, false);

$saveLibraryTest = [
    "libraryId" => 1,
    "title" => "Test changed p1",
    "machineName" => "H5P.Test",
    "majorVersion" => 0,
    "minorVersion" => 1,
    "patchVersion" => 1,
    "runnable" => true,
    "metadataSettings" => [
        "disable" => true,
        "disableExtraTitleField" => false
    ],
    "fullscreen" => true,
    "embedTypes" => "iframe, link",
    "preloadedJs" => [
        ["path" => "preload1.js"],
        ["path" => "preload2.js"]
    ],
    "preloadedCss" => [
        ["path" => "preload1.css"],
        ["path" => "preload2.css"]
    ],
    "dropLibraryCss" => [
        ["machineName" => "H5P.DropCss"]
    ],
    "semantics" => "{\"test\": \"testval\"}",
    "language" => [
        "nb" => "{}"
    ]
];

h5phost_save_library_data($saveLibraryTest, true);

print_r($saveLibraryTest);

$saveLibraryTest = [
    "libraryId" => "conflict",
    "title" => "Test changed p2",
    "machineName" => "H5P.Test",
    "majorVersion" => 0,
    "minorVersion" => 1,
    "patchVersion" => 2,
    "runnable" => true,
    "metadataSettings" => [
        "disable" => true,
        "disableExtraTitleField" => false
    ],
    "fullscreen" => true,
    "embedTypes" => "iframe, link",
    "preloadedJs" => [
        ["path" => "preload1.js"],
        ["path" => "preload2.js"]
    ],
    "preloadedCss" => [
        ["path" => "preload1.css"],
        ["path" => "preload2.css"]
    ],
    "dropLibraryCss" => [
        ["machineName" => "H5P.DropCss"]
    ],
    "semantics" => "{\"test\": \"testval\"}",
    "language" => [
        "nb" => "{}"
    ]
];

h5phost_save_library_data($saveLibraryTest, true);

$saveLibraryTest = [
    "title" => "Addon test",
    "machineName" => "H5P.AddOn",
    "majorVersion" => 0,
    "minorVersion" => 1,
    "patchVersion" => 0,
    "runnable" => true,
    "metadataSettings" => [
        "disable" => true,
        "disableExtraTitleField" => false
    ],
    "fullscreen" => true,
    "embedTypes" => "iframe, link",
    "preloadedJs" => [
        ["path" => "preload1.js"],
        ["path" => "preload2.js"]
    ],
    "preloadedCss" => [
        ["path" => "preload1.css"],
        ["path" => "preload2.css"]
    ],
    "dropLibraryCss" => [
        ["machineName" => "H5P.DropCss"]
    ],
    "semantics" => "{\"test\": \"testval\"}",
    "language" => [
        "nb" => "{}"
    ],
    "addTo" => [
        "test" => [],
        "test2" => [["test" => "test"],0,0.2,"t",false,true],
        "test3" => null
    ]
];

h5phost_save_library_data($saveLibraryTest, true);

print_r($saveLibraryTest);

h5phost_set_library_tutorial_url("H5P.Test", "https://radiotube.org");

h5phost_set_error_message("Error", "10");

h5phost_set_info_message("Info");

print_r(h5phost_get_messages("error"));
print_r(h5phost_get_messages("error"));

print_r(h5phost_get_messages("info"));
print_r(h5phost_get_messages("info"));

echo h5phost_t("Hello !world - !world", ['world' => '<p>"\'world\'"</p> &!'])."\n";
echo h5phost_t("Hello @world - @world", ['world' => '<p>"\'world\'"</p> &!'])."\n";
echo h5phost_t("Hello %world - %world", ['world' => '<p>"\'world\'"</p> &!'])."\n";
echo h5phost_t("!Hello !world - @world - %world", ['!Hello' => 'Ehlo', 'world' => '<p>"\'world\'"</p> &!'])."\n";

echo h5phost_get_library_file_url("/H5P.Test/0/1", "icon.svg")."\n";
echo h5phost_get_library_file_url("H5P.Test/0/1", "/icon.svg")."\n";
echo h5phost_get_library_file_url("H5P.Test/0/1/", "/icon.svg")."\n";

)";

        phpEngine.execute(testScript);
    }

    {
        std::string testScript = R"(
    echo "Upload:\n";
    echo h5phost_get_uploaded_h5p_folder_path()."\n";
    echo h5phost_get_uploaded_h5p_path()."\n";
)";

        auto ctx = platform.setH5pUpload("/tmp/upload-34534653", "/tmp/tmpdir43504353");
        phpEngine.execute(testScript);
    }

    {
        std::string testScript = R"(
    echo "Addons test:\n";
    print_r(h5phost_load_addons());

    echo "Load lib conf: (should be empty)\n";
    print_r(h5phost_get_library_config(["test"]));

    echo "Load libs:\n";
    print_r(h5phost_load_libraries());

    $admin_url = h5phost_get_admin_url();
    echo "Admin url (prob null): ".($admin_url !== null ? $admin_url : "<null>")."\n";

    echo "Get lib H5P.Test (): ".h5phost_get_library_id("H5P.Test")."\n";
    echo "Get lib H5P.Test (null, null): ".h5phost_get_library_id("H5P.Test", null, null)."\n";
    echo "Get lib H5P.Test (null): ".h5phost_get_library_id("H5P.Test", null)."\n";
    echo "Get lib H5P.Test (0): ".h5phost_get_library_id("H5P.Test", 0)."\n";
    echo "Get lib H5P.Test (0, 1): ".h5phost_get_library_id("H5P.Test", 0, 1)."\n";
    echo "Get lib H5P.TestNF (0): ".h5phost_get_library_id("H5P.TestNF", 0)."\n";

    echo "Test content allowlist: ".h5phost_get_whitelist(false, "json jpg png woff", "css js")."\n";
    echo "Test library allowlist: ".h5phost_get_whitelist(true, "json jpg png woff", "css js")."\n";

    echo "Test is patched (true/false): ";
    echo h5phost_is_patched_library(['machineName' => 'H5P.Test', 'majorVersion' => 0, 'minorVersion' => 1, 'patchVersion' => 3]) ? 'true' : 'false';
    echo '/';
    echo h5phost_is_patched_library(['machineName' => 'H5P.Test', 'majorVersion' => 0, 'minorVersion' => 2, 'patchVersion' => 0]) ? 'true' : 'false';
    echo "\n";
    echo "dev-mode=".(h5phost_is_in_dev_mode()?'yes':'no').' may-update-libs='.(h5phost_may_update_libraries()?'yes':'no')."\n";

    $content = [
            'title' => 'Test content',
            'params' => '{"content": "Content data"}',
            'library' => ['libraryId' => 3],
            'embed_type' => 'iframe',
            'disable' => false,
            'max_score' => 1,
            'slug' => 'slugtest',
            'user_id' => 1,
            'is_published' => false,
            'is_private' => false,
            'language_iso_639_3' => 'en-NO'
    ];
    echo "Insert content test: ".($content['id'] = h5phost_insert_content($content))."\n";
    echo "Update content test: ";
    h5phost_update_content($content);
    echo "done\n";

    echo "Save-1 dependencies:";
    h5phost_save_library_dependencies(3, [['machineName' => 'H5P.First', 'majorVersion' => 1, 'minorVersion' => 0], ['machineName' => 'H5P.Second', 'majorVersion' => 1, 'minorVersion' => 0]], 'editor');
    echo " editor";
    h5phost_save_library_dependencies(3, [['machineName' => 'H5P.PreFirst', 'majorVersion' => 1, 'minorVersion' => 0], ['machineName' => 'H5P.PreSecond', 'majorVersion' => 1, 'minorVersion' => 0]], 'preloaded');
    echo " preloaded";
    h5phost_save_library_dependencies(3, [['machineName' => 'H5P.DynFirst', 'majorVersion' => 1, 'minorVersion' => 0], ['machineName' => 'H5P.DynSecond', 'majorVersion' => 1, 'minorVersion' => 0]], 'dynamic');
    echo " dynamic.\n";

    echo "Delete dependencies:\n";
    h5phost_delete_library_dependencies(3);

    echo "Save-1 dependencies:";
    h5phost_save_library_dependencies(3, [['machineName' => 'H5P.First', 'majorVersion' => 1, 'minorVersion' => 0], ['machineName' => 'H5P.Second', 'majorVersion' => 1, 'minorVersion' => 0]], 'editor');
    echo " editor";
    h5phost_save_library_dependencies(3, [['machineName' => 'H5P.PreFirst', 'majorVersion' => 1, 'minorVersion' => 0], ['machineName' => 'H5P.PreSecond', 'majorVersion' => 1, 'minorVersion' => 0]], 'preloaded');
    echo " preloaded";
    h5phost_save_library_dependencies(3, [['machineName' => 'H5P.DynFirst', 'majorVersion' => 1, 'minorVersion' => 0], ['machineName' => 'H5P.DynSecond', 'majorVersion' => 1, 'minorVersion' => 0]], 'dynamic');
    echo " dynamic.\n";


    h5phost_save_library_usages($content['id'], [
        ['library' => ['libraryId' => 3, 'machineName' => 'H5P.Test', 'dropLibraryCss' => 'H5P.Test,H5P.Drop'], 'type' => 'dynamic']
    ]);
    print_r(h5phost_load_content_dependencies($content['id'], 'editor'));
    print_r(h5phost_load_content_dependencies($content['id'], 'preloaded'));
    print_r(h5phost_load_content_dependencies($content['id'], 'dynamic'));
    print_r(h5phost_load_content_dependencies($content['id'], null));
    print_r(h5phost_load_content_dependencies($content['id']));
    $content2 = [
            'title' => 'Test content',
            'params' => '{"content": "Content data"}',
            'library' => ['libraryId' => 3],
            'embed_type' => 'iframe',
            'disable' => false,
            'max_score' => 1,
            'slug' => 'slugtest',
            'user_id' => 1,
            'is_published' => false,
            'is_private' => false,
            'language_iso_639_3' => 'en-NO'
    ];
    echo "Insert content2 test: ".($content2['id'] = h5phost_insert_content($content2))."\n";
    print_r(h5phost_load_content_dependencies($content2['id']));
    h5phost_copy_library_usage($content2['id'], $content['id'], null);
    print_r(h5phost_load_content_dependencies($content2['id']));
    h5phost_delete_library_usage($content2['id']);
    print_r(h5phost_load_content_dependencies($content2['id']));
    h5phost_delete_content_data($content2['id']);

    echo "Library usage tests:\n";
    print_r(h5phost_get_library_usage(1, false));
    print_r(h5phost_get_library_usage(2, false));
    print_r(h5phost_get_library_usage(3, false));
    print_r(h5phost_get_library_usage(3, true));
    print_r(h5phost_get_library_usage(3));

    echo "Load library test:\n";
    print_r(h5phost_load_library("H5P.Test", 0, 1));
    echo "Semantics: ".($semantics = h5phost_load_library_semantics("H5P.Test", 0, 1))."\n";
    $semantics = json_decode($semantics, TRUE);
    h5phost_alter_library_semantics($semantics, "H5P.Test", 0, 1);
    print_r($semantics);

    h5phost_lock_dependency_storage();
    h5phost_unlock_dependency_storage();

    h5phost_delete_library(1);

    print_r(h5phost_load_content($content['id']));

    h5phost_set_option("string", "string");
    h5phost_set_option("true", true);
    h5phost_set_option("array", ["assoc" => [1, 2]]);

    print_r([
        "string" => h5phost_get_option("string"),
        "true" => h5phost_get_option("true"),
        "array" => h5phost_get_option("array"),
        "notset" => h5phost_get_option("notset")
    ]);

    h5phost_update_content_fields($content['id'], [
        "title" => "New title",
        "slug" => "spescont"
    ]);

    h5phost_clear_filtered_parameters(0);
    h5phost_clear_filtered_parameters([1, "2", 3]);

    echo "Num not filtered: ".h5phost_get_num_not_filtered()."\n";

    echo "Num content: ".h5phost_get_num_content(3)."\n";
    echo "Num content: ".h5phost_get_num_content(3, [1,2])."\n";
    echo "Num content: ".h5phost_get_num_content(3, [1,2,3,4,5,6,7,8,9])."\n";

    echo "Slug 'slug' available: ".(h5phost_is_content_slug_available('slug') ? 'yes' : 'no')."\n";

    echo "Num authors: ".h5phost_get_num_authors()."\n";

    h5phost_save_cached_assets("somehash", [1, 2, 3]);
    h5phost_save_cached_assets("somehash2", [1, 2, 3]);

    print_r(h5phost_delete_cached_assets(3));
    print_r(h5phost_delete_cached_assets(3));

    print_r(h5phost_get_library_content_count());

    $content2 = [
            'title' => 'Test content',
            'params' => '{"content": "Content data"}',
            'library' => ['libraryId' => 3],
            'embed_type' => 'iframe',
            'disable' => false,
            'max_score' => 1,
            'slug' => 'slugtest',
            'user_id' => 1,
            'is_published' => false,
            'is_private' => false,
            'language_iso_639_3' => 'en-NO'
    ];
    echo "Insert content2 test: ".($content2['id'] = h5phost_insert_content($content2))."\n";

    print_r(h5phost_get_library_content_count());

    h5phost_after_export_created($content, "test-export-file.zip");

    echo "Has permission 1: ".(h5phost_has_permission(1, 1) ? 'yes' : 'no')."\n";
    echo "Has permission 2: ".(h5phost_has_permission(2, null) ? 'yes' : 'no')."\n";
    echo "Has permission 3: ".(h5phost_has_permission(3) ? 'yes' : 'no')."\n";

    $cache = [
        'contentTypes' => [
            [
                    'id' => 'H5P.TestType',
                    'version' => [
                        'major' => 1,
                        'minor' => 0,
                        'patch' => 1
                    ],
                    'coreApiVersionNeeded' => [
                        'major' => 1,
                        'minor' => 0
                    ],
                    'title' => 'Title',
                    'summary' => 'Summary',
                    'description' => 'Description',
                    'icon' => '//Icon.png',
                    'isRecommended' => true,
                    'popularity' => 1000,
                    'screenshots' => ['//Screenshot1.png', '//Screenshot2.png'],
                    'license' => ['CC0', 'CC-BY'],
                    'example' => '//Example',
                    'tutorial' => '//Tutorial',
                    'keywords' => ['test'],
                    'categories' => ['test'],
                    'owner' => 1,
            ],
            [
                    'id' => 'H5P.TestType2',
                    'version' => [
                        'major' => 1,
                        'minor' => 2,
                        'patch' => 0
                    ],
                    'coreApiVersionNeeded' => [
                        'major' => 1,
                        'minor' => 2
                    ],
                    'title' => 'Title2',
                    'summary' => 'Summary2',
                    'description' => 'Description2',
                    'icon' => '//Icon2.png',
                    'isRecommended' => true,
                    'popularity' => 1000,
                    'screenshots' => ['//Screenshot2-1.png', '//Screenshot2-2.png'],
                    'license' => ['CC0', 'CC-BY'],
                    'example' => '//Example-2',
                    'tutorial' => '//Tutorial-2',
                    'keywords' => ['test2'],
                    'categories' => ['test2'],
                    'owner' => 'Huffsa',
            ]
        ]
    ];
    $cache = json_decode(json_encode($cache), FALSE);
    h5phost_replace_content_type_cache($cache);

    echo "Library has upgrades: ".(h5phost_library_has_upgrade(['machineName' => 'H5P.Test', 'majorVersion' => 2, 'minorVersion' => 1]) ? 'yes' : 'no')."\n";
)";

        phpEngine.execute(testScript);
    }

    return 0;
}
