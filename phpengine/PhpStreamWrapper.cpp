//
// Created by janeover on 31.10.2020.
//
extern "C" {
#include <php.h>
#include <php_streams.h>
};

#include <h5prunner/PhpStreamWrapper.h>

PhpStreamWrapper::PhpStreamWrapper(int fd, const std::string &phpOpenMode) : pzval(nullptr) {
    std::string fdstr = "fd://";
    fdstr.append(std::to_string(fd));
    php_stream_context *ctx = NULL;
    stream = php_stream_open_wrapper_ex(fdstr.c_str(), phpOpenMode.c_str(), 0, NULL, ctx);
}

PhpStreamWrapper::PhpStreamWrapper(const std::string &phpUri, const std::string &phpOpenMode) : pzval(nullptr) {
    php_stream_context *ctx = NULL;
    stream = php_stream_open_wrapper_ex(phpUri.c_str(), phpOpenMode.c_str(), 0, NULL, ctx);
}

PhpStreamWrapper::~PhpStreamWrapper() {
    if (stream != nullptr) {
        php_stream_close(stream);
    }
    if (pzval != nullptr) {
        free(pzval);
    }
}

php_stream *PhpStreamWrapper::get_stream() {
    return stream;
}

php_stream *PhpStreamWrapper::detach_stream() {
    php_stream *st = stream;
    stream = nullptr;
    return st;
}

zval *PhpStreamWrapper::to_zval() {
    if (pzval != nullptr) {
        return pzval;
    }
    if (stream != nullptr) {
        pzval = static_cast<zval *>(malloc(sizeof(*pzval)));
        php_stream_to_zval(stream, pzval);
        stream = nullptr;
        return pzval;
    } else {
        throw std::exception();
    }
}

PhpStreamWrapper::PhpStreamWrapper() : stream(nullptr), pzval(nullptr) {
}

PhpStreamWrapper::PhpStreamWrapper(PhpStreamWrapper &&mv) noexcept : stream(mv.stream), pzval(mv.pzval) {
    mv.stream = nullptr;
    mv.pzval = nullptr;
}
