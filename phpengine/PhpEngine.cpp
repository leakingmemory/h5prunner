//
// Created by janeover on 31.10.2020.
//

extern "C" {
#include <php.h>
#include <zend.h>
#include <zend_extensions.h>
#include <zend_execute.h>
#include <php_output.h>
#include <TSRM.h>
#include <SAPI.h>
}

#include <iostream>
#include <main/php_main.h>

#include <h5prunner/PhpEngine.h>
#include <h5prunner/PhpStreamWrapper.h>
#include <h5prunner/PhpUtils.h>

const static std::string sapi_name = "PhpEngine";
const static std::string sapi_pretty_name = "C++ php runner";

static int sapi_module_startup(sapi_module_struct *sapi_module) /* {{{ */
{
    if (php_module_startup(sapi_module, NULL, 0)==FAILURE) {
        return FAILURE;
    }
    return SUCCESS;
}

static int sapi_module_deactivate(void)
{
    return SUCCESS;
}

static size_t sapi_ub_write(const char *str, size_t str_length) /* {{{ */
{
    std::string string = std::string(str, str_length);
    std::cout << string;
    return string.length();
}

static void sapi_flush(void *server_context) /* {{{ */
{
    fflush(stdout);
}

static int sapi_header_handler(sapi_header_struct *h, sapi_header_op_enum op, sapi_headers_struct *s) /* {{{ */
{
    return 0;
}

static int sapi_send_headers(sapi_headers_struct *sapi_headers) /* {{{ */
{
    /* We do nothing here, this function is needed to prevent that the fallback
     * header handling is called. */
    return SAPI_HEADER_SENT_SUCCESSFULLY;
}

static void sapi_send_header(sapi_header_struct *sapi_header, void *server_context) /* {{{ */
{
}

static char* sapi_read_cookies(void) {
    return NULL;
}

static void sapi_register_variables(zval *track_vars_array) /* {{{ */
{
}

static void sapi_log_message(char *message, int syslog_type_int) {
    std::cerr << message << "\n";
}

#define INI_DEFAULT(name,value)\
	ZVAL_NEW_STR(&tmp, zend_string_init(value, sizeof(value)-1, 1));\
	zend_hash_str_update(configuration_hash, name, sizeof(name)-1, &tmp);\

static void sapi_cli_ini_defaults(HashTable *configuration_hash)
{
    zval tmp;
    INI_DEFAULT("display_errors", "1");
}

PhpEngine::PhpEngine(const zend_function_entry *pib_functions) {
    //php_tsrm_startup();
    zend_signal_startup();

    bzero(&sapi_module, sizeof(sapi_module));
    sapi_module.name = (char *) sapi_name.c_str();
    sapi_module.pretty_name = (char *) sapi_pretty_name.c_str();

    sapi_module.startup = sapi_module_startup;
    sapi_module.shutdown = php_module_shutdown_wrapper;

    sapi_module.deactivate = sapi_module_deactivate;

    sapi_module.ub_write = sapi_ub_write;
    sapi_module.flush = sapi_flush;

    sapi_module.sapi_error = php_error;

    sapi_module.header_handler = sapi_header_handler;
    sapi_module.send_headers = sapi_send_headers;
    sapi_module.send_header = sapi_send_header;

    sapi_module.read_cookies = sapi_read_cookies;

    sapi_module.register_server_variables = sapi_register_variables;
    sapi_module.log_message = sapi_log_message;

    sapi_module.additional_functions = pib_functions;

    sapi_module.ini_defaults = sapi_cli_ini_defaults;
    sapi_module.php_ini_path_override = NULL;
    sapi_module.phpinfo_as_text = 1;
    sapi_module.php_ini_ignore_cwd = 1;
    sapi_startup(&sapi_module);

    sapi_module.php_ini_ignore = 0;

    //sapi_module->executable_location = argv[0];

    if (sapi_module.startup(&sapi_module) == FAILURE) {
        throw std::exception();
    }

    zend_first_try {
        CG(in_compilation) = 0;
        if (php_request_startup()==FAILURE) {
            throw std::exception();
        }
        stdin_wr = new PhpStreamWrapper("php://stdin", "rb");
        stdout_wr = new PhpStreamWrapper("php://stdout", "wb");
        stderr_wr = new PhpStreamWrapper("php://stderr", "wb");
        stdin_zval = stdin_wr->to_zval();
        stdout_zval = stdout_wr->to_zval();
        stderr_zval = stderr_wr->to_zval();
        register_constant("STDIN", *stdin_zval);
        register_constant("STDOUT", *stdout_zval);
        register_constant("STDERR", *stderr_zval);
    } zend_end_try();
}

void PhpEngine::register_constant(const std::string &name, const zval &val) {
    zend_constant zc;
    zc.name = zend_string_init(name.c_str(), name.length(), 1);
    zc.value = val;
    zend_register_constant(&zc);
}

PhpEngine::~PhpEngine() {
    delete stdin_wr;
    delete stdout_wr;
    delete stderr_wr;
    php_request_shutdown((void *) 0);
}

void PhpEngine::execute(const std::string &script) {
    zend_try{
        if (zend_eval_string_ex((char *) script.c_str(), NULL, (char *) sapi_pretty_name.c_str(), 1) == FAILURE) {
            std::cerr << "Failed\n";
        }
    } zend_end_try();
}

web::json::value PhpEngine::executeFile(const std::string &filename) {
    std::optional<web::json::value> ret_as_json = {};
    zend_try {
        zend_file_handle file_handle;
        if (php_stream_open_for_zend_ex(filename.c_str(), &file_handle, 0) == SUCCESS) {
            zval return_value;
            if (zend_execute_scripts(ZEND_REQUIRE_ONCE, &return_value, 1, &file_handle) == SUCCESS) {
                ret_as_json = zval_to_json(&return_value);
                ZVAL_NULL(&return_value);
            } else {
                std::cerr << "Require once failed " << filename << "\n";
            }
        } else {
            std::cerr << "Zend open failed: " << filename << "\n";
        }
    } zend_end_try();
    if (ret_as_json) {
        return ret_as_json.value();
    } else {
        throw std::exception();
    }
}
