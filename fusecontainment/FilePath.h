//
// Created by janeover on 16.11.2020.
//

#ifndef H5PRUNNER_FILEPATH_H
#define H5PRUNNER_FILEPATH_H

#include <string>

class FilePath {
private:
    const std::string path;
public:
    explicit FilePath(const std::string &path);
    bool IsDirectory();
    void Delete();
    void RecursiveDelete();
};


#endif //H5PRUNNER_FILEPATH_H
