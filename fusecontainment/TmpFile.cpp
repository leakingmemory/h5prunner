//
// Created by janeover on 18.11.2020.
//

#include <h5prunner/TmpFile.h>
#include <strings.h>
#include <cstring>
#include <fcntl.h>
#include <unistd.h>
#include "../filesystem/FileException.h"

#define TMPNAME_TPL "/tmp/h5prunnerXXXXXX"

TmpFile::TmpFile(const void *ptr, size_t size) {
    std::string filename;
    {
        char *tempname = static_cast<char *>(malloc(sizeof(TMPNAME_TPL)));
        bcopy(TMPNAME_TPL, tempname, sizeof(TMPNAME_TPL));
        mktemp(tempname);
        if (strlen(tempname) == 0) {
            auto errorCode = errno;
            free(tempname);
            throw FileException("mktemp", {TMPNAME_TPL}, {errorCode});
        }
        filename = std::string(tempname, strlen(tempname));
        free(tempname);
    }

    int fd = open(filename.c_str(), O_CREAT | O_TRUNC | O_WRONLY, 0644);
    if (fd < 0) {
        auto errorCode = errno;
        unlink(filename.c_str());
        throw FileException("open tempfile", {filename}, {errorCode});
    }
    const char *cptr = static_cast<const char *>(ptr);
    size_t remaining = size;
    while (remaining > 0) {
        auto written = write(fd, cptr, remaining);
        if (written < 0) {
            auto errorCode = errno;
            close(fd);
            unlink(filename.c_str());
            throw FileException("write tempfile", {filename}, {errorCode});
        }
        remaining -= written;
        cptr += written;
    }
    if (close(fd) != 0) {
        auto errorCode = errno;
        unlink(filename.c_str());
        throw FileException("close tempfile", {filename}, {errorCode});
    }
    path = {filename};
}

TmpFile::~TmpFile() {
    if (path) {
        unlink(path.value().c_str());
    }
}

void TmpFile::RenameWithSuffix(const std::string &suffix) {
    if (path) {
        std::string new_name(path.value());
        new_name.append(suffix);
        if (rename(path.value().c_str(), new_name.c_str()) == 0) {
            path = {new_name};
        } else {
            std::string err("rename ");
            err.append(path.value());
            throw FileException(err, {new_name});
        }
    } else {
        throw FileException("TmpFile is empty", {}, {});
    }
}

std::string TmpFile::GetPath() {
    return path.value_or("");
}
