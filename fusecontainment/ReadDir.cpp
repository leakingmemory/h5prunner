//
// Created by janeover on 16.11.2020.
//

#include <sys/mman.h>
#include <cstring>

#include "ReadDir.h"
#include "../filesystem/FileException.h"

#define MAX_DIRENTS 65536

const dirent *ReadDir::begin() noexcept {
    return dirents;
}

const dirent *ReadDir::end() noexcept {
    if (dirents != nullptr) {
        return &(dirents[num]);
    } else {
        return nullptr;
    }
}

ReadDir::~ReadDir() {
    if (dirents != nullptr) {
        if (munmap(dirents, sizeof(*dirents) * max) != 0) {
            throw FileException("unmap", {});
        }
        dirents = nullptr;
    }
}

ReadDir::ReadDir(std::string path) : max(MAX_DIRENTS), num(0) {

    dirents = static_cast<dirent *>(mmap(NULL, max * sizeof(*dirents), PROT_READ | PROT_WRITE,
                                         MAP_ANONYMOUS | MAP_NORESERVE | MAP_PRIVATE, -1, 0));
    if (dirents == MAP_FAILED) {
        dirents = nullptr;
        throw FileException("mmap for dirents", {path});
    }
    DIR *dir = opendir(path.c_str());
    if (dir == NULL) {
        auto errorCode = errno;
        munmap(dirents, sizeof(*dirents) * max);
        dirents = nullptr;
        throw FileException("opendir", {path}, {errorCode});
    }
    errno = 0;
    struct dirent *rd;
    while ((rd = readdir(dir)) != NULL) {
        if (num >= max) {
            munmap(dirents, sizeof(*dirents) * max);
            dirents = nullptr;
            std::string error = "Too large directory, max entries ";
            error.append(std::to_string(max));
            throw FileException(error, {path}, {});
        }
        if ((strlen(rd->d_name) + 1) > sizeof(rd->d_name)) {
            munmap(dirents, sizeof(*dirents) * max);
            dirents = nullptr;
            std::string error = "Filename too long (sub)";
            throw FileException(error, {path}, {});
        }
        dirents[num++] = *rd;
    }
    auto errCode = errno;
    if (closedir(dir) != 0 && errCode == 0) {
        auto errorCode = errno;
        munmap(dirents, sizeof(*dirents) * max);
        dirents = nullptr;
        throw FileException("closedir", {path}, {errorCode});
    }
    if (errCode != 0) {
        munmap(dirents, sizeof(*dirents) * max);
        dirents = nullptr;
        throw FileException("readdir", {path}, {errCode});
    }
}
