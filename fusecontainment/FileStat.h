//
// Created by janeover on 16.11.2020.
//

#ifndef H5PRUNNER_FILESTAT_H
#define H5PRUNNER_FILESTAT_H

#include <string>

#include <sys/types.h>
#include <sys/stat.h>
#include <unistd.h>

class FileStat {
private:
    struct stat statbuf;
public:
    explicit FileStat(const std::string &filename);
    const struct stat &info();
};


#endif //H5PRUNNER_FILESTAT_H
