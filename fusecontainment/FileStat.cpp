//
// Created by janeover on 16.11.2020.
//

#include "FileStat.h"
#include "../filesystem/FileException.h"

FileStat::FileStat(const std::string &filename) {
    if (stat(filename.c_str(), &statbuf) != 0) {
        throw FileException("stat", {filename});
    }
}

const struct stat &FileStat::info() {
    return statbuf;
}
