//
// Created by janeover on 16.11.2020.
//

#ifndef H5PRUNNER_READDIR_H
#define H5PRUNNER_READDIR_H


#include <cstddef>
#include <string>
#include <dirent.h>

class ReadDir {
private:
    struct dirent *dirents;
    size_t num;
    size_t max;
public:
    ReadDir(std::string path);
    ~ReadDir();
    const dirent *begin() noexcept;
    const dirent *end() noexcept;
};


#endif //H5PRUNNER_READDIR_H
