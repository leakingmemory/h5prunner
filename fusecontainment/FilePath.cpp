//
// Created by janeover on 16.11.2020.
//

#include "FilePath.h"
#include "FileStat.h"
#include "../filesystem/FileException.h"
#include "ReadDir.h"

#include <unistd.h>
#include <cstring>
#include <sys/stat.h>


FilePath::FilePath(const std::string &path) : path(path) {
}

void FilePath::RecursiveDelete() {
    std::string subpath(path);
    if (!path.ends_with('/')) {
        subpath.append("/");
    }
    auto pathLenWS = subpath.length();
    {
        struct stat info;
        if (stat(path.c_str(), &info) != 0) {
            auto errorCode = errno;
            if (errorCode == ENOENT) {
                return;
            }
            throw FileException("stat recursive delete", {path}, {errorCode});
        }
        if ((info.st_mode & S_IFDIR) == 0) {
            if (unlink(path.c_str()) != 0) {
                throw FileException("unlink", {path});
            }
            return;
        }
    }
    ReadDir readDir(path);
    for (const auto &ent : readDir) {
        if (strcmp(ent.d_name, "..") == 0) {
            continue;
        }
        if (strcmp(ent.d_name, ".") == 0) {
            continue;
        }
        subpath.append(ent.d_name);
        if (ent.d_type == DT_DIR) {
            FilePath(subpath).RecursiveDelete();
        } else {
            FilePath(subpath).Delete();
        };
        subpath.resize(pathLenWS);
    }
    if (rmdir(path.c_str()) != 0) {
        throw FileException("rmdir", {path});
    }
}

bool FilePath::IsDirectory() {
    return (((FileStat(path).info().st_mode) & ((unsigned int) S_IFDIR)) != 0);
}

void FilePath::Delete() {
    if (unlink(path.c_str()) != 0) {
        throw FileException("Unlink", {path});
    }
}
