//
// Created by janeover on 15.11.2020.
//

#include <h5prunner/TmpDir.h>
#include <cstring>
#include <zconf.h>
#include <cerrno>
#include "../filesystem/FileException.h"
#include "FilePath.h"

TmpDir::TmpDir() {
    char *filename;
    {
        const char *templ = "/tmp/h5prunnerXXXXXX";
        size_t size = strlen(templ) + 1;
        filename = static_cast<char *>(malloc(size));
        strncpy(filename, templ, size);
    }
    char *path = mkdtemp(filename);
    if (path == NULL) {
        auto code = errno;
        free(filename);
        throw FileException("mkdtemp", {filename}, {code});
    }
    this->path = std::string(path, strlen(path));
}

TmpDir::~TmpDir() {
    if (path) {
        std::string filename = path.value();
        if (filename.length() > 4) { // Safety check
            FilePath(filename).RecursiveDelete();
        }
    }
}

std::string TmpDir::GetPath() {
    return path.value_or("");
}
