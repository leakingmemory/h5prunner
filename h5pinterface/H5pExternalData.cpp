//
// Created by janeover on 01.11.2020.
//

#include <cpprest/http_client.h>
#include <cpprest/uri.h>

#include <h5prunner/H5pExternalData.h>

H5pExternalData::H5pExternalData(const std::string &url, const std::optional<std::map<std::string,std::vector<std::string>>> &data) : client(url), data(data) {
}

pplx::task<std::optional<utf8string>> H5pExternalData::async() {
    pplx::task<web::http::http_response> requestTask;

    if (!data) {
        requestTask = client.request(web::http::methods::GET);
    } else {
        std::vector<std::string> encodedPairs;
        for (auto &[key, values] : data.value()) {
            std::string encodedKey = web::uri::encode_data_string(key);
            for (auto &value : values) {
                std::string encodedPair = encodedKey;
                encodedPair.append("=");
                encodedPair.append(web::uri::encode_data_string(value));
                encodedPairs.push_back(encodedPair);
            }
        }
        utf8string body = std::accumulate(encodedPairs.begin(), encodedPairs.end(), std::string(""), [&] (const std::string &a, const std::string &b) {
            std::string str(a);
            if (!str.empty()) {
                str.append("&");
            }
            str.append(b);
            return str;
        });
        requestTask = client.request(web::http::methods::POST, "", body, "application/x-www-form-urlencoded");
    }

    return requestTask.then([&] (web::http::http_response response) {
        web::http::status_code statusCode = response.status_code();
        if (statusCode != web::http::status_codes::NoContent && (statusCode / 100) == 2) {
            std::optional<utf8string> opt = response.extract_utf8string().get();
            return opt;
        } else {
            std::optional<utf8string> opt = {};
            return opt;
        }
    });
}

std::optional<std::string> H5pExternalData::download() {
    return async().get();
}

H5pExternalData::H5pExternalData(H5pExternalData &&mvConstr) noexcept : client(mvConstr.client), data(std::move(mvConstr.data)) {
}

