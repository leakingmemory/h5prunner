//
// Created by janeover on 15.11.2020.
//

#include <iostream>
#include <sys/stat.h>
#include <fcntl.h>
#include <unistd.h>
#include "include/h5prunner/TmpDir.h"

int main() {
    TmpDir tmpDir;
    {
        std::string testDir = tmpDir.GetPath();
        testDir.append("/testdir");
        if (mkdir(testDir.c_str(), 0700) != 0) {
            throw std::exception();
        }
        testDir.append("/testfile");
        int fd = open(testDir.c_str(), O_WRONLY | O_CREAT | O_TRUNC, 600);
        if (fd < 0) {
            throw std::exception();
        }
        if (write(fd, testDir.c_str(), testDir.length()) < 0) {
            std::cout << "Error code: " << errno << "\n";
            throw std::exception();
        }
        if (close(fd) != 0) {
            throw std::exception();
        }
    }
    std::cout << tmpDir.GetPath() << "\n";
}