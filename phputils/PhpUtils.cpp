//
// Created by janeover on 17.11.2020.
//

#include <h5prunner/PhpUtils.h>
#include <php.h>

#include <map>

web::json::value zval_to_json(zval *zv) {
    auto tp = Z_TYPE(*zv);
    switch (tp) {
        case IS_NULL:
            return web::json::value::null();
        case IS_FALSE:
            return web::json::value(false);
        case IS_TRUE:
            return web::json::value(true);
        case IS_STRING: {
            zend_string *z = Z_STR(*zv);
            return web::json::value(ZSTR_STD(z));
        }
        case IS_DOUBLE:
            return web::json::value(Z_DVAL(*zv));
        case IS_LONG:
            return web::json::value(Z_LVAL(*zv));
        case IS_OBJECT:
        case IS_ARRAY: {
            HashTable *hashTable = tp == IS_ARRAY ? Z_ARR(*zv) : Z_OBJPROP(*zv);
            int num_key;
            zend_string *zkey;
            zval *value;
            std::map<std::string,web::json::value> assoc;
            std::map<int,web::json::value> indexed;
            ZEND_HASH_FOREACH_KEY_VAL(hashTable, num_key, zkey, value)
            if (Z_TYPE(*value) == IS_INDIRECT) {
                value = Z_INDIRECT(*value);
            }
            web::json::value subjs = zval_to_json(value);
            if (zkey != NULL) {
                assoc.insert_or_assign(ZSTR_STD(zkey), subjs);
            } else {
                indexed.insert_or_assign(num_key, subjs);
            }
            ZEND_HASH_FOREACH_END();
            if (assoc.empty()) {
                std::vector<web::json::value> build;
                for (auto &[key, value] : indexed) {
                    build.push_back(value);
                }
                return web::json::value::array(build);
            } else {
                std::vector<std::pair<std::string,web::json::value>> build;
                for (auto &[key, value] : assoc) {
                    build.emplace_back(key, value);
                }
                for (auto &[key, value] : indexed) {
                    build.emplace_back(std::to_string(key), value);
                }
                return web::json::value::object(build);
            }
        }
        default:
            return web::json::value::null();
    }
}
