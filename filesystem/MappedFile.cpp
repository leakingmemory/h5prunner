//
// Created by janeover on 17.11.2020.
//

#include "MappedFile.h"
#include "FileException.h"

#include <fcntl.h>
#include <sys/types.h>
#include <unistd.h>
#include <cerrno>
#include <sys/mman.h>
#include <strings.h>

MappedFile::MappedFile(const std::string &filename) {
    bytes = nullptr;
    int fd = open(filename.c_str(), O_RDONLY);
    if (fd < 0) {
        throw FileException("open", {filename});
    }
    auto length = lseek(fd, 0, SEEK_END);
    if (length < 0) {
        auto errorCode = errno;
        close(fd);
        throw FileException("lseek end", {filename}, {errno});
    }
    if (lseek(fd, 0, SEEK_SET) != 0) {
        auto errorCode = errno;
        close(fd);
        throw FileException("lseek start", {filename}, {errno});
    }
    bytes = mmap(NULL, length, PROT_READ, MAP_PRIVATE, fd, 0);
    if (bytes == MAP_FAILED) {
        auto errorCode = errno;
        bytes = nullptr;
        close(fd);
        throw FileException("mmap read", {filename}, {errno});
    }
    if (close(fd) != 0) {
        auto errorCode = errno;
        munmap(bytes, length);
        bytes = nullptr;
        throw FileException("close after mmap", {filename}, {errno});
    }
    size = length;
}

MappedFile::~MappedFile() {
    if (bytes != nullptr) {
        if (munmap(bytes, size) != 0) {
            auto errorCode = errno;
            bytes = nullptr;
            throw FileException("munmap", {}, {errno});
        }
        bytes = nullptr;
        size = 0;
    }
}

const void *MappedFile::Content() noexcept {
    return bytes;
}

size_t MappedFile::Size() noexcept {
    return size;
}

MappedFile::MappedFile(const MappedFile &cp) {
    if (cp.bytes != nullptr) {
        bytes = mmap(NULL, cp.size, PROT_READ | PROT_WRITE, MAP_PRIVATE | MAP_ANONYMOUS, -1, 0);
        if (bytes == MAP_FAILED) {
            auto errorCode = errno;
            bytes = nullptr;
            throw FileException("mmap anonymous", {}, errorCode);
        }
        bcopy(cp.bytes, bytes, cp.size);
        size = cp.size;
    } else {
        bytes = nullptr;
        size = 0;
    }
}

MappedFile::MappedFile(MappedFile &&mv) : bytes(mv.bytes), size(mv.size) {
    mv.bytes = nullptr;
    mv.size = 0;
}
