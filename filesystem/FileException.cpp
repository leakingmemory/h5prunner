//
// Created by janeover on 16.11.2020.
//

#include "FileException.h"

#include <string.h>

FileException::FileException(std::string error, std::optional<std::string> filename, std::optional<int> code) noexcept : error(error), filename(filename) {
    if (filename) {
        prettyError = filename.value();
        prettyError.append(": ");
        prettyError.append(error);
    } else {
        prettyError = error;
    }
    if (code) {
        char buf[1024];
        strerror_r(code.value(), buf, sizeof(buf));
        const char *err = buf;
        prettyError.append(": ");
        prettyError.append(err);
    }
}

const char *FileException::what() const noexcept {
    return this->prettyError.c_str();
}
