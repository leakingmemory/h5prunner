//
// Created by janeover on 17.11.2020.
//

#ifndef H5PRUNNER_MAPPEDFILE_H
#define H5PRUNNER_MAPPEDFILE_H

#include <cstddef>
#include <string>

template<typename T> class MappedFileIterator {
private:
    const void *bytes;
    const size_t size;
public:
    constexpr MappedFileIterator(const void *bytes, size_t size) : bytes(bytes), size(size) {
    }
    constexpr const T *begin() noexcept {
        return static_cast<const T *>(bytes);
    }
    constexpr const T *end() noexcept {
        auto corrected_size = size - (size % sizeof(T));
        const void *endptr = static_cast<const void *>(&((static_cast<const char *>(bytes)[corrected_size])));
        return static_cast<const T *>(endptr);
    }
};

class MappedFile {
private:
    void *bytes;
    size_t size;
public:
    MappedFile(const MappedFile &cp);
    MappedFile(MappedFile &&mv);
    explicit MappedFile(const std::string &filename);
    ~MappedFile();
    const void *Content() noexcept;
    size_t Size() noexcept;

    template<typename T> MappedFileIterator<T> iterate() {
        return MappedFileIterator<T>(bytes, size);
    }
};


#endif //H5PRUNNER_MAPPEDFILE_H
