//
// Created by janeover on 16.11.2020.
//

#ifndef H5PRUNNER_FILEEXCEPTION_H
#define H5PRUNNER_FILEEXCEPTION_H


#include <cerrno>
#include <exception>
#include <string>
#include <optional>

class FileException : std::exception {
private:
    std::string error;
    std::optional<std::string> filename;
    std::optional<int> code;
    std::string prettyError;
public:
    FileException(std::string error, std::optional<std::string> filename, std::optional<int> code) noexcept;
    FileException(std::string error, std::optional<std::string> filename) noexcept : FileException(error, filename, errno) {
    }
    const char *what() const noexcept override;
};


#endif //H5PRUNNER_FILEEXCEPTION_H
