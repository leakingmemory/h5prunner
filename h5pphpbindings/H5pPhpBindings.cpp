//
// Created by janeover on 01.11.2020.
//

#include <h5prunner/H5pPhpBindings.h>
#include <h5prunner/PhpUtils.h>

#include <php.h>
#include <zend_exceptions.h>
#include <boost/algorithm/string.hpp>
#include <boost/format.hpp>
#include <sstream>
#include <cpprest/json.h>

static H5pPhpBindings *singleton = nullptr;

#define add_assoc_named_std_string(assoc, name, value) add_assoc_stringl(assoc, std::string(name).c_str(), value.c_str(), value.length())
#define add_assoc_std_string(assoc, value) add_assoc_named_std_string(assoc, #value, value)
#define eee_add_assoc_named_long(assoc, name, value) add_assoc_long_ex(assoc, name.c_str(), name.length(), value)
#define eee_add_assoc_long(assoc, value) eee_add_assoc_named_long(assoc, std::string(#value), value)
#define eee_add_assoc_named_bool(assoc, name, value) add_assoc_bool_ex(assoc, name.c_str(), name.length(), value)
#define eee_add_assoc_bool(assoc, value) eee_add_assoc_named_bool(assoc, std::string(#value), value)

static std::string zval_as_string(zval *z) {
    auto tp = Z_TYPE(*z);
    std::string str;
    switch (tp) {
        case IS_STRING: {
            zend_string *zstr = Z_STR(*z);
            return std::string(ZSTR_VAL(zstr), ZSTR_LEN(zstr));
        }
        case IS_DOUBLE: {
            double dbl = Z_DVAL(*z);
            return std::to_string(dbl);
        }
        case IS_LONG: {
            long lng = Z_LVAL(*z);
            return std::to_string(lng);
        }
        case IS_TRUE: {
            return std::string("true");
        }
        case IS_FALSE: {
            return std::string("false");
        }
        default:
            throw std::exception();
    }
}

static std::string zval_as_string_nofail(zval *z) {
    try {
        return zval_as_string(z);
    } catch (...) {
        return "";
    }
}

static long zval_as_long_nofail(zval *zv) {
    auto tp = Z_TYPE(*zv);
    switch (tp) {
        case IS_LONG:
            return Z_LVAL(*zv);
        case IS_DOUBLE:
            return (long) round(Z_DVAL(*zv));
        case IS_STRING: {
            zend_string *zstr = Z_STR(*zv);
            std::string str = ZSTR_STD(zstr);
            try {
                return std::stol(str);
            } catch (...) {
                return 0;
            }
        }
        case IS_TRUE:
            return 1;
        default:
            return 0;
    }
}

static bool zval_as_bool(zval *zv) {
    auto tp = Z_TYPE(*zv);
    switch (tp) {
        case IS_LONG:
            return Z_LVAL(*zv) != 0;
        case IS_DOUBLE:
            return abs(Z_DVAL(*zv)) > 0.0001;
        case IS_STRING: {
            zend_string *zstr = Z_STR(*zv);
            std::string str = ZSTR_STD(zstr);
            if (str.empty()) {
                return false;
            }
            return str != "0" && str != "false";
        }
        case IS_NULL:
        case IS_FALSE:
            return false;
        case IS_ARRAY: {
            HashTable *ht = Z_ARR(*zv);
            return zend_hash_num_elements(ht) > 0;
        }
        default:
            return true;
    }
}

void zval_merge_json(zval *zv, const web::json::value &json) {
    switch (json.type()) {
        case web::json::value::Number:
            convert_to_long(zv);
            ZVAL_LONG(zv, json.as_integer());
            break;
        case web::json::value::Boolean:
            convert_to_boolean(zv);
            ZVAL_BOOL(zv, json.as_bool());
            break;
        case web::json::value::String: {
            convert_to_string(zv);
            std::string str = json.as_string();
            ZVAL_STRINGL(zv, str.c_str(), str.length());
            break;
        }
        case web::json::value::Null:
            convert_to_null(zv);
            break;
        case web::json::value::Object: {
                if (Z_TYPE(*zv) != IS_ARRAY) {
                    convert_to_array(zv);
                    HashTable *ht = Z_ARR(*zv);
                    /*
                     * Usually converted item to array(item). So just remove item 0.
                     */
                    if (zend_hash_index_exists(ht, 0)) {
                        zend_hash_index_del(ht, 0);
                    }
                }
                auto json_object = json.as_object();
                std::map<std::string,web::json::value> map;
                for (auto &[key, value] : json_object) {
                    map.insert_or_assign(key, value);
                }
                HashTable *ht = Z_ARR(*zv);
                long numk;
                zend_string *zkey;
                zval *valz;
                std::string key;
                ZEND_HASH_FOREACH_KEY_VAL(ht, numk, zkey, valz)
                    if (zkey != NULL) {
                        key = ZSTR_STD(zkey);
                    } else {
                        key = std::to_string(numk);
                    }
                    auto iter = map.find(key);
                    if (iter != map.end()) {
                        zval_merge_json(valz, iter->second);
                        map.erase(key);
                    }
                ZEND_HASH_FOREACH_END();
                for (auto &[key, subval] : map) {
                    valz = zend_hash_str_add_empty_element(ht, key.c_str(), key.length());
                    ZVAL_NULL(valz);
                    zval_merge_json(valz, subval);
                }
                break;
            }
        case web::json::value::Array: {
            if (Z_TYPE(*zv) != IS_ARRAY) {
                convert_to_array(zv);
                HashTable *ht = Z_ARR(*zv);
                /*
                 * Usually converted item to array(item). So just remove item 0.
                 */
                if (zend_hash_index_exists(ht, 0)) {
                    zend_hash_index_del(ht, 0);
                }
            }
            auto json_array = json.as_array();
            HashTable *ht = Z_ARR(*zv);
            long idx = 0;
            zval *valz;
            for (auto &item : json_array) {
                while (zend_hash_index_find(ht, idx) != NULL) {
                    idx++;
                }
                valz = zend_hash_index_add_empty_element(ht, idx++);
                ZVAL_NULL(valz);
                zval_merge_json(valz, item);
            }
            break;
        }
    }
}

void zval_array_delete_items(zval *zv, std::vector<std::vector<std::string>> paths) {
    auto tp = Z_TYPE(*zv);
    if (tp == IS_ARRAY) {
        HashTable *ht = Z_ARR(*zv);
        std::map<std::string,std::vector<std::vector<std::string>>> subs;
        zval *subz;
        for (auto &path : paths) {
            if (path.size() > 0) {
                if (path.size() > 1) {
                    auto iterator = path.begin();
                    const std::string &first = *iterator;
                    std::vector<std::string> subpath;
                    for (; iterator < path.end(); iterator++) {
                        subpath.push_back(*iterator);
                    }
                    auto exist = subs.find(first);
                    if (exist == subs.end()) {
                        std::vector<std::vector<std::string>> subpaths = {subpath};
                        subs.insert_or_assign(first, subpaths);
                    } else {
                        exist->second.push_back(subpath);
                    }
                } else {
                    const std::string &del = path.front();

                    subz = zend_hash_str_find_ind(ht, del.c_str(), del.length());
                    if (subz != NULL) {
                        zend_hash_str_del(ht, del.c_str(), del.length());
                    }
                }
            }
        }
        for (auto &[key, subpaths] : subs) {
            subz = zend_hash_str_find_ind(ht, key.c_str(), key.length());
            if (subz != NULL) {
                zval_array_delete_items(subz, subpaths);
            }
        }
    }
}

static std::vector<std::tuple<std::string,std::string>> values_for_key(const std::string &key, zval *z) {
    auto tp = Z_TYPE(*z);
    switch (tp) {
        case IS_FALSE:
        case IS_TRUE:
        case IS_STRING:
        case IS_DOUBLE:
        case IS_LONG:
            return {{key, zval_as_string(z)}};
        case IS_ARRAY: {
            HashTable *hashTable = Z_ARR(*z);
            zval *zv;
            std::string arrayKey = key;
            arrayKey.append("[]");
            std::vector<std::tuple<std::string,std::string>> vector;
            ZEND_HASH_FOREACH_VAL(hashTable, zv)
                auto zvtype = Z_TYPE(*zv);
                switch (zvtype) {
                    case IS_STRING:
                    case IS_DOUBLE:
                    case IS_LONG:
                    case IS_TRUE:
                    case IS_FALSE:
                        vector.push_back({arrayKey, zval_as_string(zv)});
                }
            ZEND_HASH_FOREACH_END();
            return vector;
        }
        default:
            return {};
    }
}

static std::vector<std::tuple<std::string,std::string>> php_array_to_post_fields(zval *arr) {
    HashTable *hashTable = Z_ARR(*arr);
    int num_key;
    zend_string *zkey;
    zval *value;
    std::vector<std::tuple<std::string,std::string>> vector;
    ZEND_HASH_FOREACH_KEY_VAL(hashTable, num_key, zkey, value)
        std::string key;
        if (zkey != NULL) {
            key = std::string(ZSTR_VAL(zkey), ZSTR_LEN(zkey));
        } else {
            key = std::to_string(num_key);
        }
        for (auto &tuple : values_for_key(key, value)) {
            vector.push_back(tuple);
        }
    ZEND_HASH_FOREACH_END();
    return vector;
}

static std::vector<std::string> split_comma_sep(std::string str) {
    std::istringstream strstream(str);
    std::vector<std::string> vector;
    std::string token;
    while (std::getline(strstream, token, ',')) {
        boost::trim(token);
        vector.push_back(token);
    }
    return vector;
}

static std::vector<std::string> extract_h5p_array(zval *arr, std::string assocName) {
    HashTable *outer = Z_ARR(*arr);
    std::vector<std::string> vector;
    zval *zroot;
    ZEND_HASH_FOREACH_VAL(outer, zroot)
        if (Z_TYPE(*zroot) == IS_ARRAY) {
            HashTable *inner = Z_ARR(*zroot);
            int num_key;
            zend_string *zkey;
            zval *value;

            ZEND_HASH_FOREACH_KEY_VAL(inner, num_key, zkey, value)
                if (zkey != NULL) {
                    std::string key = std::string(ZSTR_VAL(zkey), ZSTR_LEN(zkey));
                    if (key == assocName) {
                        int zt = Z_TYPE(*value);
                        switch (zt) {
                            case IS_FALSE:
                            case IS_TRUE:
                            case IS_DOUBLE:
                            case IS_LONG:
                            case IS_STRING:
                                vector.push_back(zval_as_string(value));
                        }
                    }
                }
            ZEND_HASH_FOREACH_END();
        }
    ZEND_HASH_FOREACH_END();
    return vector;
}

static std::map<std::string,std::string> php_array_to_map(zval *arr) {
    HashTable *hashTable = Z_ARR(*arr);
    int num_key;
    zend_string *zkey;
    zval *value;
    std::map<std::string,std::string> map;
    ZEND_HASH_FOREACH_KEY_VAL(hashTable, num_key, zkey, value)
        std::string key;
        if (zkey != NULL) {
            key = std::string(ZSTR_VAL(zkey), ZSTR_LEN(zkey));
        } else {
            key = std::to_string(num_key);
        }
        int zt = Z_TYPE(*value);
        switch (zt) {
            case IS_FALSE:
            case IS_TRUE:
            case IS_DOUBLE:
            case IS_LONG:
            case IS_STRING:
                map.insert_or_assign(key, zval_as_string(value));
        }
    ZEND_HASH_FOREACH_END();
    return map;
}

static h5p::save_library_data read_save_library_data(zval *arr) {
    HashTable *hashTable = Z_ARR(*arr);
    int num_key;
    zend_string *zkey;
    zval *value;
    h5p::save_library_data data = {};

    ZEND_HASH_FOREACH_KEY_VAL(hashTable, num_key, zkey, value)
        std::string key;
        if (zkey != NULL) {
            key = std::string(ZSTR_VAL(zkey), ZSTR_LEN(zkey));
        } else {
            key = std::to_string(num_key);
        }
        auto tp = Z_TYPE(*value);
        switch (tp) {
            case IS_FALSE:
            case IS_TRUE:
            case IS_LONG: {
                long lv;
                if (tp == IS_LONG) {
                    lv = Z_LVAL(*value);
                } else if (tp == IS_TRUE) {
                    lv = 1;
                } else {
                    lv = 0;
                }
                if (key == "libraryId") {
                    data.libraryId = lv;
                    break;
                } else if (key == "majorVersion") {
                    data.majorVersion = lv;
                    break;
                } else if (key == "minorVersion") {
                    data.minorVersion = lv;
                    break;
                } else if (key == "patchVersion") {
                    data.patchVersion = lv;
                    break;
                } else if (key == "runnable") {
                    data.runnable = lv != 0;
                    break;
                } else if (key == "fullscreen") {
                    data.fullscreen = lv != 0;
                    break;
                }
            }
            case IS_STRING:
            case IS_DOUBLE: {
                std::string str = zval_as_string(value);
                try {
                    long lv = std::stol(str);
                    if (key == "libraryId") {
                        data.libraryId = lv;
                    } else if (key == "majorVersion") {
                        data.majorVersion = lv;
                    } else if (key == "minorVersion") {
                        data.minorVersion = lv;
                    } else if (key == "patchVersion") {
                        data.patchVersion = lv;
                    } else if (key == "runnable") {
                        data.runnable = lv != 0;
                    } else if (key == "fullscreen") {
                        data.fullscreen = lv != 0;
                    }
                } catch (...) {
                }
                if (key == "title") {
                    data.title = str;
                } else if (key == "machineName") {
                    data.machineName = str;
                } else if (key == "embedTypes") {
                    data.embedTypes = split_comma_sep(str);
                } else if (key == "semantics") {
                    data.semantics = str;
                }
            }
            break;
            case IS_ARRAY: {
                HashTable *subtb = Z_ARR(*value);
                int nkey;
                zend_string *subzkey;
                zval *subval;

                if (key == "metadataSettings") {
                    ZEND_HASH_FOREACH_KEY_VAL(subtb, nkey, subzkey, subval)
                            if (subzkey != NULL) {
                                std::string subkey = std::string(ZSTR_VAL(subzkey), ZSTR_LEN(subzkey));
                                auto fieldType = Z_TYPE(*subval);
                                std::optional<long> field = {};
                                switch (fieldType) {
                                    case IS_FALSE:
                                        field = 0;
                                        break;
                                    case IS_TRUE:
                                        field = 1;
                                        break;
                                    case IS_LONG:
                                        field = Z_LVAL(*subval);
                                        break;
                                    case IS_STRING:
                                    case IS_DOUBLE: {
                                        std::string str = zval_as_string(subval);
                                        try {
                                            field = std::stol(str);
                                        } catch (...) {
                                            field = {};
                                        }
                                    }
                                }
                                if (field) {
                                    if (subkey == "disable") {
                                        data.metadataSettings.disable = field.value() != 0;
                                    } else if (subkey == "disableExtraTitleField") {
                                        data.metadataSettings.disableExtraTitleField = field.value() != 0;
                                    }
                                }
                            }
                    ZEND_HASH_FOREACH_END();
                } else if (key == "preloadedJs") {
                    data.preloadedJs = extract_h5p_array(value, "path");
                } else if (key == "preloadedCss") {
                    data.preloadedCss = extract_h5p_array(value, "path");
                } else if (key == "dropLibraryCss") {
                    data.dropLibraryCss = extract_h5p_array(value, "machineName");
                } else if (key == "language") {
                    data.language = php_array_to_map(value);
                } else if (key == "addTo") {
                    data.addTo = zval_to_json(value).serialize();
                }
            }
        }
    ZEND_HASH_FOREACH_END();
    return data;
}

extern "C" {
    PHP_FUNCTION (h5phost_get_platform_info) {
        try {
            auto platformInfo = singleton->framework_interface()->getPlatformInfo();
            array_init(return_value);
            auto name = std::get<0>(platformInfo);
            auto version = std::get<1>(platformInfo);
            auto h5pVersion = std::get<2>(platformInfo);
            add_assoc_std_string(return_value, name);
            add_assoc_std_string(return_value, version);
            add_assoc_std_string(return_value, h5pVersion);
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION (h5phost_fetch_external_data) {
        char *uri;
        size_t uriLength;
        zval *data = NULL;
        zend_bool blocking;
        zend_bool blockingIsNull = 1;
        char *stream = NULL;
        size_t streamLength;


        ZEND_PARSE_PARAMETERS_START(1, 4)
            Z_PARAM_STRING(uri, uriLength)
            if (_num_args > 1) {
                Z_PARAM_ARRAY_EX(data, 1, 0)
                if (_num_args > 2) {
                    Z_PARAM_BOOL_EX(blocking, blockingIsNull, 1, 0)
                    if (_num_args > 3) {
                        Z_PARAM_STRING_EX(stream, streamLength, 1, 0)
                    }
                }
            }
        ZEND_PARSE_PARAMETERS_END();
        try {
            std::optional<std::map<std::string,std::vector<std::string>>> dataObj = {};
            if (data != NULL) {
                std::map<std::string,std::vector<std::string>> map;
                for (const auto &tuple : php_array_to_post_fields(data)) {
                    const std::string &key = std::get<0>(tuple);
                    const std::string &value = std::get<1>(tuple);
                    auto iterator = map.find(key);
                    if (iterator != map.end()) {
                        iterator->second.push_back(value);
                    } else {
                        std::vector<std::string> vector = {value};
                        map.insert_or_assign(key, vector);
                    }
                }
                dataObj = map;
            }

            std::optional<std::string> optStream = {};
            if (stream != NULL) {
                optStream = std::string(stream, streamLength);
            }
            std::optional<std::string> result = singleton->framework_interface()->fetchExternalData(
                    std::string(uri, uriLength),
                    dataObj,
                    (blockingIsNull != 0 || blocking != 0),
                    optStream);

            if (result) {
                RETURN_STRING(result->c_str());
            } else {
                RETURN_NULL();
            }
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_save_library_data) {
        zval *dataRef;
        zval *data;
        zend_bool newObject;

        ZEND_PARSE_PARAMETERS_START(2, 2)
            Z_PARAM_ZVAL(dataRef)
            Z_PARAM_BOOL(newObject)
        ZEND_PARSE_PARAMETERS_END();

        // Passed by reference
        data = Z_REFVAL(*dataRef);

        try {
            h5p::save_library_data lib_data = read_save_library_data(data);
            singleton->framework_interface()->saveLibraryData(lib_data, newObject);
            if (newObject) {
                HashTable *hashTable = Z_ARR(*data);
                int num_key;
                zend_string *zkey;
                zval *value;

                /*
                 * Two RETURN paths below:
                 */
                ZEND_HASH_FOREACH_KEY_VAL(hashTable, num_key, zkey, value)
                    if (zkey != NULL) {
                        std::string key = std::string(ZSTR_VAL(zkey), ZSTR_LEN(zkey));
                        if (key == "libraryId") {
                            if (Z_TYPE(*value) == IS_LONG) {
                                ZVAL_LONG(value, (long) lib_data.libraryId);

                                // RETURN: After updating libraryId
                                return;
                            } else {
                                zend_hash_del(hashTable, zkey);
                            }
                        }
                    }
                ZEND_HASH_FOREACH_END();

                std::string key = "libraryId";
                add_assoc_long_ex(data, key.c_str(), key.length(), lib_data.libraryId);

                // RETURN: After adding libraryId
            }
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_set_library_tutorial_url) {
        zend_string *z_machineName;
        zend_string *z_tutorialUrl;

        ZEND_PARSE_PARAMETERS_START(2, 2)
            Z_PARAM_STR(z_machineName)
            Z_PARAM_STR(z_tutorialUrl)
        ZEND_PARSE_PARAMETERS_END();

        try {
            std::string machineName = ZSTR_STD(z_machineName);
            std::string tutorialUrl = ZSTR_STD(z_tutorialUrl);

            singleton->framework_interface()->setLibraryTutorialUrl(machineName, tutorialUrl);
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_set_error_message) {
        zend_string *z_message;
        zval *z_code = NULL;

        ZEND_PARSE_PARAMETERS_START(1, 2)
            Z_PARAM_STR(z_message)
            if (_num_args > 1) {
                Z_PARAM_ZVAL_EX(z_code, 0, 0)
            }
        ZEND_PARSE_PARAMETERS_END();

        try {
            std::string message = ZSTR_STD(z_message);
            std::optional<std::string> code = {};
            if (z_code != NULL) {
                int zt = Z_TYPE(*z_code);
                switch (zt) {
                    case IS_FALSE:
                    case IS_TRUE:
                    case IS_LONG:
                    case IS_DOUBLE:
                    case IS_STRING:
                        code = zval_as_string(z_code);
                }
            }

            singleton->framework_interface()->setErrorMessage(message, code);
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_set_info_message) {
        zend_string *z_message;

        ZEND_PARSE_PARAMETERS_START(1, 1)
                Z_PARAM_STR(z_message)
        ZEND_PARSE_PARAMETERS_END();

        try {
            std::string message = ZSTR_STD(z_message);

            singleton->framework_interface()->setInfoMessage(message);
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_get_messages) {
        zend_string *z_type;

        ZEND_PARSE_PARAMETERS_START(1, 1)
                Z_PARAM_STR(z_type)
        ZEND_PARSE_PARAMETERS_END();

        try {
            std::string messageType = ZSTR_STD(z_type);

            h5p::message_type mt;

            if (messageType == "error") {
                mt = h5p::error;
            } else if (messageType == "info") {
                mt = h5p::info;
            } else {
                throw std::exception();
            }

            auto messages = singleton->framework_interface()->getMessages(mt);
            array_init(return_value);
            for (const auto &msg : messages) {
                add_next_index_stringl(return_value, msg.c_str(), msg.length());
            }
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_t) {
        zend_string *z_message;
        zval *z_arr;

        ZEND_PARSE_PARAMETERS_START(2, 2)
            Z_PARAM_STR(z_message)
            Z_PARAM_ARRAY(z_arr)
        ZEND_PARSE_PARAMETERS_END();

        try {
            std::string message = ZSTR_STD(z_message);
            std::map<std::string,std::string> replacements = php_array_to_map(z_arr);

            std::string text = singleton->framework_interface()->t(message, replacements);

            RETURN_STRINGL(text.c_str(), text.length());
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_get_library_file_url) {
        zend_string *z_folder;
        zend_string *z_filename;

        ZEND_PARSE_PARAMETERS_START(2, 2)
            Z_PARAM_STR(z_folder)
            Z_PARAM_STR(z_filename)
        ZEND_PARSE_PARAMETERS_END();

        try {
            std::string url = singleton->framework_interface()->getLibraryFileUrl(ZSTR_STD(z_folder), ZSTR_STD(z_filename));
            RETURN_STRINGL(url.c_str(), url.length());
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_get_uploaded_h5p_folder_path) {
        try {
            std::string path = singleton->framework_interface()->getUploadedH5pFolderPath();
            RETURN_STRINGL(path.c_str(), path.length());
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_get_uploaded_h5p_path) {
        try {
            std::string path = singleton->framework_interface()->getUploadedH5pPath();
            RETURN_STRINGL(path.c_str(), path.length());
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_load_addons) {
        try {
            std::vector<h5p::library_addon> addons = singleton->framework_interface()->loadAddons();
            array_init(return_value);
            HashTable *ht = Z_ARR(*return_value);
            unsigned long idx = 0;
            for (auto &addon : addons) {
                int libraryId = addon.libraryId;
                std::string machineName = addon.machineName;
                int majorVersion = addon.majorVersion;
                int minorVersion = addon.minorVersion;
                int patchVersion = addon.patchVersion;
                std::string addTo = addon.addTo;
                std::string preloadedJs = std::accumulate(addon.preloadedJs.begin(), addon.preloadedJs.end(), std::string(""), [] (const std::string &a, const std::string &b) {
                    if (!a.empty()) {
                        std::string acc(a);
                        acc.append(",");
                        acc.append(b);
                        return acc;
                    } else {
                        return b;
                    }
                });
                std::string preloadedCss = std::accumulate(addon.preloadedCss.begin(), addon.preloadedCss.end(), std::string(""), [] (const std::string &a, const std::string &b) {
                    if (!a.empty()) {
                        std::string acc(a);
                        acc.append(",");
                        acc.append(b);
                        return acc;
                    } else {
                        return b;
                    }
                });
                zval *subarr = zend_hash_index_add_empty_element(ht, idx++);
                array_init(subarr);
                eee_add_assoc_long(subarr, libraryId);
                add_assoc_std_string(subarr, machineName);
                eee_add_assoc_long(subarr, majorVersion);
                eee_add_assoc_long(subarr, minorVersion);
                eee_add_assoc_long(subarr, patchVersion);
                add_assoc_std_string(subarr, addTo);
                add_assoc_std_string(subarr, preloadedJs);
                add_assoc_std_string(subarr, preloadedCss);
            }
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_get_library_config) {
        HashTable *libHt;

        ZEND_PARSE_PARAMETERS_START(1, 1)
            Z_PARAM_ARRAY_HT(libHt)
        ZEND_PARSE_PARAMETERS_END();

        try {
            std::vector<std::string> libraries;
            int num_key;
            zend_string *zkey;
            zval *value;
            ZEND_HASH_FOREACH_KEY_VAL(libHt, num_key, zkey, value)
                int zt = Z_TYPE(*value);
                switch (zt) {
                    case IS_FALSE:
                    case IS_TRUE:
                    case IS_DOUBLE:
                    case IS_LONG:
                    case IS_STRING:
                        libraries.push_back(zval_as_string(value));
                }
            ZEND_HASH_FOREACH_END();
            singleton->framework_interface()->getLibraryConfig(libraries);
            array_init(return_value);
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_load_libraries) {
        try {
            std::map<std::string,std::vector<h5p::library_load>> ll_map = singleton->framework_interface()->loadLibraries();
            array_init(return_value);
            HashTable *ret_ht = Z_ARR(*return_value);
            for (auto &[machineName, ll_v] : ll_map) {
                zval *mac_zarr = zend_hash_str_add_empty_element(ret_ht, machineName.c_str(), machineName.length());
                array_init(mac_zarr);
                unsigned long idx = 0;
                HashTable *mac_ht = Z_ARR(*mac_zarr);
                for (auto &ll : ll_v) {
                    zval *ll_zarr = zend_hash_index_add_empty_element(mac_ht, idx++);
                    array_init(ll_zarr);
                    HashTable *ll_ht = Z_ARR(*ll_zarr);
                    unsigned int id = ll.id;
                    std::string name = ll.name;
                    unsigned int major_version = ll.major_version;
                    unsigned int minor_version = ll.minor_version;
                    unsigned int patch_version = ll.patch_version;
                    std::string title = ll.title;
                    bool restricted = ll.restricted;
                    bool runnable = ll.runnable;
                    eee_add_assoc_long(ll_zarr, id);
                    add_assoc_std_string(ll_zarr, name);
                    eee_add_assoc_long(ll_zarr, major_version);
                    eee_add_assoc_long(ll_zarr, minor_version);
                    eee_add_assoc_long(ll_zarr, patch_version);
                    add_assoc_std_string(ll_zarr, title);
                    eee_add_assoc_bool(ll_zarr, restricted);
                    eee_add_assoc_bool(ll_zarr, runnable);
                }
            }
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_get_admin_url) {
        try {
            std::optional<std::string> admin_url = singleton->framework_interface()->getAdminUrl();
            if (admin_url) {
                const std::string &au = admin_url.value();
                RETURN_STRINGL(au.c_str(), au.length());
            } else {
                RETURN_NULL();
            }
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_get_library_id) {
        zend_string *z_machine_name;
        zend_long z_major_v, z_minor_v;
        zend_bool z_major_v_null, z_minor_v_null;
        ZEND_PARSE_PARAMETERS_START(1, 3)
            Z_PARAM_STR(z_machine_name);
            if (_num_args > 1) {
                Z_PARAM_LONG_EX(z_major_v, z_major_v_null, 1, 0)
                if (_num_args > 2) {
                    Z_PARAM_LONG_EX(z_minor_v, z_minor_v_null, 1, 0)
                } else {
                    z_minor_v_null = 1;
                }
            } else {
                z_major_v_null = 1;
                z_minor_v_null = 1;
            }
        ZEND_PARSE_PARAMETERS_END();

        try {
            std::string machine_name = ZSTR_STD(z_machine_name);
            std::optional<unsigned int> major_v = {};
            std::optional<unsigned int> minor_v = {};

            if (!(z_major_v_null)) {
                major_v = z_major_v;
                if (!(z_minor_v_null)) {
                    minor_v = z_minor_v;
                }
            }

            std::optional<unsigned int> id = singleton->framework_interface()->getLibraryId(machine_name, major_v, minor_v);

            if (id) {
                RETURN_LONG(id.value());
            } else {
                RETURN_BOOL(false);
            }
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_get_whitelist) {
        zend_bool is_library;
        zend_string *default_content;
        zend_string *default_library;

        ZEND_PARSE_PARAMETERS_START(3, 3)
            Z_PARAM_BOOL(is_library)
            Z_PARAM_STR(default_content)
            Z_PARAM_STR(default_library)
        ZEND_PARSE_PARAMETERS_END();

        try {
            std::string allowlist = singleton->framework_interface()->getWhitelist(is_library, ZSTR_STD(default_content), ZSTR_STD(default_library));
            RETURN_STRINGL(allowlist.c_str(), allowlist.length());
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    h5p::library_with_version library_with_version(HashTable *arr) {
        h5p::library_with_version libv = {};
        int index;
        zend_string *zkey;
        zval *zvalue;
        std::string key;

        ZEND_HASH_FOREACH_KEY_VAL(arr, index, zkey, zvalue)
            if (zkey != NULL) {
                key = ZSTR_STD(zkey);
                if (key == "machineName") {
                    try {
                        libv.machineName = zval_as_string(zvalue);
                    } catch (...) {}
                } else if (key == "majorVersion") {
                    libv.majorVersion = zval_as_long_nofail(zvalue);
                } else if (key == "minorVersion") {
                    libv.minorVersion = zval_as_long_nofail(zvalue);
                } else if (key == "patchVersion") {
                    libv.patchVersion = zval_as_long_nofail(zvalue);
                }
            }
        ZEND_HASH_FOREACH_END();

        return libv;
    }

    PHP_FUNCTION(h5phost_is_patched_library) {
        HashTable *arr;
        h5p::library_with_version libv;

        ZEND_PARSE_PARAMETERS_START(1, 1)
            Z_PARAM_ARRAY_HT(arr)
        ZEND_PARSE_PARAMETERS_END();


        try {
            libv = library_with_version(arr);

            bool is_patched = singleton->framework_interface()->isPatchedLibrary(libv);

            RETURN_BOOL(is_patched);
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_is_in_dev_mode) {
        try {
            RETURN_BOOL(singleton->framework_interface()->isInDevMode());
        } catch (...) {

        }
    }

    PHP_FUNCTION(h5phost_may_update_libraries) {
        try {
            RETURN_BOOL(singleton->framework_interface()->mayUpdateLibraries());
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    static h5p::content_data read_content_data(HashTable *content_ht) {
        long num_key;
        zend_string *zkey;
        zval *zvalue;

        h5p::content_data content = {};

        ZEND_HASH_FOREACH_KEY_VAL(content_ht, num_key, zkey, zvalue)
                if (zkey != NULL) {
                    std::string key = ZSTR_STD(zkey);

                    if (key == "id") {
                        content.id = zval_as_long_nofail(zvalue);
                    } else if (key == "params") {
                        content.json = zval_as_string_nofail(zvalue);
                    } else if (key == "library" && Z_TYPE(*zvalue) == IS_ARRAY) {
                        HashTable *sht = Z_ARR(*zvalue);
                        long nk;
                        zend_string *sub_zk;
                        zval *subz;

                        ZEND_HASH_FOREACH_KEY_VAL(sht, nk, sub_zk, subz)
                                if (sub_zk != NULL && ZSTR_STD(sub_zk) == "libraryId") {
                                    content.library = zval_as_long_nofail(subz);
                                }
                        ZEND_HASH_FOREACH_END();
                    } else if (key == "title") {
                        content.title = zval_as_string_nofail(zvalue);
                    } else if (key == "embed_type") {
                        content.embed_type = split_comma_sep(zval_as_string_nofail(zvalue));
                    } else if (key == "disable") {
                        content.disable = zval_as_bool(zvalue);
                    } else if (key == "max_score") {
                        content.max_score = zval_as_long_nofail(zvalue);
                    } else if (key == "slug") {
                        content.slug = zval_as_string_nofail(zvalue);
                    } else if (key == "user_id") {
                        content.user_id = zval_as_string_nofail(zvalue);
                    } else if (key == "is_published") {
                        content.is_published = zval_as_bool(zvalue);
                    } else if (key == "is_private") {
                        content.is_private = zval_as_bool(zvalue);
                    } else if (key == "language_iso_639_3") {
                        content.language_iso_639_3 = zval_as_string_nofail(zvalue);
                    } else if (key == "metadata") {
                        HashTable *sht = Z_ARR(*zvalue);
                        long nk;
                        zend_string *sub_zk;
                        zval *subz;

                        ZEND_HASH_FOREACH_KEY_VAL(sht, nk, sub_zk, subz)
                                std::string key;
                                if (sub_zk != NULL) {
                                    key = ZSTR_STD(sub_zk);
                                } else {
                                    key = std::to_string(nk);
                                }
                                content.metadata.insert_or_assign(key, zval_as_string_nofail(subz));
                        ZEND_HASH_FOREACH_END();
                    }
                }
        ZEND_HASH_FOREACH_END();

        return content;
    }

    PHP_FUNCTION(h5phost_insert_content) {
        HashTable *content_ht;
        long main_content_id_l;
        zend_bool main_content_id_is_null;

        ZEND_PARSE_PARAMETERS_START(1, 2)
            Z_PARAM_ARRAY_HT(content_ht)
            if (_num_args > 1) {
                Z_PARAM_LONG_EX(main_content_id_l, main_content_id_is_null, 0, 0)
            } else {
                main_content_id_is_null = true;
            }
        ZEND_PARSE_PARAMETERS_END();

        long num_key;
        zend_string *zkey;
        zval *zvalue;

        try {
            std::optional<unsigned int> main_content_id = {};
            if (!main_content_id_is_null) {
                main_content_id = main_content_id_l;
            }
            h5p::content_data content = read_content_data(content_ht);
            unsigned int contentId = singleton->framework_interface()->insertContent(content, main_content_id);

            RETURN_LONG(contentId);
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_update_content) {
        HashTable *content_ht;
        long main_content_id_l;
        zend_bool main_content_id_is_null;

        ZEND_PARSE_PARAMETERS_START(1, 2)
            Z_PARAM_ARRAY_HT(content_ht)
            if (_num_args > 1) {
                Z_PARAM_LONG_EX(main_content_id_l, main_content_id_is_null, 0, 0)
            } else {
                main_content_id_is_null = true;
            }
        ZEND_PARSE_PARAMETERS_END();

        long num_key;
        zend_string *zkey;
        zval *zvalue;

        try {
            std::optional<unsigned int> main_content_id = {};
            if (!main_content_id_is_null) {
                main_content_id = main_content_id_l;
            }
            h5p::content_data content = read_content_data(content_ht);
            singleton->framework_interface()->updateContent(content, main_content_id);
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_reset_content_user_data) {
        long content_id;

        ZEND_PARSE_PARAMETERS_START(1, 1)
            Z_PARAM_LONG(content_id)
        ZEND_PARSE_PARAMETERS_END();

        try {
            singleton->framework_interface()->resetContentUserData(content_id);
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_save_library_dependencies) {
        long library_id;
        HashTable *deps_ht;
        zend_string *zdeptype;

        ZEND_PARSE_PARAMETERS_START(3, 3)
            Z_PARAM_LONG(library_id)
            Z_PARAM_ARRAY_HT(deps_ht)
            Z_PARAM_STR(zdeptype)
        ZEND_PARSE_PARAMETERS_END();

        zval *dep_z;
        HashTable *dep_ht;
        long num;
        zend_string *zkey;
        zval *zv;
        std::string sdeptype;

        std::vector<h5p::library_without_patch_version> libraries;
        h5p::dependency_type deptype;

        try {
            sdeptype = ZSTR_STD(zdeptype);
            if (sdeptype == "editor") {
                deptype = h5p::editor;
            } else if (sdeptype == "preloaded") {
                deptype = h5p::preloaded;
            } else if (sdeptype == "dynamic") {
                deptype = h5p::dynamic;
            } else {
                throw std::exception();
            }

            ZEND_HASH_FOREACH_VAL(deps_ht, dep_z)
                if (Z_TYPE(*dep_z) == IS_ARRAY) {
                    dep_ht = Z_ARR(*dep_z);
                    h5p::library_without_patch_version lib = {};
                    ZEND_HASH_FOREACH_KEY_VAL(dep_ht, num, zkey, zv)
                        if (zkey != NULL) {
                            std::string key = ZSTR_STD(zkey);
                            if (key == "machineName") {
                                lib.machineName = zval_as_string_nofail(zv);
                            } else if (key == "majorVersion") {
                                lib.majorVersion = zval_as_long_nofail(zv);
                            } else if (key == "minorVersion") {
                                lib.minorVersion = zval_as_long_nofail(zv);
                            }
                        }
                    ZEND_HASH_FOREACH_END();
                    libraries.push_back(lib);
                }
            ZEND_HASH_FOREACH_END();

            singleton->framework_interface()->saveLibraryDependencies(library_id, libraries, deptype);
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_copy_library_usage) {
        long content_id;
        long copy_from_id;
        long content_main_id_l;
        zend_bool content_main_id_is_null;
        std::optional<unsigned int> content_main_id = {};

        ZEND_PARSE_PARAMETERS_START(2, 3)
            Z_PARAM_LONG(content_id)
            Z_PARAM_LONG(copy_from_id)
            if (_num_args > 2) {
                Z_PARAM_LONG_EX(content_main_id_l, content_main_id_is_null, 0,0)
            } else {
                content_main_id_is_null = true;
            }
        ZEND_PARSE_PARAMETERS_END();

        try {
            if (!content_main_id_is_null) {
                content_main_id = content_main_id_l;
            }

            singleton->framework_interface()->copyLibraryUsage(content_id, copy_from_id, content_main_id);
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_delete_content_data) {
        long content_id;

        ZEND_PARSE_PARAMETERS_START(1, 1)
                Z_PARAM_LONG(content_id)
        ZEND_PARSE_PARAMETERS_END();

        try {
            singleton->framework_interface()->deleteContentData(content_id);
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_delete_library_usage) {
        long content_id;

        ZEND_PARSE_PARAMETERS_START(1, 1)
                Z_PARAM_LONG(content_id)
        ZEND_PARSE_PARAMETERS_END();

        try {
            singleton->framework_interface()->deleteLibraryUsage(content_id);
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    static h5p::library_dependency read_library_dependency(HashTable *ht) {
        h5p::library_dependency libdep = {};
        long num;
        zend_string *zkey;
        std::string key;
        zval *zv;
        std::string machine_name;
        std::optional<std::vector<std::string>> drop_css;

        ZEND_HASH_FOREACH_KEY_VAL(ht, num, zkey, zv)
            if (zkey != NULL) {
                key = ZSTR_STD(zkey);
                if (key == "machineName") {
                    machine_name = zval_as_string_nofail(zv);
                } else if (key == "dropLibraryCss") {
                    std::string commasep = zval_as_string_nofail(zv);
                    drop_css = split_comma_sep(commasep);
                } else if (key == "libraryId") {
                    libdep.library_id = zval_as_long_nofail(zv);
                }
            }
        ZEND_HASH_FOREACH_END();
        if (drop_css) {
            const std::vector<std::string> &dp = drop_css.value();
            libdep.drop_css = false;
            for (const std::string &dcss : dp) {
                if (dcss == machine_name) {
                    libdep.drop_css = true;
                    break;
                }
            }
        }
        return libdep;
    }

    PHP_FUNCTION(h5phost_save_library_usage) {
        long content_id;
        HashTable *libraries_ht;

        ZEND_PARSE_PARAMETERS_START(2, 2)
            Z_PARAM_LONG(content_id)
            Z_PARAM_ARRAY_HT(libraries_ht)
        ZEND_PARSE_PARAMETERS_END();

        zval *libdep_z;
        HashTable *libdep_ht;
        long num;
        zend_string *zkey;
        zval *zv;
        std::vector<h5p::library_dependency> libdeps;

        try {
            ZEND_HASH_FOREACH_VAL(libraries_ht, libdep_z)
                if (Z_TYPE(*libdep_z) == IS_ARRAY) {
                    std::optional<h5p::library_dependency> libdep = {};
                    h5p::dependency_type deptype = h5p::dynamic;

                    libdep_ht = Z_ARR(*libdep_z);
                    ZEND_HASH_FOREACH_KEY_VAL(libdep_ht, num, zkey, zv)
                        if (zkey != NULL) {
                            std::string key = ZSTR_STD(zkey);
                            if (key == "library") {
                                if (Z_TYPE(*zv) == IS_ARRAY) {
                                    HashTable *ht = Z_ARR(*zv);
                                    libdep = read_library_dependency(ht);
                                }
                            } else if (key == "type") {
                                std::string tp = zval_as_string_nofail(zv);
                                if (tp == "editor") {
                                    deptype = h5p::editor;
                                } else if (tp == "preloaded") {
                                    deptype = h5p::preloaded;
                                } else if (tp == "dynamic") {
                                    deptype = h5p::dynamic;
                                }
                            }
                        }
                    ZEND_HASH_FOREACH_END();
                    if (libdep) {
                        auto ld = libdep.value();
                        ld.type = deptype;
                        libdeps.push_back(ld);
                    }
                }
            ZEND_HASH_FOREACH_END();

            singleton->framework_interface()->saveLibraryUsage(content_id, libdeps);
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_get_library_usage) {
        long library_id;
        zend_bool skip_content_b;
        zend_bool skip_content_is_null;

        ZEND_PARSE_PARAMETERS_START(1, 2)
            Z_PARAM_LONG(library_id)
            if (_num_args > 1) {
                Z_PARAM_BOOL_EX(skip_content_b, skip_content_is_null, 0, 0)
            } else {
                skip_content_is_null = true;
            }
        ZEND_PARSE_PARAMETERS_END();

        try {
            bool skip_content = !skip_content_is_null && skip_content_b;
            auto usage = singleton->framework_interface()->getLibraryUsage(library_id, skip_content);
            array_init(return_value);
            long libraries = usage.libraries;
            long content = usage.content;
            eee_add_assoc_long(return_value, libraries);
            eee_add_assoc_long(return_value, content);
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_load_library) {
        zend_string *z_machine_name;
        long major_version;
        long minor_version;
        std::string machine_name;

        ZEND_PARSE_PARAMETERS_START(3, 3)
            Z_PARAM_STR(z_machine_name)
            Z_PARAM_LONG(major_version)
            Z_PARAM_LONG(minor_version)
        ZEND_PARSE_PARAMETERS_END();

        try {
            machine_name = ZSTR_STD(z_machine_name);
            std::optional<h5p::load_library_data> load_lib = singleton->framework_interface()->loadLibrary(machine_name, major_version, minor_version);

            if (load_lib) {
                auto ll = load_lib.value();
                array_init(return_value);
                {
                    long libraryId = ll.library.libraryId;
                    std::string title = ll.library.title;
                    std::string machineName = ll.library.machineName;
                    long majorVersion = ll.library.majorVersion;
                    long minorVersion = ll.library.minorVersion;
                    long patchVersion = ll.library.patchVersion;
                    long runnable = ll.library.runnable ? 1 : 0;
                    long fullscreen = ll.library.fullscreen ? 1 : 0;
                    eee_add_assoc_long(return_value, libraryId);
                    add_assoc_std_string(return_value, title);
                    add_assoc_std_string(return_value, machineName);
                    eee_add_assoc_long(return_value, majorVersion);
                    eee_add_assoc_long(return_value, minorVersion);
                    eee_add_assoc_long(return_value, patchVersion);
                    eee_add_assoc_long(return_value, runnable);
                    eee_add_assoc_long(return_value, fullscreen);
                }
                {
                    std::string embedTypes = std::accumulate(ll.library.embedTypes.begin(), ll.library.embedTypes.end(), std::string(""),
                                                             [] (const std::string &a, const std::string &b) {
                        if (!a.empty()) {
                            std::string acc(a);
                            acc.append(",");
                            acc.append(b);
                            return acc;
                        } else {
                            return b;
                        }
                    });
                    add_assoc_std_string(return_value, embedTypes);
                }
                {
                    std::string preloadedJs = std::accumulate(ll.library.preloadedJs.begin(), ll.library.preloadedJs.end(), std::string(""),
                                                             [] (const std::string &a, const std::string &b) {
                                                                 if (!a.empty()) {
                                                                     std::string acc(a);
                                                                     acc.append(",");
                                                                     acc.append(b);
                                                                     return acc;
                                                                 } else {
                                                                     return b;
                                                                 }
                                                             });
                    add_assoc_std_string(return_value, preloadedJs);
                }
                {
                    std::string preloadedCss = std::accumulate(ll.library.preloadedCss.begin(), ll.library.preloadedCss.end(), std::string(""),
                                                             [] (const std::string &a, const std::string &b) {
                                                                 if (!a.empty()) {
                                                                     std::string acc(a);
                                                                     acc.append(",");
                                                                     acc.append(b);
                                                                     return acc;
                                                                 } else {
                                                                     return b;
                                                                 }
                                                             });
                    add_assoc_std_string(return_value, preloadedCss);
                }
                {
                    long idx = 0;
                    std::vector<std::string> dcss_vec = ll.library.dropLibraryCss;
                    std::string key = "dropLibraryCss";
                    zval *zv = zend_hash_str_add_empty_element(Z_ARR(*return_value), key.c_str(), key.length());
                    HashTable *ht;
                    zval *zsubarr;
                    array_init(zv);
                    ht = Z_ARR(*zv);
                    for (auto &machineName : dcss_vec) {
                        zsubarr = zend_hash_index_add_empty_element(ht, idx++);
                        array_init(zsubarr);
                        add_assoc_std_string(zsubarr, machineName);
                    }
                }
                if (ll.library.semantics) {
                    std::string semantics = ll.library.semantics.value();
                    add_assoc_std_string(return_value, semantics);
                }
                if (ll.dependencies.preloadedDependencies) {
                    auto &preloadedDependencies = ll.dependencies.preloadedDependencies.value();
                    long idx = 0;
                    std::string key = "preloadedDependencies";
                    zval *zv = zend_hash_str_add_empty_element(Z_ARR(*return_value), key.c_str(), key.length());
                    HashTable *ht;
                    zval *zsubarr;
                    std::string machineName;
                    long majorVersion;
                    long minorVersion;
                    array_init(zv);
                    ht = Z_ARR(*zv);
                    for (auto &preloaded_dep : preloadedDependencies) {
                        zsubarr = zend_hash_index_add_empty_element(ht, idx++);
                        array_init(zsubarr);
                        machineName = preloaded_dep.machineName;
                        majorVersion = preloaded_dep.majorVersion;
                        minorVersion = preloaded_dep.minorVersion;
                        add_assoc_std_string(zsubarr, machineName);
                        eee_add_assoc_long(zsubarr, majorVersion);
                        eee_add_assoc_long(zsubarr, minorVersion);
                    }
                }
                if (ll.dependencies.dynamicDependencies) {
                    auto &preloadedDependencies = ll.dependencies.dynamicDependencies.value();
                    long idx = 0;
                    std::string key = "dynamicDependencies";
                    zval *zv = zend_hash_str_add_empty_element(Z_ARR(*return_value), key.c_str(), key.length());
                    HashTable *ht;
                    zval *zsubarr;
                    std::string machineName;
                    long majorVersion;
                    long minorVersion;
                    array_init(zv);
                    ht = Z_ARR(*zv);
                    for (auto &preloaded_dep : preloadedDependencies) {
                        zsubarr = zend_hash_index_add_empty_element(ht, idx++);
                        array_init(zsubarr);
                        machineName = preloaded_dep.machineName;
                        majorVersion = preloaded_dep.majorVersion;
                        minorVersion = preloaded_dep.minorVersion;
                        add_assoc_std_string(zsubarr, machineName);
                        eee_add_assoc_long(zsubarr, majorVersion);
                        eee_add_assoc_long(zsubarr, minorVersion);
                    }
                }
                if (ll.dependencies.editorDependencies) {
                    auto &preloadedDependencies = ll.dependencies.editorDependencies.value();
                    long idx = 0;
                    std::string key = "editorDependencies";
                    zval *zv = zend_hash_str_add_empty_element(Z_ARR(*return_value), key.c_str(), key.length());
                    HashTable *ht;
                    zval *zsubarr;
                    std::string machineName;
                    long majorVersion;
                    long minorVersion;
                    array_init(zv);
                    ht = Z_ARR(*zv);
                    for (auto &preloaded_dep : preloadedDependencies) {
                        zsubarr = zend_hash_index_add_empty_element(ht, idx++);
                        array_init(zsubarr);
                        machineName = preloaded_dep.machineName;
                        majorVersion = preloaded_dep.majorVersion;
                        minorVersion = preloaded_dep.minorVersion;
                        add_assoc_std_string(zsubarr, machineName);
                        eee_add_assoc_long(zsubarr, majorVersion);
                        eee_add_assoc_long(zsubarr, minorVersion);
                    }
                }
            } else {
                RETURN_BOOL(false);
            }
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_load_library_semantics) {
        zend_string *z_machine_name;
        long major_version;
        long minor_version;
        std::string machine_name;

        ZEND_PARSE_PARAMETERS_START(3, 3)
                Z_PARAM_STR(z_machine_name)
                Z_PARAM_LONG(major_version)
                Z_PARAM_LONG(minor_version)
        ZEND_PARSE_PARAMETERS_END();

        try {
            machine_name = ZSTR_STD(z_machine_name);
            std::optional<std::string> semantics = singleton->framework_interface()->loadLibrarySemantics(machine_name,
                                                                                                           major_version,
                                                                                                           minor_version);
            if (semantics) {
                std::string str = semantics.value();
                RETURN_STRINGL(str.c_str(), str.length());
            } else {
                RETURN_NULL();
            }
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_alter_library_semantics) {
        zval *sem_ref_z;
        zend_string *machine_name_z;
        long majorVersion;
        long minorVersion;
        std::string machine_name;

        ZEND_PARSE_PARAMETERS_START(4, 4)
            Z_PARAM_ZVAL(sem_ref_z)
            Z_PARAM_STR(machine_name_z)
            Z_PARAM_LONG(majorVersion)
            Z_PARAM_LONG(minorVersion)
        ZEND_PARSE_PARAMETERS_END();

        zval *sem_z = Z_REFVAL(*sem_ref_z);

        try {
            web::json::value json = zval_to_json(sem_z);
            machine_name = ZSTR_STD(machine_name_z);
            std::optional<h5p::json_modifications> mod_opt = singleton->framework_interface()->alterLibrarySemantics(json, machine_name, majorVersion, minorVersion);

            if (mod_opt) {
                const h5p::json_modifications &mod = mod_opt.value();
                zval_array_delete_items(sem_z, mod.delete_paths);
                zval_merge_json(sem_z, mod.merge);
            }
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_delete_library_dependencies) {
        long library_id;

        ZEND_PARSE_PARAMETERS_START(1, 1)
            Z_PARAM_LONG(library_id)
        ZEND_PARSE_PARAMETERS_END();

        try {
            singleton->framework_interface()->deleteLibraryDependencies(library_id);
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_lock_dependency_storage) {
        try {
            singleton->framework_interface()->lockDependencyStorage();
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_unlock_dependency_storage) {
        try {
            singleton->framework_interface()->unlockDependencyStorage();
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_delete_library) {
        long library_id;

        ZEND_PARSE_PARAMETERS_START(1, 1)
            Z_PARAM_LONG(library_id)
        ZEND_PARSE_PARAMETERS_END();

        try {
            singleton->framework_interface()->deleteLibrary(library_id);
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_load_content) {
        long content_id;

        ZEND_PARSE_PARAMETERS_START(1, 1)
            Z_PARAM_LONG(content_id)
        ZEND_PARSE_PARAMETERS_END();

        try {
            std::optional<h5p::content_load> clopt = singleton->framework_interface()->loadContent(content_id);
            if (clopt) {
                const h5p::content_load &cl = clopt.value();
                array_init(return_value);
                {
                    unsigned int id = cl.contentId;
                    unsigned int contentId = cl.contentId;
                    std::string params = cl.json;
                    std::string title = cl.title;
                    long disable = cl.disable ? 1 : 0;
                    std::string user_id = cl.user_id;
                    int max_score = cl.max_score;
                    unsigned int libraryId = cl.libraryId;
                    std::string libraryName = cl.libraryName;
                    unsigned int libraryMajorVersion = cl.libraryMajorVersion;
                    unsigned int libraryMinorVersion = cl.libraryMinorVersion;
                    long libraryFullscreen = cl.libraryFullscreen;
                    eee_add_assoc_long(return_value, id);
                    eee_add_assoc_long(return_value, contentId);
                    add_assoc_std_string(return_value, params);
                    add_assoc_std_string(return_value, title);
                    eee_add_assoc_long(return_value, disable);
                    add_assoc_std_string(return_value, user_id);
                    eee_add_assoc_long(return_value, max_score);
                    eee_add_assoc_long(return_value, libraryId);
                    add_assoc_std_string(return_value, libraryName);
                    eee_add_assoc_long(return_value, libraryMajorVersion);
                    eee_add_assoc_long(return_value, libraryMinorVersion);
                    eee_add_assoc_long(return_value, libraryFullscreen);
                }
                if (cl.slug) {
                    std::string slug = cl.slug.value();
                    add_assoc_std_string(return_value, slug);
                }
                if (cl.language) {
                    std::string language = cl.language.value();
                    add_assoc_std_string(return_value, language);
                }
                {
                    std::string embedType = std::accumulate(
                            cl.embedType.begin(), cl.embedType.end(), std::string(""),
                            [] (const std::string &a, const std::string &b) {
                                if (!a.empty()) {
                                    std::string acc(a);
                                    acc.append(",");
                                    acc.append(b);
                                    return acc;
                                } else {
                                    return b;
                                }
                            });
                    add_assoc_std_string(return_value, embedType);
                }
                {
                    std::string libraryEmbedTypes = std::accumulate(
                            cl.libraryEmbedTypes.begin(), cl.libraryEmbedTypes.end(), std::string(""),
                            [] (const std::string &a, const std::string &b) {
                                if (!a.empty()) {
                                    std::string acc(a);
                                    acc.append(",");
                                    acc.append(b);
                                    return acc;
                                } else {
                                    return b;
                                }
                            });
                    add_assoc_std_string(return_value, libraryEmbedTypes);
                }
                {
                    std::string key = "metadata";
                    zval *metadata = zend_hash_str_add_empty_element(Z_ARR(*return_value), key.c_str(), key.length());
                    array_init(metadata);
                    for (const auto &[k, value] : cl.metadata) {
                        zval *zv = zend_hash_str_add_empty_element(Z_ARR(*metadata), k.c_str(), k.length());
                        ZVAL_STRINGL(zv, value.c_str(), value.length());
                    }
                }
            } else {
                zend_throw_exception(NULL, "Content not found", 0);
            }
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }

    PHP_FUNCTION(h5phost_load_content_dependencies) {
        long content_id;
        zval *zvtype;
        zend_string *ztype;
        std::string stype;
        std::optional<h5p::dependency_type> deptype = {};

        ZEND_PARSE_PARAMETERS_START(1, 2)
            Z_PARAM_LONG(content_id)
            if (_num_args > 1) {
                Z_PARAM_ZVAL(zvtype)
            } else {
                zvtype = NULL;
            }
        ZEND_PARSE_PARAMETERS_END();

        try {
            if (zvtype != NULL) {
                if (Z_TYPE(*zvtype) == IS_STRING) {
                    ztype = Z_STR(*zvtype);
                    stype = ZSTR_STD(ztype);
                    if (stype == "editor") {
                        deptype = h5p::editor;
                    } else if (stype == "preloaded") {
                        deptype = h5p::preloaded;
                    } else if (stype == "dynamic") {
                        deptype = h5p::dynamic;
                    } else {
                        throw std::exception();
                    }
                } else if (Z_TYPE(*zvtype) != IS_NULL && Z_TYPE(*zvtype) != IS_FALSE) {
                    throw std::exception();
                }
            }

            const std::vector<h5p::library_dependency_load> deploads = singleton->framework_interface()->loadContentDependencies(content_id, deptype);

            array_init(return_value);
            HashTable *ht = Z_ARR(*return_value);
            zval *zv_depload;
            HashTable *ht_depload;
            unsigned int idx = 0;
            for (const auto &depload : deploads) {
                zv_depload = zend_hash_index_add_empty_element(ht, idx++);
                array_init(zv_depload);
                ht_depload = Z_ARR(*zv_depload);

                if (depload.libraryId) {
                    unsigned int libraryId = depload.libraryId.value();
                    eee_add_assoc_long(zv_depload, libraryId);
                }
                {
                    std::string machineName = depload.machineName;
                    unsigned int majorVersion = depload.majorVersion;
                    unsigned int minorVersion = depload.minorVersion;
                    unsigned int patchVersion = depload.patchVersion;
                    add_assoc_std_string(zv_depload, machineName);
                    eee_add_assoc_long(zv_depload, majorVersion);
                    eee_add_assoc_long(zv_depload, minorVersion);
                    eee_add_assoc_long(zv_depload, patchVersion);
                }
                if (depload.preloadedJs) {
                    const auto &value = depload.preloadedJs.value();
                    std::string preloadedJs = std::accumulate(
                            value.begin(), value.end(), std::string(""),
                            [] (const std::string &a, const std::string &b) {
                                if (!a.empty()) {
                                    std::string acc(a);
                                    acc.append(",");
                                    acc.append(b);
                                    return acc;
                                } else {
                                    return b;
                                };
                            });
                    add_assoc_std_string(zv_depload, preloadedJs);
                }
                if (depload.preloadedCss) {
                    const auto &value = depload.preloadedCss.value();
                    std::string preloadedCss = std::accumulate(
                            value.begin(), value.end(), std::string(""),
                            [] (const std::string &a, const std::string &b) {
                                if (!a.empty()) {
                                    std::string acc(a);
                                    acc.append(",");
                                    acc.append(b);
                                    return acc;
                                } else {
                                    return b;
                                };
                            });
                    add_assoc_std_string(zv_depload, preloadedCss);
                }
                if (depload.dropCss) {
                    const auto &value = depload.dropCss.value();
                    std::string dropCss = std::accumulate(
                            value.begin(), value.end(), std::string(""),
                            [] (const std::string &a, const std::string &b) {
                                if (!a.empty()) {
                                    std::string acc(a);
                                    acc.append(",");
                                    acc.append(b);
                                    return acc;
                                } else {
                                    return b;
                                };
                            });
                    add_assoc_std_string(zv_depload, dropCss);
                }
            }
        } catch (...) {
            zend_throw_exception(NULL, "H5phost platform exception", 0);
        }
    }
}

PHP_FUNCTION(h5phost_get_option) {
    zend_string *zname;

    ZEND_PARSE_PARAMETERS_START(1, 1)
        Z_PARAM_STR(zname)
    ZEND_PARSE_PARAMETERS_END();

    try {
        std::string name = ZSTR_STD(zname);
        std::optional<std::string> json_str = singleton->framework_interface()->getOption(name);
        if (json_str) {
            web::json::value json = web::json::value::parse(json_str.value());
            ZVAL_NULL(return_value);
            zval_merge_json(return_value, json);
        } else {
            RETURN_NULL();
        }
    } catch (...) {
        zend_throw_exception(NULL, "H5phost platform exception", 0);
    }
}

PHP_FUNCTION(h5phost_set_option) {
    zend_string *zname;
    zval *zvalue;

    ZEND_PARSE_PARAMETERS_START(2, 2)
            Z_PARAM_STR(zname)
            Z_PARAM_ZVAL(zvalue);
    ZEND_PARSE_PARAMETERS_END();

    try {
        std::string name = ZSTR_STD(zname);
        if (Z_TYPE(*zvalue) != IS_NULL) {
            web::json::value json = zval_to_json(zvalue);
            singleton->framework_interface()->setOption(name, {json.serialize()});
        } else {
            singleton->framework_interface()->setOption(name, {});
        }
    } catch (...) {
        zend_throw_exception(NULL, "H5phost platform exception", 0);
    }
}

PHP_FUNCTION(h5phost_update_content_fields) {
    long content_id;
    zval *fields;

    ZEND_PARSE_PARAMETERS_START(2, 2)
        Z_PARAM_LONG(content_id)
        Z_PARAM_ZVAL(fields)
    ZEND_PARSE_PARAMETERS_END();

    try {
        HashTable *ht = Z_ARR(*fields);
        long num;
        zend_string *zkey;
        std::string key;
        zval *zvalue;
        h5p::update_content_fields update_fields = {};

        ZEND_HASH_FOREACH_KEY_VAL(ht, num, zkey, zvalue)
            if (zkey != NULL) {
                key = ZSTR_STD(zkey);
                if (key == "title") {
                    update_fields.title = zval_as_string_nofail(zvalue);
                } else if (key == "slug") {
                    update_fields.slug = zval_as_string_nofail(zvalue);
                } else if (key == "filtered") {
                    update_fields.filtered = zval_as_string_nofail(zvalue);
                }
            }
        ZEND_HASH_FOREACH_END();

        singleton->framework_interface()->updateContentFields(content_id, update_fields);
    } catch (...) {
        zend_throw_exception(NULL, "H5phost platform exception", 0);
    }
}

PHP_FUNCTION(h5phost_clear_filtered_parameters) {
    zval *z_lib_ids;
    std::vector<unsigned int> lib_ids = {};

    ZEND_PARSE_PARAMETERS_START(1, 1)
        Z_PARAM_ZVAL(z_lib_ids)
    ZEND_PARSE_PARAMETERS_END();

    try {
        auto z_lib_ids_type = Z_TYPE(*z_lib_ids);
        switch (z_lib_ids_type) {
            case IS_LONG: {
                unsigned int id = Z_LVAL(*z_lib_ids);
                lib_ids = { id };
                break;
            }
            case IS_ARRAY: {
                HashTable *ht = Z_ARR(*z_lib_ids);
                zval *zv;
                ZEND_HASH_FOREACH_VAL(ht, zv) {
                    std::string strdec;
                    auto zt = Z_TYPE(*zv);
                    switch (zt) {
                        case IS_LONG: {
                            unsigned int id = Z_LVAL(*zv);
                            lib_ids.push_back(id);
                            break;
                        }
                        case IS_STRING: {
                            zend_string *zstr = Z_STR(*zv);
                            strdec = ZSTR_STD(zstr);
                            unsigned int id = std::stol(strdec);
                            lib_ids.push_back(id);
                            break;
                        }
                        default:
                            throw std::exception();
                    }
                } ZEND_HASH_FOREACH_END();
            }
        }

        singleton->framework_interface()->clearFilteredParameters(lib_ids);
    } catch (...) {
        zend_throw_exception(NULL, "H5phost platform exception", 0);
    }
}

PHP_FUNCTION(h5phost_get_num_not_filtered) {
    try {
        unsigned int count = singleton->framework_interface()->getNumNotFiltered();
        RETURN_LONG(count);
    } catch (...) {
        zend_throw_exception(NULL, "H5phost platform exception", 0);
    }
}

PHP_FUNCTION(h5phost_get_num_content) {
    zend_long library_id;
    zval *skip_z;
    HashTable *skip_ht;
    std::vector<unsigned int> skip = {};
    zval *skip_id_z;
    zend_long skip_id;

    ZEND_PARSE_PARAMETERS_START(1, 2)
        Z_PARAM_LONG(library_id)
        if (_num_args > 1) {
            Z_PARAM_ZVAL(skip_z);
        } else {
            skip_z = NULL;
        }
    ZEND_PARSE_PARAMETERS_END();

    try {
        if (skip_z != NULL && Z_TYPE(*skip_z) == IS_ARRAY) {
            skip_ht = Z_ARR(*skip_z);
            ZEND_HASH_FOREACH_VAL(skip_ht, skip_id_z)
                    auto tp = Z_TYPE(*skip_id_z);
                    switch (tp) {
                        case IS_LONG: {
                            skip_id = Z_LVAL(*skip_id_z);
                            break;
                        }
                        case IS_STRING: {
                            zend_string *zstr = Z_STR(*skip_id_z);
                            std::string str = ZSTR_STD(zstr);
                            skip_id = std::stol(str);
                            break;
                        }
                        default:
                            throw std::exception();
                    }
                    skip.push_back(skip_id);
            ZEND_HASH_FOREACH_END();
        }
        zend_long count = singleton->framework_interface()->getNumContent(library_id, skip);
        RETURN_LONG(count);
    } catch (...) {
        zend_throw_exception(NULL, "H5phost platform exception", 0);
    }
}

PHP_FUNCTION(h5phost_is_content_slug_available) {
    zend_string *slug;

    ZEND_PARSE_PARAMETERS_START(1, 1)
        Z_PARAM_STR(slug)
    ZEND_PARSE_PARAMETERS_END();

    try {
        std::string slug_str = ZSTR_STD(slug);
        zend_bool res = singleton->framework_interface()->isContentSlugAvailable(slug_str);
        RETURN_BOOL(res);
    } catch (...) {
        zend_throw_exception(NULL, "H5phost platform exception", 0);
    }
}

PHP_FUNCTION(h5phost_get_num_authors) {
    try {
        zend_long num = singleton->framework_interface()->getNumAuthors();
        RETURN_LONG(num);
    } catch (...) {
        zend_throw_exception(NULL, "H5phost platform exception", 0);
    }
}

PHP_FUNCTION(h5phost_save_cached_assets) {
    zend_string *zhash;
    std::string hash;
    HashTable  *library_ids_ht;
    std::vector<unsigned int> library_ids = {};
    zval *library_id_z;

    ZEND_PARSE_PARAMETERS_START(2, 2)
        Z_PARAM_STR(zhash)
        Z_PARAM_ARRAY_HT(library_ids_ht)
    ZEND_PARSE_PARAMETERS_END();

    try {
        hash = ZSTR_STD(zhash);

        ZEND_HASH_FOREACH_VAL(library_ids_ht, library_id_z)
            auto tp = Z_TYPE(*library_id_z);
            switch (tp) {
                case IS_LONG: {
                    zend_long id = Z_LVAL(*library_id_z);
                    library_ids.push_back(id);
                    break;
                }
                case IS_STRING: {
                    zend_string *zstr = Z_STR(*library_id_z);
                    std::string str = ZSTR_STD(zstr);
                    unsigned int id = std::stol(str);
                    library_ids.push_back(id);
                    break;
                }
                default:
                    throw std::exception();
            }
        ZEND_HASH_FOREACH_END();

        singleton->framework_interface()->saveCachedAssets(hash, library_ids);
    } catch (...) {
        zend_throw_exception(NULL, "H5phost platform exception", 0);
    }
}

PHP_FUNCTION(h5phost_delete_cached_assets) {
    zend_long library_id;

    ZEND_PARSE_PARAMETERS_START(1, 1)
        Z_PARAM_LONG(library_id)
    ZEND_PARSE_PARAMETERS_END();

    try {
        std::vector<std::string> hashes = singleton->framework_interface()->deleteCachedAssets(library_id);
        array_init(return_value);
        HashTable *rt_ht = Z_ARR(*return_value);
        zend_long idx = 0;
        for (const std::string &hash : hashes) {
            zval *zv = zend_hash_index_add_empty_element(rt_ht, idx++);
            ZVAL_STRINGL(zv, hash.c_str(), hash.length());
        }
    } catch (...) {
        zend_throw_exception(NULL, "H5phost platform exception", 0);
    }
}

PHP_FUNCTION(h5phost_get_library_content_count) {
    HashTable *ht;

    try {
        std::vector<std::tuple<h5p::library_without_patch_version, unsigned int>> counts = singleton->framework_interface()->getLibraryContentCount();
        array_init(return_value);
        ht = Z_ARR(*return_value);
        for (auto &[lib_info, count] : counts) {
            std::string key = (boost::format("%s %s.%s") % lib_info.machineName % lib_info.majorVersion % lib_info.minorVersion).str();
            zval *zv = zend_hash_str_add_empty_element(ht, key.c_str(), key.length());
            ZVAL_LONG(zv, count);
        }
    } catch (...) {
        zend_throw_exception(NULL, "H5phost platform exception", 0);
    }
}

PHP_FUNCTION(h5phost_after_export_created) {
    HashTable *content_ht;
    unsigned int content_id;
    bool found_content_id = false;
    zend_string *zfilename;
    std::string filename;
    zend_long key_num;
    zend_string *key_str;
    std::string key;
    zval *zv;

    ZEND_PARSE_PARAMETERS_START(2, 2)
        Z_PARAM_ARRAY_HT(content_ht)
        Z_PARAM_STR(zfilename)
    ZEND_PARSE_PARAMETERS_END();

    try {
        ZEND_HASH_FOREACH_KEY_VAL(content_ht, key_num, key_str, zv)
            if (key_str != NULL) {
                key = ZSTR_STD(key_str);
                if (key == "id") {
                    content_id = Z_LVAL(*zv);
                    found_content_id = true;
                }
            }
        ZEND_HASH_FOREACH_END();
        if (!found_content_id) {
            throw std::exception();
        }
        filename = ZSTR_STD(zfilename);

        singleton->framework_interface()->afterExportCreated(content_id, filename);
    } catch (...) {
        zend_throw_exception(NULL, "H5phost platform exception", 0);
    }
}

#define H5P_PERMISSION_DOWNLOAD_H5P 0
#define H5P_PERMISSION_EMBED_H5P 1
#define H5P_PERMISSION_CREATE_RESTRICTED 2
#define H5P_PERMISSION_UPDATE_LIBRARIES 3
#define H5P_PERMISSION_INSTALL_RECOMMENDED 4
#define H5P_PERMISSION_COPY_H5P 8
PHP_FUNCTION(h5phost_has_permission) {
    zend_long h5p_permission;
    h5p::permission permission;
    zval *zuid;
    std::optional<unsigned int> uid;

    ZEND_PARSE_PARAMETERS_START(1, 2)
        Z_PARAM_LONG(h5p_permission)
        if (_num_args > 1) {
            Z_PARAM_ZVAL(zuid)
        } else {
            zuid = NULL;
        }
    ZEND_PARSE_PARAMETERS_END();

    try {
        switch (h5p_permission) {
            case H5P_PERMISSION_DOWNLOAD_H5P:
                permission = h5p::download_h5p;
                break;
            case H5P_PERMISSION_EMBED_H5P:
                permission = h5p::embed_h5p;
                break;
            case H5P_PERMISSION_CREATE_RESTRICTED:
                permission = h5p::create_restricted;
                break;
            case H5P_PERMISSION_UPDATE_LIBRARIES:
                permission = h5p::update_libraries;
                break;
            case H5P_PERMISSION_INSTALL_RECOMMENDED:
                permission = h5p::install_recommended;
                break;
            case H5P_PERMISSION_COPY_H5P:
                permission = h5p::copy_h5p;
                break;
            default:
                throw std::exception();
        }
        if (zuid != NULL) {
            auto ztype = Z_TYPE(*zuid);
            switch (ztype) {
                case IS_LONG: {
                    uid = Z_LVAL(*zuid);
                    break;
                }
                case IS_STRING: {
                    zend_string *zstr = Z_STR(*zuid);
                    std::string str = ZSTR_STD(zstr);
                    uid = std::stol(str);
                    break;
                }
                case IS_NULL: {
                    uid = {};
                    break;
                }
                default:
                    throw std::exception();
            }
        } else {
            uid = {};
        }

        bool hasPerm = singleton->framework_interface()->hasPermission(permission, uid);

        RETURN_BOOL(hasPerm);
    } catch (...) {
        zend_throw_exception(NULL, "H5phost platform exception", 0);
    }
}

h5p::hub_content_type decode_hub_content_type(HashTable *content_type_ht) {
    h5p::hub_content_type hubContentType = {};
    zend_long content_type_numindex;
    zend_string *content_type_zkey;
    std::string content_type_key;
    zval *content_type_zval;
    HashTable *sub_ht;
    zend_long sub_index;
    zend_string *sub_zkey;
    std::string sub_key;
    zval *sub_zval;

    hubContentType.categories = "[]";

    ZEND_HASH_FOREACH_KEY_VAL(content_type_ht, content_type_numindex, content_type_zkey, content_type_zval)
        if (content_type_zkey != NULL) {
            if (Z_TYPE(*content_type_zval) == IS_INDIRECT) {
                content_type_zval = Z_INDIRECT(*content_type_zval);
            }
            content_type_key = ZSTR_STD(content_type_zkey);
            if (content_type_key == "id") {
                hubContentType.name = zval_as_string_nofail(content_type_zval);
            } else if (content_type_key == "version") {
                if (Z_TYPE(*content_type_zval) == IS_OBJECT) {
                    sub_ht = Z_OBJPROP(*content_type_zval);
                    ZEND_HASH_FOREACH_KEY_VAL(sub_ht, sub_index, sub_zkey, sub_zval)
                        if (sub_zkey != NULL) {
                            sub_key = ZSTR_STD(sub_zkey);
                            if (Z_TYPE(*sub_zval) == IS_INDIRECT) {
                                sub_zval = Z_INDIRECT(*sub_zval);
                            }
                            if (sub_key == "major") {
                                hubContentType.major_version = zval_as_long_nofail(sub_zval);
                            } else if (sub_key == "minor") {
                                hubContentType.minor_version = zval_as_long_nofail(sub_zval);
                            } else if (sub_key == "patch") {
                                hubContentType.patch_version = zval_as_long_nofail(sub_zval);
                            }
                        }
                    ZEND_HASH_FOREACH_END();
                }
            } else if (content_type_key == "coreApiVersionNeeded") {
                if (Z_TYPE(*content_type_zval) == IS_OBJECT) {
                    sub_ht = Z_OBJPROP(*content_type_zval);
                    ZEND_HASH_FOREACH_KEY_VAL(sub_ht, sub_index, sub_zkey, sub_zval)
                        if (sub_zkey != NULL) {
                            sub_key = ZSTR_STD(sub_zkey);
                            if (Z_TYPE(*sub_zval) == IS_INDIRECT) {
                                sub_zval = Z_INDIRECT(*sub_zval);
                            }
                            if (sub_key == "major") {
                                hubContentType.h5P_major_version = zval_as_long_nofail(sub_zval);
                            } else if (sub_key == "minor") {
                                hubContentType.h5P_minor_version = zval_as_long_nofail(sub_zval);
                            }
                        }
                    ZEND_HASH_FOREACH_END();
                }
            } else if (content_type_key == "title") {
                hubContentType.title = zval_as_string_nofail(content_type_zval);
            } else if (content_type_key == "summary") {
                hubContentType.summary = zval_as_string_nofail(content_type_zval);
            } else if (content_type_key == "description") {
                hubContentType.description = zval_as_string_nofail(content_type_zval);
            } else if (content_type_key == "icon") {
                hubContentType.icon = zval_as_string_nofail(content_type_zval);
            } else if (content_type_key == "is_recommended") {
                hubContentType.is_recommended = zval_as_bool(content_type_zval);
            } else if (content_type_key == "popularity") {
                hubContentType.popularity = zval_as_string_nofail(content_type_zval);
            } else if (content_type_key == "screenshots") {
                auto tp = Z_TYPE(*content_type_zval);
                if (tp != IS_NULL && tp != IS_FALSE) {
                    hubContentType.screenshots = zval_to_json(content_type_zval).serialize();
                }
            } else if (content_type_key == "license") {
                auto tp = Z_TYPE(*content_type_zval);
                if (tp != IS_NULL && tp != IS_FALSE) {
                    hubContentType.license = zval_to_json(content_type_zval).serialize();
                }
            } else if (content_type_key == "example") {
                hubContentType.example = zval_as_string_nofail(content_type_zval);
            } else if (content_type_key == "tutorial") {
                hubContentType.tutorial = zval_as_string_nofail(content_type_zval);
            } else if (content_type_key == "keywords") {
                auto tp = Z_TYPE(*content_type_zval);
                if (tp != IS_NULL && tp != IS_FALSE) {
                    hubContentType.keywords = zval_to_json(content_type_zval).serialize();
                }
            } else if (content_type_key == "categories") {
                auto tp = Z_TYPE(*content_type_zval);
                if (tp != IS_NULL && tp != IS_FALSE) {
                    hubContentType.categories = zval_to_json(content_type_zval).serialize();
                }
            } else if (content_type_key == "owner") {
                hubContentType.owner = zval_as_string(content_type_zval);
            }
        }
    ZEND_HASH_FOREACH_END();
    return hubContentType;
}

PHP_FUNCTION(h5phost_replace_content_type_cache) {
    zval *content_type_cache_z;
    HashTable *content_type_cache_ht;
    zend_long content_type_cache_numindex;
    zend_string *content_type_cache_zstr_key;
    std::string content_type_cache_key;
    zval *content_type_cache_zval;
    HashTable *content_types_ht;
    zval *content_type_zval;
    std::vector<h5p::hub_content_type> hub_content_types = {};

    ZEND_PARSE_PARAMETERS_START(1, 1)
        Z_PARAM_OBJECT(content_type_cache_z)
    ZEND_PARSE_PARAMETERS_END();

    try {
        content_type_cache_ht = Z_OBJPROP(*content_type_cache_z);
        ZEND_HASH_FOREACH_KEY_VAL(content_type_cache_ht, content_type_cache_numindex, content_type_cache_zstr_key, content_type_cache_zval)
            if (content_type_cache_zstr_key != NULL) {
                content_type_cache_key = ZSTR_STD(content_type_cache_zstr_key);
                if (content_type_cache_key == "contentTypes") {
                    if (Z_TYPE(*content_type_cache_zval) == IS_INDIRECT) {
                        content_type_cache_zval = Z_INDIRECT(*content_type_cache_zval);
                    }
                    if (Z_TYPE(*content_type_cache_zval) == IS_ARRAY) {
                        content_types_ht = Z_ARR(*content_type_cache_zval);
                        ZEND_HASH_FOREACH_VAL(content_types_ht, content_type_zval)
                            if (Z_TYPE(*content_type_zval) == IS_OBJECT) {
                                hub_content_types.push_back(decode_hub_content_type(Z_OBJPROP(*content_type_zval)));
                            }
                        ZEND_HASH_FOREACH_END();
                    } else {
                        throw std::exception();
                    }
                }
            }
        ZEND_HASH_FOREACH_END();

        singleton->framework_interface()->replaceContentTypeCache(hub_content_types);
    } catch (...) {
        zend_throw_exception(NULL, "H5phost platform exception", 0);
    }
}

PHP_FUNCTION(h5phost_library_has_upgrade) {
    zval *library_zval;
    h5p::library_with_version libv;

    ZEND_PARSE_PARAMETERS_START(1, 1)
        Z_PARAM_ARRAY(library_zval)
    ZEND_PARSE_PARAMETERS_END();

    try {
        libv = library_with_version(Z_ARR(*library_zval));

        zend_bool res = singleton->framework_interface()->libraryHasUpgrade(libv);

        RETURN_BOOL(res);
    } catch (...) {
        zend_throw_exception(NULL, "H5phost platform exception", 0);
    }
}

PHP_FUNCTION(host_push_command) {
    zend_string *zstr;

    ZEND_PARSE_PARAMETERS_START(1, 1)
        Z_PARAM_STR(zstr)
    ZEND_PARSE_PARAMETERS_END();

    try {
        std::string str = ZSTR_STD(zstr);
        singleton->pushCommand(str);
    } catch (...) {
        zend_throw_exception(NULL, "Host platform exception, push command", 0);
    }
}

PHP_FUNCTION(host_pop_command) {
    try {
        std::string str = singleton->popCommand();
        RETURN_STRINGL(str.c_str(), str.length());
    } catch (...) {
        zend_throw_exception(NULL, "Host platform exception, pop command", 0);
    }
}

ZEND_BEGIN_ARG_INFO_EX(h5phost_fetch_external_data_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, url)
    ZEND_ARG_ARRAY_INFO(0, data, 1)
    ZEND_ARG_INFO(0, blocking)
    ZEND_ARG_INFO(0, stream)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_save_library_data_arginfo, 0, 0, 2)
    ZEND_ARG_ARRAY_INFO(1, libraryData, 0)
    ZEND_ARG_INFO(0, newObject)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_set_library_tutorial_url_arginfo, 0, 0, 2)
    ZEND_ARG_INFO(0, machineName)
    ZEND_ARG_INFO(0, tutorialUrl)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_set_error_message_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, message)
    ZEND_ARG_INFO(0, code)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_set_info_message_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, message)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_get_messages_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, type)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_t_arginfo, 0, 0, 2)
    ZEND_ARG_INFO(0, message)
    ZEND_ARG_ARRAY_INFO(0, replacements, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_get_library_file_url_arginfo, 0, 0, 2)
    ZEND_ARG_INFO(0, folder)
    ZEND_ARG_INFO(0, filename)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_get_library_config_arginfo, 0, 0, 1)
    ZEND_ARG_ARRAY_INFO(0, libraries, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_get_library_id_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, machineName)
    ZEND_ARG_INFO(0, majorVersion)
    ZEND_ARG_INFO(0, minorVersion)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_get_whitelist_arginfo, 0, 0, 3)
    ZEND_ARG_INFO(0, isLibrary)
    ZEND_ARG_INFO(0, defaultContentWhitelist)
    ZEND_ARG_INFO(0, defaultLibraryWhitelist)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_is_patched_library_arginfo, 0, 0, 1)
    ZEND_ARG_ARRAY_INFO(0, library, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_insert_content_arginfo, 0, 0, 1)
    ZEND_ARG_ARRAY_INFO(0, content, 0)
    ZEND_ARG_INFO(0, contentMainId)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_update_content_arginfo, 0, 0, 1)
    ZEND_ARG_ARRAY_INFO(0, content, 0)
    ZEND_ARG_INFO(0, contentMainId)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_reset_content_user_data_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, contentId)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_save_library_dependencies_arginfo, 0, 0, 3)
    ZEND_ARG_INFO(0, libraryId)
    ZEND_ARG_ARRAY_INFO(0, dependencies, 0)
    ZEND_ARG_INFO(0, dependencyType)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_copy_library_usage_arginfo, 0, 0, 2)
    ZEND_ARG_INFO(0, contentId)
    ZEND_ARG_INFO(0, copyFromId)
    ZEND_ARG_INFO(0, mainContentId)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_delete_content_data_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, contentId)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_delete_library_usage_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, contentId)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_save_library_usage_arginfo, 0, 0, 2)
    ZEND_ARG_INFO(0, contentId)
    ZEND_ARG_ARRAY_INFO(0, libraries, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_get_library_usage_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, libraryId)
    ZEND_ARG_INFO(0, skipContent)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_load_library_arginfo, 0, 0, 3)
    ZEND_ARG_INFO(0, machineName)
    ZEND_ARG_INFO(0, majorVersion)
    ZEND_ARG_INFO(0, minorVersion)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_load_library_semantics_arginfo, 0, 0, 3)
    ZEND_ARG_INFO(0, machineName)
    ZEND_ARG_INFO(0, majorVersion)
    ZEND_ARG_INFO(0, minorVersion)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_alter_library_semantics_arginfo, 0, 0, 4)
    ZEND_ARG_INFO(1, semantics)
    ZEND_ARG_INFO(0, machineName)
    ZEND_ARG_INFO(0, majorVersion)
    ZEND_ARG_INFO(0, minorVersion)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_delete_library_dependencies_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, libraryId)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_delete_library_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, libraryId)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_load_content_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, contentId)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_load_content_dependencies_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, contentId)
    ZEND_ARG_INFO(0, type)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_get_option_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, name)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_set_option_arginfo, 0, 0, 2)
    ZEND_ARG_INFO(0, name)
    ZEND_ARG_INFO(0, value)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_update_content_fields_arginfo, 0, 0, 2)
    ZEND_ARG_INFO(0, content_id)
    ZEND_ARG_ARRAY_INFO(0, fields, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_clear_filtered_parameters_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, library_ids)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_get_num_content_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, library_id)
    ZEND_ARG_ARRAY_INFO(0, skip, 1)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_is_content_slug_available_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, slug)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_save_cached_assets_arginfo, 0, 0, 2)
    ZEND_ARG_INFO(0, key)
    ZEND_ARG_ARRAY_INFO(0, library_ids, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_delete_cached_assets_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, library_id)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_after_export_created_arginfo, 0, 0, 2)
    ZEND_ARG_ARRAY_INFO(0, content, 0)
    ZEND_ARG_INFO(0, filename)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_has_permission_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, permission)
    ZEND_ARG_INFO(0, uid)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_replace_content_type_cache_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, content_type_cache)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(h5phost_library_has_upgrade_arginfo, 0, 0, 1)
    ZEND_ARG_ARRAY_INFO(0, library, 0)
ZEND_END_ARG_INFO()

ZEND_BEGIN_ARG_INFO_EX(host_push_command_arginfo, 0, 0, 1)
    ZEND_ARG_INFO(0, command)
ZEND_END_ARG_INFO()

static const zend_function_entry pib_functions[] = {
    PHP_FE(h5phost_get_platform_info, NULL)
    PHP_FE(h5phost_fetch_external_data, h5phost_fetch_external_data_arginfo)
    PHP_FE(h5phost_save_library_data, h5phost_save_library_data_arginfo)
    PHP_FE(h5phost_set_library_tutorial_url, h5phost_set_library_tutorial_url_arginfo)
    PHP_FE(h5phost_set_error_message, h5phost_set_error_message_arginfo)
    PHP_FE(h5phost_set_info_message, h5phost_set_info_message_arginfo)
    PHP_FE(h5phost_get_messages, h5phost_get_messages_arginfo)
    PHP_FE(h5phost_t, h5phost_t_arginfo)
    PHP_FE(h5phost_get_library_file_url, h5phost_get_library_file_url_arginfo)
    PHP_FE(h5phost_get_uploaded_h5p_folder_path, NULL)
    PHP_FE(h5phost_get_uploaded_h5p_path, NULL)
    PHP_FE(h5phost_load_addons, NULL)
    PHP_FE(h5phost_get_library_config, h5phost_get_library_config_arginfo)
    PHP_FE(h5phost_load_libraries, NULL)
    PHP_FE(h5phost_get_admin_url, NULL)
    PHP_FE(h5phost_get_library_id, h5phost_get_library_id_arginfo)
    PHP_FE(h5phost_get_whitelist, h5phost_get_whitelist_arginfo)
    PHP_FE(h5phost_is_patched_library, h5phost_is_patched_library_arginfo)
    PHP_FE(h5phost_is_in_dev_mode, NULL)
    PHP_FE(h5phost_may_update_libraries, NULL)
    PHP_FE(h5phost_insert_content, h5phost_insert_content_arginfo)
    PHP_FE(h5phost_update_content, h5phost_update_content_arginfo)
    PHP_FE(h5phost_reset_content_user_data, h5phost_reset_content_user_data_arginfo)
    PHP_FE(h5phost_save_library_dependencies, h5phost_save_library_dependencies_arginfo)
    PHP_FE(h5phost_copy_library_usage, h5phost_copy_library_usage_arginfo)
    PHP_FE(h5phost_delete_content_data, h5phost_delete_content_data_arginfo)
    PHP_FE(h5phost_delete_library_usage, h5phost_delete_library_usage_arginfo)
    PHP_FE(h5phost_save_library_usage, h5phost_save_library_usage_arginfo)
    PHP_FE(h5phost_get_library_usage, h5phost_get_library_usage_arginfo)
    PHP_FE(h5phost_load_library, h5phost_load_library_arginfo)
    PHP_FE(h5phost_load_library_semantics, h5phost_load_library_semantics_arginfo)
    PHP_FE(h5phost_alter_library_semantics, h5phost_alter_library_semantics_arginfo)
    PHP_FE(h5phost_delete_library_dependencies, h5phost_delete_library_dependencies_arginfo)
    PHP_FE(h5phost_lock_dependency_storage, NULL)
    PHP_FE(h5phost_unlock_dependency_storage, NULL)
    PHP_FE(h5phost_delete_library, h5phost_delete_library_arginfo)
    PHP_FE(h5phost_load_content, h5phost_load_content_arginfo)
    PHP_FE(h5phost_load_content_dependencies, h5phost_load_content_dependencies_arginfo)
    PHP_FE(h5phost_get_option, h5phost_get_option_arginfo)
    PHP_FE(h5phost_set_option, h5phost_set_option_arginfo)
    PHP_FE(h5phost_update_content_fields, h5phost_update_content_fields_arginfo)
    PHP_FE(h5phost_clear_filtered_parameters, h5phost_clear_filtered_parameters_arginfo)
    PHP_FE(h5phost_get_num_not_filtered, NULL)
    PHP_FE(h5phost_get_num_content, h5phost_get_num_content_arginfo)
    PHP_FE(h5phost_is_content_slug_available, h5phost_is_content_slug_available_arginfo)
    PHP_FE(h5phost_get_num_authors, NULL)
    PHP_FE(h5phost_save_cached_assets, h5phost_save_cached_assets_arginfo)
    PHP_FE(h5phost_delete_cached_assets, h5phost_delete_cached_assets_arginfo)
    PHP_FE(h5phost_get_library_content_count, NULL)
    PHP_FE(h5phost_after_export_created, h5phost_after_export_created_arginfo)
    PHP_FE(h5phost_has_permission, h5phost_has_permission_arginfo)
    PHP_FE(h5phost_replace_content_type_cache, h5phost_replace_content_type_cache_arginfo)
    PHP_FE(h5phost_library_has_upgrade, h5phost_library_has_upgrade_arginfo)
    PHP_FE(host_push_command, host_push_command_arginfo)
    PHP_FE(host_pop_command, NULL)
    PHP_FE_END
};

H5pPhpBindings::H5pPhpBindings(H5PFrameworkInterface &h5pFrameworkInterface) : h5pFrameworkInterface(h5pFrameworkInterface) {
    if (singleton != nullptr) {
        throw std::exception();
    }
    singleton = this;
}

H5pPhpBindings::~H5pPhpBindings() {
    singleton = nullptr;
}

H5PFrameworkInterface *H5pPhpBindings::framework_interface() {
    return &h5pFrameworkInterface;
}

const zend_function_entry *H5pPhpBindings::zend_function_entries() {
    return pib_functions;
}

void H5pPhpBindings::pushCommand(const std::string &cmd) {
    command.push_back(cmd);
}

std::string H5pPhpBindings::popCommand() {
    std::string back = command.back();
    command.pop_back();
    return back;
}
